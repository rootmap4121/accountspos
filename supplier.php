<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $obj->bodyhead(); ?>  
		<script>
			function SaveCustomer()
			{
				var first_name=$('.span12 .first_name').val();
				var last_name=$('.span12 .last_name').val();
				var company_name=$('#company_name').val();
				var email=$('.span12 .email').val();
				var phone=$('.span12 .phone').val();
				var country=$('#country').val();
				var city_name=$('#city_name').val();
				var zip=$('#zip').val();
				var address=$('#address').val();
				//console.log(first_name+" "+last_name+" "+email+" "+phone+" "+country+" "+city_name+" "+zip+" "+address);
				$.post("lib/supplier.php",{'st':1,'first_name':first_name,'last_name':last_name,'email':email,'phone':phone,'country':country,'city_name':city_name,'company_name':company_name,'zip':zip,'address':address},function(data)
				{
					if(data==1)
					{
					   	clear();
				       		$.jGrowl('Saved, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
					}
					else if(data==2)
					{
					  	$.jGrowl('Failed, Already Exists.', { sticky: false, theme: 'growl-warning', header: 'Error!' });
					}
					else
					{
						$.jGrowl('Failed, Try Again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
					}
				});
			}


			function clear()
			{
				$('.span12 .first_name').val("");
				$('.span12 .last_name').val("");
				$('.span12 .email').val("");
				$('.span12 .phone').val("");
				$('#city_name').val("");
				$('#zip').val("");
				$('#address').val("");	
			}
		</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
			    <h5><i class="font-money"></i> New supplier Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                            
                                          
                                          <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">     
                                           <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px;">
                                                <div class="control-group">
                                                    <label class="control-label"> * First Name :</label>
                                                    <div class="controls"><input class="span12 first_name" type="text" name="name" /></div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> * Last Name </label>
                                                    <div class="controls"><input class="span12 last_name" type="text" name="email" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> * Email </label>
                                                    <div class="controls"><input class="span12 email" type="text" name="email" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> * Phone </label>
                                                    <div class="controls"><input class="span12 phone" type="text" name="phone" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label">  Company Name </label>
						    <div class="controls"><input class="span12" id="company_name" type="text" name="phone" /></div>
                                                </div>

                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px; float:right;">
                                                

                                                <div class="control-group">
                                                    <label class="control-label"> Country </label>
                                                    <div class="controls">
                                                        <select name="country" data-placeholder="Select Country" class="select-search" id="country" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php 
															$sqlcountry=$obj->SelectAll("country");
															if(!empty($sqlcountry))
															foreach($sqlcountry as $country):
															?>
                                                            <option  value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option> 
                                                            <?php endforeach; ?>
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> City Name </label>
                                                    <div class="controls"><input class="span4" id="city_name" type="text" name="city" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Zip Code </label>
                                                    <div class="controls">
                                                        <input class="span6 zip" type="text" id="zip" name="zip" />
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> Address </label>
                                                    <div class="controls"><input id="address" class="span12" type="text" name="address" /></div>
                                                </div>

                                                
                                                
                                                
                                                
                                                <div class="control-group">
                                                    <label class="control-label">&nbsp;</label>
                                                    <div class="controls"><button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                    <i class="icon-plus-sign"></i> Save Supplier Detail </button></div>
                                                </div>
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>
                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->


                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                </fieldset>                     



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div> 
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>

</html>
