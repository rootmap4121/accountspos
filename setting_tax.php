<?php
include('class/auth.php');
if(isset($_GET['del']))
{
	extract($_GET);
	if($obj->delete("account_module_tax_rate",array("shop_id"=>$shop_id,"ledger_id"=>$ledger_id))==1)
	{
		$obj->Success("Tax Rate Deleted Successfully",$obj->filename());
	}
	elseif(!$sql)
	{
		$obj->Error("Tax Rate Deletion Failed.",$obj->filename());
	}
}
elseif(isset($_POST['update']))
{
	extract($_POST);
	$chktaxcheck=$obj->exists_multiple("account_module_tax_rate",array("shop_id"=>$shop_id,"ledger_id"=>$ledger_id));
	if($chktaxcheck==1)
	{
		if($obj->updateUsingMultiple("account_module_tax_rate",array("rate"=>$rate,"date"=>date('Y-m-d'),"status"=>1),array("shop_id"=>$shop_id,"ledger_id"=>$ledger_id))==1)
		{
			$obj->Success("Tax Rate Updated Successfully.",$obj->filename());
		}
		else
		{
			$obj->Success("Failed, Update Tax Rate.",$obj->filename());
		}
	}
	else
	{
		if($obj->insert("account_module_tax_rate",array("rate"=>$rate,"date"=>date('Y-m-d'),"status"=>1,"shop_id"=>$shop_id,"ledger_id"=>$ledger_id))==1)
		{
			$obj->Success("Tax Rate Saved Successfully.",$obj->filename());
		}
		else
		{
			$obj->Error("Tax Rate Saved Failed.",$obj->filename());
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Tax Setting for ledger book </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                        <div class="well span10">
                                            
                                          <form action="" method="post">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th style="text-align:center;">Ledger ID</th>
                                                        <th style="text-align:center;">Tax Name</th>
                                                        <th style="text-align:center;">Rate</th>
                                                        <th style="text-align:center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!isset($_GET['edit'])){
                                                    $sql1=$obj->SelectAll("account_module_newtax");
                                                    $a=1;
                                                    if(!empty($sql1))
                                                    foreach($sql1 as $row)
                                                    {
														$tax_rate=$obj->SelectAllByVal2("account_module_tax_rate","shop_id",$shop_id,"ledger_id",$row->main_id,"rate");
														if($tax_rate=="")
														{
															$rate=0;	
														}
														else
														{
															$rate=$tax_rate;
														}
														
                                                    ?>
                                                        <tr>
                                                            <td valign="middle" align="center" style="width:30px;"><?php echo $a; ?></td>
                                                            <td valign="middle" align="center"><?php echo $row->main_id; ?></td>
                                                            <td align="center" valign="middle"><?php echo $row->name; ?></td>
                                                            <td align="center" valign="middle"><?php echo $rate; ?>%</td>
                                                            <td align="center" valign="middle">
                                                            <a href="<?php echo $obj->filename(); ?>?edit=<?php echo $row->id; ?>" style="color:rgba(51,51,51,1);" onClick="javascript:return confirm('Are you absolutely sure to edit This?')" title="Edit"><i class="icon-edit"></i></a> 
                                                            <a href="<?php echo $obj->filename(); ?>?del=1&amp;ledger_id=<?php echo $row->main_id; ?>" style="color:rgba(51,51,51,1);"  onclick="javascript:return confirm('Are you absolutely sure to delete This?')" title="Delete"><i class="icon-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                     <?php 
                                                     $a++;
                                                     } 
                                                }
                                                else
                                                {
                                                            $sql1=$obj->SelectAllByID_Multiple("account_module_newtax",array("id"=>$_GET['edit']));
                                                            $a=1;
                                                            if(!empty($sql1))
                                                            foreach($sql1 as $row)
                                                            {
																$tax_rate=$obj->SelectAllByVal2("account_module_tax_rate","shop_id",$shop_id,"ledger_id",$row->main_id,"rate");
														if($tax_rate=="")
														{
															$rate=0;	
														}
														else
														{
															$rate=$tax_rate;
														}
                                                         ?> 
                                                         <tr>
                                                            <td valign="middle" align="center" style="width:30px;"><?php echo $a; ?></td>
                                                            <td valign="middle" align="center"><?php echo $row->main_id; ?>
                                                            <input type="hidden" value="<?php echo $_GET['edit']; ?>" name="id">
                                                            <input type="hidden" value="<?php echo $row->main_id; ?>" name="ledger_id">
                                                            </td>
                                                            <td align="center" valign="middle"><input type="text" value="<?php echo $row->name; ?>" name="name"></td>
                                                            <td align="center" valign="middle"><input type="text" style="width:50px;" value="<?php echo $rate; ?>" name="rate">%</td>
                                                            <td align="center" valign="middle">
                                                                <button type="submit" name="update" class="btn btn-info" onClick="javascript:return confirm('Are you absolutely sure to change This?')" title="update"><i class="icon-check"></i> Update Changes</button> 
                                                                <button type="reset" name="reset" class="btn btn-danger" onClick="javascript:return confirm('Are you absolutely sure to clear This?')" title="clear"><i class="icon-close"></i> Reset</button>
                                                            </td>
                                                        </tr>
                                                <?php 
                                                        $a++;
                                                        }
                                                
                                                }
                                                
                                                 ?> 
                                                </tbody>
                                            </table>
                                            </form>
                                                        <!--tab 1 content start from here-->
                                                    </div>
                                                    
                                                    
                                                    
                                               
                                        </div>
                                        <!-- General form elements -->


                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                </fieldset>                     



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
			ddtreemenu.createTree("treemenu1", true)
			ddtreemenu.createTree("treemenu2", true)
			ddtreemenu.createTree("treemenu3", true)
			ddtreemenu.createTree("treemenu4", true)
			ddtreemenu.createTree("treemenu5", true)
			ddtreemenu.createTree("treemenu6", true)
			ddtreemenu.createTree("treemenu7", false)
			</script>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
