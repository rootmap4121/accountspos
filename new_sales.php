<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
        <script>
			function SaveCustomer()
			{
				
				var emptyflag=false;
				//var formdata = nucleus('#myForm, #myForms').serializeArray();  
				var product = nucleus('.product_id').serializeArray();
				var quantity = nucleus('.quantity').serializeArray();
				var unitprice = nucleus('.unitprice').serializeArray();
				var rowtotal = nucleus('.rowtotal').serializeArray();
				
				var description = nucleus('.description').serializeArray();
				
				jQuery.each( product, function( i, val ){ if(val.value==""){ emptyflag=true; } });
				jQuery.each( quantity, function( i, val ){ if(val.value=="" || val.value==0){ emptyflag=true; } });
				jQuery.each( unitprice, function( i, val ){ if(val.value=="" || val.value==0){ emptyflag=true; } });
				jQuery.each( rowtotal, function( i, val ){ if(val.value=="" || val.value==0){ emptyflag=true; } });
				
				//console.log(emptyflag);
				
				var invoice_date=$('#invoice_date').val();
				var paid_date=$('#paid_date').val();
				var customer_id=$('#customer_id').val();
				var subheading=$('#subheading').val();
				var shop_id=$('#shop_id').val();
				var memo=$('#memo').val();
				
				if(invoice_date!="" && paid_date!="" && customer_id!="" && shop_id!="" && emptyflag==false)
				{
					
/*					if(journal_entry_check()==0)
					{
						$.jGrowl('Invalid Journal Entry.', { sticky: false, theme: 'growl-error', header: 'Invalid Journal Entry!' });
					}
					else
					{*/
					$.jGrowl('Your Sales Invoice is Processing now, Please Wait....', { sticky: false, theme: 'growl-info', header: 'Processing!' });
						$.post("lib/sales.php",{'st':2,
						'invoice_date':invoice_date,
						'paid_date':paid_date,
						'customer_id':customer_id,
						'subheading':subheading,
						'shop_id':shop_id,
						'memo':memo,'description':description,'product':product,'quantity':quantity,'unitprice':unitprice,'rowtotal':rowtotal},function(data)
						{
							if(data==1)
							{
							   clear();
							   $.jGrowl('Saved, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
							}
							else if(data==2)
							{
							  $.jGrowl('Failed, Already Exists.', { sticky: false, theme: 'growl-warning', header: 'Error!' });
							}
							else
							{
							   $.jGrowl('Failed, Try Again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
							}
						});
					/*}*/
				}
				else
				{
					$.jGrowl('Failed, Some Field is Empty.', { sticky: false, theme: 'growl-error', header: 'Error!' });
				}
			}
			
			function clear()
			{
				$('#invoice_date').val("");
				$('#paid_date').val("");
				$('#customer_id').val("");
				$('#subheading').val("");
				$('#shop_id').val("");
				$('#memo').val("");
				
				$(".quantity").val("");
				$(".unitprice").val("");
				$(".rowtotal").val("");
				
				$('.clonerow').hide('slow');
				$('.clonerow').remove();
				
				checksum(0);
				
			}
			
		
			function checksum(st)
			{
				var quantity = 0.00;
				var unitprice = 0.00;
				var rowtotal = 0.00;
				var aa,bb,cc;
				  
               $(".quantity").each ( function() {
                   quantity += parseFloat ( $(this).val().replace(/\s/g,'').replace(',','.'));
               });
			   
			   $(".unitprice").each ( function() {
                   unitprice += parseFloat ( $(this).val().replace(/\s/g,'').replace(',','.'));
               });
			   
			   $(".rowtotal").each ( function() {
                   rowtotal += parseFloat ( $(this).val().replace(/\s/g,'').replace(',','.'));
               });
				
				document.getElementById('total_quantity').value= quantity;
				document.getElementById('totalunitprice').value= unitprice;
				document.getElementById('totalrowtotal').value= rowtotal;
				aa=quantity;
				bb=unitprice;
				cc=rowtotal;
				if(st==1)
				{
					if(aa==0 || aa=="" || bb==0 || bb=="" || cc==0 || cc=="") 
					{
						return 0;
					}
					else
					{
						return 1;	
					}
				}
			}
			
			//$().closest(
			
			nucleus(document).ready(function()
			{
				nucleus("#ui-datepicker-div").html("</form>");
				nucleus('#select-customer-id').find('.select2-input').keyup(function(){
					var getvalue=nucleus(this).val();
					var getlength=getvalue.length;
					var place=nucleus('#select-customer-id').find('select').attr('id');
					nucleus('#'+place).html("");
					if(getlength>=4)
					{
						$(".select2-no-results").html("wait your data is loading...");
						$.post("lib/search_controller.php",{'st':1,'table':"account_module_customer",'search':getvalue,'field_a':"id",'field_b':"concat(fname,' ',lname)"},function(fetch){
							var datacl=jQuery.parseJSON(fetch);
							var opt=datacl.data;
							nucleus('#'+place).html(opt);
						});
						checksum(0);
					}
				});
				
				nucleus('#select-customer-id-0').find('.select2-input').keyup(function(){
					var getvalue=nucleus(this).val();
					var getlength=getvalue.length;
					var place=nucleus('#select-customer-id-0').find('select').attr('id');
					nucleus('#'+place).html("");
					if(getlength>=4)
					{
						$(".select2-no-results").html("wait your data is loading...");
						$.post("lib/search_controller.php",{'st':1,'table':"account_module_product",'search':getvalue,'field_a':"id",'field_b':"pname"},function(fetch){
							var datacl=jQuery.parseJSON(fetch);
							var opt=datacl.data;
							nucleus('#'+place).html(opt);
						});
						checksum(0);
					}
				});
				
				
				
			});
			
		</script>
        <script> 

				function data(id)
				{
					nucleus(document).find('.select2-input').keyup(function(){
							var vald=nucleus(this).val();
							var getvalue=vald;
							var getlength=getvalue.length;
							nucleus('#select-customer-id-'+id).find('select').html("");
							if(getlength>=4)
							{
								$(".select2-no-results").html("wait your data is loading...");
								$.post("lib/search_controller.php",{'st':1,'table':"account_module_product",'search':getvalue,'field_a':"id",'field_b':"pname"},function(fetch){
									var datacl=jQuery.parseJSON(fetch);
									var opt=datacl.data;
									nucleus('#select-customer-id-'+id).find('select').html(opt);
								});
								checksum(0);
							}
					});					
				}
				
				function autoselectdata(id,place0,place1,place2,place3,place4)
				{
					//console.log(id+", "+place1+", "+place2+", "+place3+", "+place4);	
					 $.jGrowl('Please wait, Your Product data is processing now.', { sticky: false, theme: 'growl-success', header: 'Processing!' });
					 $.post("lib/sales.php",{'st':1,'id':id},function(fetch){
						//console.log(fetch);
						var datacl=nucleus.parseJSON(fetch);
						var status=datacl.status;
						var quantity=datacl.quantity;
						var price=datacl.price;
						var description=datacl.description;
						
						if(status==1)
						{
							$.jGrowl('Done, Now you can modify your quantity or price.', { sticky: false, theme: 'growl-success', header: 'Processing!' });
							nucleus("#"+place0).val(id);
							//console.log(description,quantity,price);
							nucleus("#"+place1).val(description);
							nucleus("#"+place2).val("0");
							nucleus("#"+place3).val(price);
						}
						else
						{
							$.jGrowl('Failed, Field quantity is empty please select another product.', { sticky: false, theme: 'growl-error', header: 'Error!' });
						}
						checksum(0);
					});
				}
				
				function autoquantitydata(id,place1,place2,place3,place4)
				{
					//console.log(id+", "+place1+", "+place2+", "+place3+", "+place4);	
					$.jGrowl('Please wait, data is processing.', { sticky: false, theme: 'growl-warning', header: 'Processing!' });
					var quantity=id;
					var unitpriceex=nucleus("#"+place3).val();					
					var qp=quantity*unitpriceex;
					if(quantity!="" || quantity!=0)
					{
						$.jGrowl('Total Row Total, [ '+quantity+' X '+unitpriceex+' =  '+qp+'].', { sticky: false, theme: 'growl-success', header: 'Done' });
						nucleus("#"+place4).val(qp);
					}
					checksum(0);					
				}
				//autounitdata
				function autounitdata(id,place1,place2,place3,place4)
				{
					//console.log(id+", "+place1+", "+place2+", "+place3+", "+place4);	
					$.jGrowl('Please wait, data is processing.', { sticky: false, theme: 'growl-warning', header: 'Processing!' });
					var quantity=nucleus("#"+place2).val();
					var unitpriceex=id;		
					var qp=quantity*unitpriceex;
					if(unitpriceex!="" || quantity!=0)
					{
						$.jGrowl('Total Row Total, [ '+quantity+' X '+unitpriceex+' =  '+qp+'].', { sticky: false, theme: 'growl-success', header: 'Done' });
						nucleus("#"+place4).val(qp);
					}		
					checksum(0);			
				}
				
				function autorowdata(id,place1,place2,place3,place4)
				{
					//console.log(id+", "+place1+", "+place2+", "+place3+", "+place4);	
					$.jGrowl('Please wait, data is processing.', { sticky: false, theme: 'growl-warning', header: 'Processing!' });
					var quantity=nucleus("#"+place2).val();
					//var unitpriceex=nucleus("#"+place3).val();;		
					var rowtotal=id;
					var devideacualunit=rowtotal/quantity;
					var newunit=devideacualunit.toFixed(2);
					//console.log(newunit);
					
					if(rowtotal!="" || rowtotal!=0)
					{
						$.jGrowl('New product unit price, [ '+rowtotal+' / '+quantity+' =  '+newunit+'].', { sticky: false, theme: 'growl-success', header: 'Done' });
						nucleus("#"+place3).val(newunit);
					}
					checksum(0);
				}
				
				
			 </script>
        <script>
		function hiderow(id)
		{
			   var chkclass=id;
			   //alert(chkclass);
			   if(chkclass=='class0')
			   {
				   alert("You Cann't Remove Table 1st & 2nd Rows");
			   }
			   else
			   {
					var c = confirm("are you sure to Delete this Row From Table ?.");
					if(c)
					{
					    $('.'+id).closest('tr').remove();
						var increval=cloneCount--;
						var increvald=cloneCountd--;
					}
			   }
		}
		
		</script>
    </head>

    <body>
     
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> New Sales Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">


								<div id="one"></div>
                                <div id="two"></div>
                                <div id="three"></div>
                                <div id="four"></div>
                                <div id="five"></div>

                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                          
                                                    <blockquote style="margin-top:-50px;">
                                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                                    </blockquote>
                                            
                                          
                                     <form class="form-horizontal" id="myForm" method="post" name="invoice" action="">    
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span4">
                                            	<div class="control-group" id="select-customer-id">
                                                    <label class="span12">Choose Customer *</label>
                                                    <select name="customer_id" data-placeholder="Select Customer"  class="minimum-select"  id="customer_id" tabindex="2">
                                                            <option value=""></option> 
                                                            
                                                       </select>
                                                </div>
                                            	
                                            	<div class="control-group">
                                                    <label class="span12">Invoice Date *</label>
                                                    <input type="text" name="invoice_date" id="invoice_date" class="datepicker" placeholder="Payment Date" />
                                                </div>                                               
                                            </div>
                                            
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span12">Subheading *</label>
                                                    <input type="text" name="subheading" id="subheading" class="span12 memo" placeholder="Journal track ID/Name/No." />
                                                </div>        
                                                
                                                <div class="control-group">
                                                    <label class="span12">Due Paid Date *</label>
                                                    <input type="text" name="paid_date" id="paid_date" class="datepicker" placeholder="Payment Date" />
                                                </div>  
                                                                                       
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="span4">
                                                
												
                                                <div class="control-group">
                                                    <label class="span12">Choose Shop *</label>
                                                    <select name="shop_id" data-placeholder="Select Shop" class="select-search" id="shop_id" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php
														   $sql2=$obj->SelectAll("store");
														   if(!empty($sql2))
														   foreach($sql2 as $rows)
														   {
														   ?>      
															<option value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
															<?php
														   }
															?>
                                                       </select>
                                                </div>
                                                
                                                
                                                <div class="control-group">
                                                    <label class="span12">Note / Memo *</label>
                                                    <input type="text" name="memo" id="memo" class="span12 memo" placeholder="Journal track ID/Name/No." />
                                                </div> 
                                                
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     

										
                                        </fieldset>                     

                                </form>
                                        
                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->












										<div class="row-fluid block">
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="table-overflow">
                                           <form class="form-horizontal" id="myForms" method="post" name="invoice" action="">    
                                    <fieldset>
                                                <table class="table table-striped table-bordered"  id="productmore">
                                                        <thead>
                                                            <tr>
                                                                <th>Product</th>
                                                                <th>Detail</th>
                                                                <th style="text-align:right;">Quantity</th>
                                                                <th style="text-align:right;">Unite Price</th>
                                                                <th style="text-align:right;">Total Price</th>
                                                                <th style="text-align:right;"><input type="hidden" id="currentId"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="POITable">
                                                        	<tr class="class0" id="0">
                                                            	<td>
                                                                    <div class="control-group" id="select-customer-id-0">
                                                                        <input type="hidden" class="product_id" name="product_id[]" id="product_id_0" />
<select name="product_ids[]" id="customer_id_0" tabindex="2" data-placeholder="Select Product"  onchange="autoselectdata(this.value,'product_id_0','select-description-0','select-quantity-0','select-unit-0','select-rt-0')" class="minimum-select">
                                                                                <option value=""></option> 
                                                                                
                                                                           </select>
                                                                    </div>
                                                                </td>
                                                            	<td><input type="text"  class="description"  name="description[]" id="select-description-0"></td>
                                                                <td style="text-align:right;">
                                                                
                                                                <input type="text" name="quantity[]" class="quantity"  style="width:50px; text-align:right;font-weight:bolder; "  onkeyup="autoquantitydata(this.value,'select-description-0','select-quantity-0','select-unit-0','select-rt-0')" name="quantity[]" id="select-quantity-0" value="0">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                
                                                                <input type="text" name="unitprice[]" class="unitprice"  style="width:100px; text-align:right;font-weight:bolder; "  onkeyup="autounitdata(this.value,'select-description-0','select-quantity-0','select-unit-0','select-rt-0')"  name="opt_a_debit[]" id="select-unit-0" value="0">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text"  class="rowtotal" style="width:100px; text-align:right;font-weight:bolder; " name="rowtotal[]"  onkeyup="autorowdata(this.value,'select-description-0','select-quantity-0','select-unit-0','select-rt-0')" id="select-rt-0"  value="0">
                                                                </td>                                                                 <td><label><a href="#" class="hhdrow" onClick="hiderow('class0')"><i class="icon-remove"></i></a></label></td>
                                                            </tr>
                                                         </tbody>
                                                         <tbody>       
                                                            <tr>
                                                            	<td align="center">
                                                                <button type="button"  onclick="return addTableRow('#productmore');return false;" id="btn2" class="btn" ><i class="icon-plus"></i> ADD MORE</button>
                                                                </td>
                                                            	<td align="right" valign="middle" style="font-weight:bolder;"><strong>Total</strong></td>
                                                                <td><input id="total_quantity"  type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="total_quantity" /></td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalunitprice" id="totalunitprice">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalrowtotal" id="totalrowtotal">
                                                                </td>                                                               <td></td>
                                                                </tr>        
                                                        </tbody>
                                                    </table>
                                                    </fieldset>                     

                                </form>
                                                      
                                                </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     

</fieldset>                     

                                </form>
                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>




									<div class="row-fluid block">
                                          
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span5">
                                            	                                          
                                            </div>
                                            
                                            <div class="span6">
                                            	                                         
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="spa1">
                                                
												
                                               <div class="control-group">
                                                    <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                    <i class="icon-plus-sign"></i> Save Record </button>
                                                </div>
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                  
                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->




                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                                  



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>  
            
            <script type="text/javascript">
				
				var cloneCount = 1;
				var cloneCountd = 0;
				function addTableRow(table)
				{
					var increval=cloneCount++;
					var increvald=cloneCountd++;
					var $tr = $(table+' tbody:first').children("tr:last").clone().attr("class",'class'+ increval);
						
						$tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
						$tr.find(".hhdrow").attr("onClick","hiderow('class"+increval+"')");
						$tr.find("#s2id_customer_id_"+increvald).hide();
						$tr.find('select').select2();
						$tr.find(".description").attr("id","select-description-"+ increval);
						$tr.find(".quantity").attr("id","select-quantity-"+ increval);
						$tr.find(".unitprice").attr("id","select-unit-"+ increval);
						$tr.find(".rowtotal").attr("id","select-rt-"+ increval);
						$tr.find('select').html("<option value=''></option>");
					//$(table+' tbody:first').children("tr:last").after($tr);
					$(table+' tbody:first').children("tr:last").after($tr);
					customize('class'+ increval,increval);
				}
				
				//$().addClass(
				function customize(idclass,incre)
				{
					nucleus('.'+idclass).addClass("clonerow");
				
					nucleus('.'+idclass+' div[0]').attr("id","select-customer-id-"+incre);
					nucleus("#select-customer-id-"+incre).hide();
					//nucleus("#s2id_customer_id_"+incre).hide();
					nucleus('.'+idclass).find('div:first').attr("id","select-customer-id-"+incre);
			nucleus('.'+idclass).find('#select-customer-id-'+incre).find('div').attr("id","s2id_customer_id_"+incre);
					nucleus('.'+idclass).find('select').html("");
					nucleus('.'+idclass).find('.product_id').attr("id","product_id_"+incre);
					nucleus('.'+idclass).find('select').attr("onChange","autoselectdata(this.value,'product_id_"+incre+"','select-description-"+incre+"','select-quantity-"+incre+"','select-unit-"+incre+"','select-rt-"+incre+"')");
					nucleus('.'+idclass).find('.quantity').attr("onKeyUp","autoquantitydata(this.value,'select-description-"+incre+"','select-quantity-"+incre+"','select-unit-"+incre+"','select-rt-"+incre+"')");
					nucleus('.'+idclass).find('.unitprice').attr("onKeyUp","autounitdata(this.value,'select-description-"+incre+"','select-quantity-"+incre+"','select-unit-"+incre+"','select-rt-"+incre+"')");
					nucleus('.'+idclass).find('.rowtotal').attr("onKeyUp","autorowdata(this.value,'select-description-"+incre+"','select-quantity-"+incre+"','select-unit-"+incre+"','select-rt-"+incre+"')");
					nucleus('.'+idclass).bind('click',data(incre));
					//nucleus('.'+idclass).find('.select2-container .minimum-select').attr("id","s2id_customer_id_"+incre);
					//nucleus('.'+idclass+' div[0]').attr("id","select-customer-id-"+incre);
					//alert(newdaat);	
					//
				}
				
				
			</script>
                
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		<div id="habijabi"></div>
    </body>
</html>
