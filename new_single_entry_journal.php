<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
        <script>
			function SaveCustomer()
			{
				var date=$('.datepicker').val();
				var memo=$('.span12 .memo').val();
				var shop_id=$('#shop_id').val();
				
				var option_a=$('#option_a').val();
				var opt_a_d=$('#opt_a_d').val();
				var opt_a_debit=$('#opt_a_debit').val();
				var opt_a_cradit=$('#opt_a_cradit').val();
				
				var option_b=$('#option_b').val();
				var opt_b_d=$('#opt_b_d').val();
				var opt_b_debit=$('#opt_b_debit').val();
				var opt_b_cradit=$('#opt_b_cradit').val();
				
				if(date!="" && memo!="" && shop_id!="" && option_a!="" && opt_a_d!="" && opt_a_debit!="" && opt_a_cradit!="" && option_b!="" && opt_b_d!="" && opt_b_debit!="" && opt_b_cradit!="")
				{
					
					if(journal_entry_check()==0)
					{
						$.jGrowl('Invalid Journal Entry.', { sticky: false, theme: 'growl-error', header: 'Invalid Journal Entry!' });
					}
					else
					{
					
						//console.log(date+" "+memo+" "+shop_id+" "+option_a+" "+opt_a_d+" "+opt_a_debit+" "+opt_a_cradit+" "+option_b+" "+opt_b_d+" "+opt_b_debit+" "+opt_b_cradit);
						$.post("lib/single_entry_journal.php",{'st':1,
						'date':date,
						'memo':memo,
						'shop_id':shop_id,
						'option_a':option_a,
						'opt_a_d':opt_a_d,
						'opt_a_debit':opt_a_debit,
						'opt_a_cradit':opt_a_cradit,
						
						'option_b':option_b,
						'opt_b_d':opt_b_d,
						'opt_b_debit':opt_b_debit,
						'opt_b_cradit':opt_b_cradit},function(data)
						{
							if(data==1)
							{
							   clear();
							   $.jGrowl('Saved, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
							}
							else if(data==2)
							{
							  $.jGrowl('Failed, Already Exists.', { sticky: false, theme: 'growl-warning', header: 'Error!' });
							}
							else
							{
							   $.jGrowl('Failed, Try Again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
							}
						});
					}
				}
				else
				{
					$.jGrowl('Failed, Some Field is Empty.', { sticky: false, theme: 'growl-error', header: 'Error!' });
				}
			}
			
			function clear()
			{
				$('.datepicker').val("");
				$('.span12 .memo').val("");
				$('#shop_id').val("");
				
				
				$('#opt_a_d').val("");
				$('#opt_a_debit').val("0.00");
				$('#opt_a_cradit').val("0.00");
				
				
				$('#opt_b_d').val("");
				$('#opt_b_debit').val("0.00");
				$('#opt_b_cradit').val("0.00");
				
				$('#totaldebit').val("0.00");
				$('#totalcradit').val("0.00");
			}
			
		
			function journal_entry_check()
			{
				var debit = 0.0;
				var cradit = 0.0;
				var aa;
				var bb;
				  
               $(".test").each ( function() {
                   debit += parseFloat ( $(this).val().replace(/\s/g,'').replace(',','.'));
                });
                //alert ( debit );
				aa=document.getElementById('totaldebit').value= debit;
				
				 $(".tests").each ( function() {
                   cradit += parseFloat ( $(this).val().replace(/\s/g,'').replace(',','.'));
                });
				 //alert ( cradit );
                bb=document.getElementById('totalcradit').value= cradit;
				
				if(document.getElementById('totaldebit').value != document.getElementById('totalcradit').value) 
				{
					return 0;
				}
				else
				{
					return 1;	
				}
			}
			
		</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Single Entry New Journal Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                          
                                                    <blockquote style="margin-top:-20px;">
                                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                                    </blockquote>
                                            
                                          
                                          <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span12">Payment Date *</label>
                                                    <input type="text" name="date" class="datepicker" placeholder="Payment Date" />
                                                </div>                                               
                                            </div>
                                            
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span12">Journal tracking ID/Name/No *</label>
                                                    <input type="text" name="date" class="span12 memo" placeholder="Journal track ID/Name/No." />
                                                </div>                                               
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="span4">
                                                
												
                                                <div class="control-group">
                                                    <label class="span12">Choose Shop *</label>
                                                    <select name="shop_id" data-placeholder="Select Shop" class="select-search" id="shop_id" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php
														   $sql2=$obj->SelectAll("branch");
														   if(!empty($sql2))
														   foreach($sql2 as $rows)
														   {
														   ?>      
															<option value="<?php echo $rows->branch_id; ?>"><?php echo $rows->name; ?></option>
															<?php
														   }
															?>
                                                       </select>
                                                </div>
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->












										<div class="row-fluid block">
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="table-overflow">
                                                <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Account</th>
                                                                <th>Description/Memo</th>
                                                                <th style="text-align:right;">Debit</th>
                                                                <th style="text-align:right;">Credit</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        	<tr>
                                                            	<td>
                                                                <select class="required small" style="margin-left:20px; width:200px;" name="option_a[]"  id="option_a">
                                                                    <option selected>--------------------------</option>
                                                                    <optgroup label="Asset">
                                                                    <?php
                                                                    $sql1=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
																	if(!empty($sql1))
                                                                    foreach($sql1 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Liability / Cradit Card">
                                                                    <?php
                                                                    $sql2=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='2'");
																	if(!empty($sql2))
                                                                    foreach($sql2 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Income">
                                                                    <?php
                                                                    $sql3=$obj->FlyQuery("SELECT id,head_sub_list_name  FROM account_module_ladger_list_properties WHERE main_head_id='3'");
																	if(!empty($sql3))
                                                                    foreach($sql3 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Expense">
                                                                    <?php
                                                                    $sql4=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='4'");
																	if(!empty($sql4))
                                                                    foreach($sql4 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Equity">
                                                                    <?php
                                                                    $sql5=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='6'");
																	if($sql5)
                                                                    foreach($sql5 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    
                                                                </select>
                                                                </td>
                                                            	<td><input type="text" class="required small" style="width:300px;" name="opt_a_d[]" id="opt_a_d"></td>
                                                                <td style="text-align:right;">
                                                                
                                                                <input type="text" class="test"  style="width:100px; text-align:right;font-weight:bolder; " name="opt_a_debit[]" id="opt_a_debit" value="0">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text"  class="tests" style="width:100px; text-align:right;font-weight:bolder; " name="opt_a_cradit[]" id="opt_a_cradit" value="0">
                                                                </td>                                                            </tr>
                                                            <tr>
                                                            	<td>
                                                                <select class="required small" style="margin-left:20px; width:200px;" name="option_a[]"  id="option_b">
                                                                    <option selected>--------------------------</option>
                                                                    <optgroup label="Asset">
                                                                    <?php
                                                                    $sql1=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
																	if(!empty($sql1))
                                                                    foreach($sql1 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Liability / Cradit Card">
                                                                    <?php
                                                                    $sql2=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='2'");
																	if(!empty($sql2))
                                                                    foreach($sql2 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Income">
                                                                    <?php
                                                                    $sql3=$obj->FlyQuery("SELECT id,head_sub_list_name  FROM account_module_ladger_list_properties WHERE main_head_id='3'");
																	if(!empty($sql3))
                                                                    foreach($sql3 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Expense">
                                                                    <?php
                                                                    $sql4=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='4'");
																	if(!empty($sql4))
                                                                    foreach($sql4 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    <optgroup label="Equity">
                                                                    <?php
                                                                    $sql5=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='6'");
																	if($sql5)
                                                                    foreach($sql5 as $row)
                                                                    {
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    </optgroup>
                                                                    
                                                                </select>
                                                                </td>
                                                            	<td><input type="text" class="required small" style="width:300px;" name="opt_a_d[]" id="opt_b_d"></td>
                                                                <td style="text-align:right;">
                                                                
                                                                <input type="text" class="test"  style="width:100px; text-align:right;font-weight:bolder; " name="opt_a_debit[]" id="opt_b_debit" onKeyUp="addNumbers()" value="0">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text"  class="tests" style="width:100px; text-align:right;font-weight:bolder; " name="opt_a_cradit[]" id="opt_b_cradit" onKeyUp="addNumbers_cradit()" value="0">
                                                                </td>                                                            </tr>
                                                            <tr>
                                                            	<td>
                                                                
                                                                </td>
                                                            	<td align="right" valign="middle" style="font-weight:bolder;"><strong>Total</strong></td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totaldebit" id="totaldebit">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalcradit" id="totalcradit">
                                                                </td>                                                            </tr>        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>




									<div class="row-fluid block">
                                          
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span5">
                                            	                                          
                                            </div>
                                            
                                            <div class="span6">
                                            	                                         
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="spa1">
                                                
												
                                               <div class="control-group">
                                                    <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                    <i class="icon-plus-sign"></i> Save Record </button>
                                                </div>
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>
                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->




                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                                  



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
