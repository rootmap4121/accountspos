<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
		
        <script>
		    
			function deleteR(id)
			{
				var c = confirm("are you sure to Delete this Journal Record ?.");
				if(c)
				{
					$('#tr'+id).hide('slow');
					$.post("lib/single_entry_journal.php",{'st':3,'id':id},function(data)
					{
						if(data==1)
						{
							$.jGrowl('Data, Successfully Delete.', { sticky: false, theme: 'growl-success', header: 'success!' });
						}
						else
						{
							$.jGrowl('Failed, Please Try Again.', { sticky: false, theme: 'growl-error', header: 'Failed!' });
						}
					});
				}
			}
			
			function view(id)
			{
				var c = confirm("are you sure to view this Journal Record ?.");
				if(c)
				{
					$('#tr'+id).css("background","#999");
					window.location.replace("./edit_single_entry_journal.php?journal="+id);	
				}
				else
				{
					$('#tr'+id).css("background","none");	
				}
			}
		</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Journal List Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                		<!-- Content Start from here customized -->
										<!-- General form elements -->
										<div class="row-fluid block">
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Journal ID</th>
                                                                <th>Description</th>
                                                                <th style="text-align:right; padding-right:25px;">Amount</th>
                                                                <th>Date</th>
                                                                <th style="text-align:center;">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
																<!--<tr><td colspan="5"><?php echo $chk; ?> Journal Transaction Record Found </td></tr>-->
																<?php
																$sql1=$obj->FlyQuery("select a.id,a.jd_id,a.jd,a.jddate,sum(b.debit) as total from account_module_journal_description as a left join account_module_ladger as b on b.link_id=a.link_id 
GROUP BY a.link_id");
																$i=1;
																if(!empty($sql1))
																foreach($sql1 as $row)
																{
																?>
																	<tr class="em" id="tr<?php echo $row->id; ?>">
																		<td> <?php echo $i; ?> </td>
																		<td align="left"><?php echo $row->jd_id; ?></td>
																		<td align="left"><?php echo $row->jd; ?></td>
																		
																		<td style="text-align:right;"> 
																		<?php  
																		echo number_format($row->total,2);
																		?>
																		</td>
                                                                        <td><?php echo $row->jddate; ?> </td>
																		<td align="center" valign="middle">
																		<a href="#" onClick="view(<?php echo $row->id; ?>)"><i class="icon-list"></i></a>
																		<a href="#" onClick="deleteR(<?php echo $row->id; ?>)"><i class="icon-trash"></i></a> 
																		</td>
																	</tr>
																<?php
																$i++;
																}
															?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>









                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                                  



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
			ddtreemenu.createTree("treemenu1", true)
			ddtreemenu.createTree("treemenu2", true)
			ddtreemenu.createTree("treemenu3", true)
			ddtreemenu.createTree("treemenu4", true)
			ddtreemenu.createTree("treemenu5", true)
			ddtreemenu.createTree("treemenu6", true)
			ddtreemenu.createTree("treemenu7", false)
			</script>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
