<?php
include('class/auth.php');
include('./querysheld/payment.php');
$obj_payment = new Payment();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_payment->admin($from, $to);
    } else {
        $sql = $obj_payment->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_payment->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_payment->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_payment->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_payment->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_payment->manager($from, $to, $input_by);
    } else {
        $sql = $obj_payment->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_payment->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_payment->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Paid Payment Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("lib/paid_payment.php", {'st': 2, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            $.jGrowl('Paid Record, Successfully Delete.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        }
                        else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }
        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Paid Payment List Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Paid Payment</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Voucher. ID</th>
                                                        <th>Supplier</th>
                                                        <th>Paid From Ledger</th>
                                                        <th>Total Amount</th>
                                                        <th>Paid By</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
//                                                    $sql = $obj->FlyQuery("SELECT 
//                                                    a.id,
//                                                    concat(b.fname,' ',b.lname) as customer_name,
//                                                    a.vendor_id,
//                                                    a.date,
//                                                    a.pa,
//                                                    p.head_sub_list_name as ledger,
//                                                    a.amount,
//                                                    s.name as receiver 
//                                                    FROM account_module_bill_payment as a 
//                                                    LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
//                                                    LEFT JOIN store as s ON s.id=a.input_by 
//                                                    LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa");
                                                    $quantity=0;
                                                    $total_amount=0;
                                                    $i = 1;
                                                    if (!empty($sql))
                                                        foreach ($sql as $row):
                                                            ?>
                                                            <tr id="tr<?php echo $row->id; ?>">
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $row->id; ?></td>
                                                                <td><?php echo $row->customer_name; ?></td>
                                                                <td><?php echo $row->ledger; ?></td>
                                                                <td><?php echo $obj->amountconvert($row->amount); ?></td>
                                                                <td><?php echo $row->receiver; ?></td>
                                                                <td><?php echo $obj->dates($row->date); ?></td>
                                                                <td>
                                                                    <a href="#" onClick="deleteR(<?php echo $row->id; ?>)"><i class="icon-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $quantity+=1;
                                                            $total_amount+=$row->amount;
                                                            $i++;
                                                        endforeach;
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3"></td>
                                                        <td style="font-weight: bolder; text-align: right;">Total Amount = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_amount); ?></td>
                                                        <td colspan="2" style="font-weight: bolder; text-align: right;">Total Quantity = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($quantity); ?></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
