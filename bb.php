<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Balance Sheet </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   

                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td width="463" align="left" valign="middle"><h4><strong>Asset</strong></h4></td>
                                                        <td width="595" align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Bank</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (SELECT ladger_id,SUM(debit) as debit,SUM(cradit) as cradit FROM account_module_ladger GROUP BY ladger_id) as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='1')");
                                                    $cogs = 0;
                                                    $total_revenue = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit=$cogsexpense->debit;
                                                        $cradit=$cogsexpense->cradit;
                                                        $netm = $cogsexpense->total;
                                                                if ($netm != 0) {
                                                                    ?>
                                                                    <tr>
                                                                        <td valign="middle" align="left">
                                                                            <a href="viewledger.php?ladger_id=<?php echo $inid; ?>" style="text-decoration:none; font-weight:normal;"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                        </td>
                                                                        <td align="right" valign="middle" >
                                                                            <span style="text-decoration:none; font-weight:normal;">
                                                                                <?php
                                                                                $getch = substr($netm, 0, 1);
                                                                                if ($getch != '-') {
                                                                                    echo number_format($netm, 2);
                                                                                } else {
                                                                                    echo number_format(substr($netm, 1, 200), 2);
                                                                                }
                                                                                $total_revenue+=$netm;
                                                                                ?>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            
                                                        
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Bank</strong></td>
                                                        <td align="right" valign="middle"><strong><u><?php
                                                                    //echo $total_revenue;
                                                                    $chktr = substr($total_revenue, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        $totalforbank = $total_revenue;
                                                                        echo number_format($totalforbank, 2);
                                                                    } else {
                                                                        $totalforbank = substr($total_revenue, 1, 200);
                                                                        echo number_format($totalforbank, 2);
                                                                    }
                                                                    ?></u></strong></td>
                                                    </tr>
                                                    <!--bank query end -->
                                                    <!--current asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Current Asset</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Inter Project Current Asset</td>
                                                        <td align="right" valign="middle" ><?php echo number_format($ipca_current_asset, 2); ?></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (SELECT ladger_id,SUM(debit) as debit,SUM(cradit) as cradit FROM account_module_ladger GROUP BY ladger_id) as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='2')");
                                                    $currentasset = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit=$cogsexpense->debit;
                                                        $cradit=$cogsexpense->cradit;
                                                        
                                                                $netm1 = $cogsexpense->total;
                                                                if ($netm1 != 0) {
                                                                    ?>

                                                                    <tr>
                                                                        <td valign="middle" align="left">
                                                                            <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                        </td>
                                                                        <td align="right" valign="middle" >
                                                                            <?php
                                                                            $getch = substr($netm1, 0, 1);
                                                                            if ($getch != '-') {
                                                                                echo number_format($netm1, 2);
                                                                            } else {
                                                                                echo number_format(substr($netm1, 1, 200), 2);
                                                                            }
                                                                            $currentasset+=$netm1;
                                                                            ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            
                                                        
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Current Asset</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($currentasset, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        echo number_format($currentassettotal = $currentasset + $ipca_current_asset, 2);
                                                                    } else {
                                                                        echo number_format($currentassettotal = substr($currentasset, 1, 200) + $ipca_current_asset, 2);
                                                                    }
                                                                    ?></u>
                                                            </strong></td>
                                                    </tr>
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Fixed Asset</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (SELECT ladger_id,SUM(debit) as debit,SUM(cradit) as cradit FROM account_module_ladger GROUP BY ladger_id) as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='3')");
                                                    $fixedasset = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit=$cogsexpense->debit;
                                                        $cradit=$cogsexpense->cradit;
                                                        
                                                                $netm2 = $cogsexpense->total;
                                                                if ($netm2 != 0) {
                                                                    ?>
                                                                    <tr>
                                                                        <td valign="middle" align="left"><a href="viewledger.php?ladger_id=<?php echo $inid; ?>"></a><a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a></td>
                                                                        <td align="right" valign="middle" ><?php
                                                                            $getch = substr($netm2, 0, 1);
                                                                            if ($getch != '-') {
                                                                                echo number_format($netm2, 2);
                                                                            } else {
                                                                                echo number_format(substr($netm2, 1, 200), 2);
                                                                            }
                                                                            $fixedasset+=$netm2;
                                                                            ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            
                                                        
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Fixed Asset</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($fixedasset, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        $fixedassettotal = $fixedasset;
                                                                        echo number_format($fixedassettotal, 2);
                                                                    } else {
                                                                        $fixedassettotal = substr($fixedasset, 1, 200);
                                                                        echo number_format($fixedassettotal, 2);
                                                                    }
                                                                    ?></u>
                                                            </strong></td>
                                                    </tr>
                                                    <!--liability section start -->
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Asset</strong></td>
                                                        <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><strong><u><?php echo number_format($totalasset = $total_revenue + $currentasset + $ipca_current_asset + $fixedasset, 2); ?></u></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><h4><strong>Liability</strong></h4></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Current Liability</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Inter Current Project Account</td>
                                                        <td align="right" valign="middle" ><?php echo number_format($ipca_current_liability, 2); ?></td>
                                                    </tr>
                                                    <?php
                                                    $ld = $obj->FlyQuery("SELECT * FROM account_module_ladger_list_properties WHERE main_head_id='2' AND head_sub_list_id!='382'");
                                                    $liabilitycurrent = 0;
                                                    foreach ($ld as $row) {
                                                        $inid = $row->id;
                                                        $getdutotal = $obj->FlyQuery("SELECT * FROM account_module_ladger AND ladger_id!='$inid' AND ladger_id!='41'");
                                                        $debit = 0;
                                                        $cradit = 0;
                                                        if (!empty($getdutotal))
                                                            foreach ($getdutotal as $due) {
                                                                $debit+=$due->debit;
                                                                $cradit+=$due->cradit;
                                                            }
                                                        $netm3 = $debit - $cradit;
                                                        if ($netm3 != 0) {
                                                            ?>

                                                            <tr>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                </td>
                                                                <td align="right" valign="middle" ><?php
                                                                    $getch = substr($netm3, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo number_format($netm3, 2);
                                                                    } else {
                                                                        echo number_format(substr($netm3, 1, 200), 2);
                                                                    }
                                                                    $liabilitycurrent+=$netm3;
                                                                    ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total for Current Liability</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($liabilitycurrent, 0, 1);
                                                                    if ($chktr != "-") {
                                                                        echo number_format($liabilitycurrenttotal = $liabilitycurrent, 2);
                                                                    } else {
                                                                        echo number_format($liabilitycurrenttotal = substr($liabilitycurrent, 1, 200), 2);
                                                                    }
                                                                    ?>
                                                                </u></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Liabilities</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $sdh = substr($liabilitycurrent, 0, 1);

                                                                    $absliablity = substr($liabilitycurrent, 1, 200);

                                                                    if ($sdh == "-") {
                                                                        $ttliabilities = $absliablity + $ipca_current_liability;
                                                                    } else {
                                                                        $ttliabilities = $liabilitycurrent + $ipca_current_liability;
                                                                    }
                                                                    echo $ttliabilities;
                                                                    //if($sdh)
                                                                    //$ipca_current_liability;
                                                                    //echo $liabilitycurrent;								
                                                                    ?>
                                                                </u></strong></td>
                                                    </tr>

                                                    <!--liability section start -->
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"> 
                                                            <h4><strong>Equity</strong></h4></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->SelectAllByID_Multiple("account_module_head_sub_list", array("list_of_head_accounts_id" => 5));
                                                    $totalequity = 0;
                                                    if (!empty($chkfourcountled))
                                                        foreach ($chkfourcountled as $cogsexpense) {
                                                            $cogsid = $cogsexpense->id;
                                                            $chkvisibleledgeraccount = $obj->exists_multiple("account_module_ladger_list_properties", array("main_head_id" => 5, "head_sub_list_id" => $cogsid));
                                                            if ($chkvisibleledgeraccount != 0) {

                                                                $ld = $obj->SelectAllByID_Multiple("account_module_ladger_list_properties", array("main_head_id" => 5, "head_sub_list_id" => $cogsid));
                                                                foreach ($ld as $row) {

                                                                    $inid = $row->id;
                                                                    $getdutotal = $obj->SelectAllByID_Multiple("account_module_ladger", array("ladger_id" => $inid));
                                                                    $debit = 0;
                                                                    $cradit = 0;
                                                                    if (!empty($getdutotal))
                                                                        foreach ($getdutotal as $due) {
                                                                            $debit+=$due->debit;
                                                                            $cradit+=$due->cradit;
                                                                        }
                                                                    $netm4 = $debit - $cradit;
                                                                    if ($netm4 != 0) {
                                                                        ?>
                                                                        <tr>
                                                                            <td valign="middle" align="left">
                                                                                <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                            </td>
                                                                            <td align="right" valign="middle" ><strong>
                                                                                    <?php
                                                                                    $getch = substr($netm4, 0, 1);
                                                                                    if ($getch != '-') {
                                                                                        echo number_format($netm4, 2);
                                                                                    } else {
                                                                                        echo number_format(substr($netm4, 1, 200), 2);
                                                                                    }
                                                                                    $totalequity+=$netm4;
                                                                                    ?></strong></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ?>


                                                    <tr>
                                                        <td valign="middle" align="left">Retained Earnings</td>
                                                        <td align="right" valign="middle">
                                                            <?php
                                                            $preyear = date('Y');
                                                            $previousyear = $preyear - 1;
                                                            $chkpredata = $obj->exists_multiple("account_module_income_statement", array("year" => $previousyear));
                                                            if ($chkpredata == 0) {
                                                                echo number_format("0", 2);
                                                            } else {
                                                                $querypre = $obj->SelectAllByVal("account_module_income_statement", "year", $previousyear, "amount");
                                                                //$fetpre=mysql_fetch_array($querypre);
                                                                echo number_format($querypre, 2);
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Current Year Earnings</td>
                                                        <td align="right" valign="middle">
                                                            <!--income statement result script-->
                                                            <?php
                                                            $inyear = date('Y');
                                                            //$netprofit_query=$obj->SelectAllByID_Multiple("account_module_income_statement",array("year"=>$inyear));
                                                            //$fetnet=mysql_fetch_array($netprofit_query);
                                                            $netprofit = $obj->SelectAllByVal("account_module_income_statement", "year", $inyear, "amount");
                                                            $chknp = substr($netprofit, 0, 1);
                                                            $chknpp = substr($netprofit, 1, 200);

                                                            if ($chknp != "-") {
                                                                echo number_format($netprofit, 2);
                                                            } else {
                                                                echo "(" . number_format(substr($netprofit, 1, 200), 2) . ")";
                                                            }
                                                            ?>
                                                            <!--income statement result calculate end-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Equity</strong></td>
                                                        <td align="right" valign="middle"><u><strong>
                                                                    <?php
                                                                    $feq = $totalequity + $netprofit;

                                                                    $chknp1 = substr($feq, 0, 1);

                                                                    if ($chknp1 != "-") {
                                                                        echo number_format($feq, 2);
                                                                    } else {
                                                                        echo "(" . number_format(substr($feq, 1, 200), 2) . ")";
                                                                    }
                                                                    ?>
                                                                </strong></u></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr><td valign="middle" align="left"><strong>Total Liabilities and Equity</strong></td>
                                                        <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><strong><u><?php
                                                                        $totalasset = $ttliabilities + $feq;
                                                                        echo number_format($totalasset, 2);

                                                                        $year = date('Y');
                                                                        $date = date('Y-m-d');
                                                                        $chkbalancesheet = $obj->exists_multiple("account_module_balancesheet", array("year" => $year));
                                                                        if ($chkbalancesheet != 0) {
                                                                            $obj->update("account_module_balancesheet", array("year" => $year, "totalasset" => $totalasset, "totalliability" => $liabilitycurrent, "totalequity" => $feq,"branch_id"=>$input_by, "date" => $date));
                                                                        } else {
                                                                            $obj->insert("account_module_balancesheet", array("totalasset" => $totalasset, "totalliability" => $liabilitycurrent, "totalequity" => $feq, "year" => $year,"branch_id"=>$input_by, "date" => $date));
                                                                        }
                                                                        ?></u></strong></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->
                                        <!-- /selects, dropdowns -->
                                    </div>
                                    <!-- /general form elements -->     
                                    <div class="clearfix"></div>
                                    <!-- Default datatable -->
                                    <!-- /default datatable -->
                                    <!--tab 1 content start from here-->  
                                </div>
                            </div>
                            <!-- General form elements -->
                            <!-- /general form elements -->
                            <div class="clearfix"></div>
                            <!-- Default datatable -->
                            <!-- /default datatable -->
                            <!-- Content End from here customized -->
                            <div class="separator-doubled"></div> 
                        </div>
                        <!-- /content container -->
                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
