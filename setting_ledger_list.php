<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
         <script type="text/javascript" src="js/simpletreemenu.js"></script>
        <link href="css/simpletree.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        .em{}
        .em:hover{ background:#093; }
        </style>
        <style type="text/css">
			.label_edit_1{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_1:hover{ color:rgba(153,0,153,1) !important; }
			.label_1{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_1:hover{ color:rgba(255,102,0,1) !important; }
			
			.label_edit_2{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_2:hover{ color:rgba(153,0,153,1) !important; }
			.label_2{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_2:hover{ color:rgba(255,102,0,1) !important; }
			
			.label_edit_3{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_3:hover{ color:rgba(153,0,153,1) !important; }
			.label_3{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_3:hover{ color:rgba(255,102,0,1) !important; }
		</style>
        <script>
		function deleteledger(ledger_id,head_id)
		{
			var c = confirm("Are You Sure To Delete this Ledger & its all payment reord.");
			if(c)
			{
				
				if(ledger_id!="")
				{
					$.post("lib/coa.php",{'st':6,'ledger_id':ledger_id},function(data)
					{
						if(data==1)
						{
							reloadcoa(head_id);
							$('#sub_sub_head_'+ledger_id).hide('slow');
							$.jGrowl('Deleted, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
							
						}
						else
						{
							$.jGrowl('Failed, Please Reload page & try again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
						}
					});	
				}
				else
				{
					$.jGrowl('Failed, Please Reload page & try again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
				}
			}
			
		}
		
		function reloadcoa(head_id)
		{
			$('#loadledger'+head_id).html('<tr><td colspan="2" align="center"><img alt="" src="images/elements/loaders/6.gif"></td></tr>');
			$.post("lib/coa.php",{'st':7,'head_id':head_id},function(data){
				$('#loadledger'+head_id).html(data);
			});
		}
		
		function AddLedgerBook(ledger_id,head_id)
		{
			var c = confirm("are you sure to add this ledger in ledger book ?.");
			if(c)
			{
				$.jGrowl('Your data is processing....',{ sticky:false,theme:'growl-info',header:'Notification!' });
				
				$.post("lib/coa.php",{'st':8,'ledger_id':ledger_id,'head_id':head_id},function(data){
					if(data==2)
					{
						$.jGrowl('Failed, Ledger Already Exists in Ledger Book.',{ sticky:false,theme:'growl-warning',header:'Warning!' });
					}
					else if(data==1)
					{
						reloadcoa(head_id);
						$.jGrowl('Your ledger info, Successfully Saved.',{ sticky:false,theme:'growl-success',header:'Notification!' });	
					}
					else
					{
						$.jGrowl('Failed, You Reject to add ledger.',{ sticky:false,theme:'growl-error',header:'Warning!' });
					}
					
				});
			}
			else
			{
				$.jGrowl('Failed, You Reject to add ledger.',{ sticky:false,theme:'growl-error',header:'Warning!' });	
			}
		}
		
		</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Custom Ledger Book </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <div class="block span6">    
                                                              



                                </div>
                                <fieldset>

                                    <div class="row-fluid block"><a  data-toggle="modal" href="#ledger_box"  class="btn btn-info pull-right"><i class="icon-plus"></i> Add New Ledger</a>
                                        <div class="well row-fluid span10">
                                            <div class="tabbable">
                                                <!--start ul tabs -->
                                                
                                                <ul class="nav nav-tabs">
                                                    <?php
													$sqlhead=$obj->FlyQuery("SELECT id,head_title from account_module_list_of_head_accounts");
													if(!empty($sqlhead))
													$a=1;
													foreach($sqlhead as $row)
													{
													?>
														<li<?php if($a==1){ ?> class="active"<?php } ?>><a href="#tab<?php echo $row->id; ?>"  data-toggle="tab"><?php echo $row->head_title; ?></a></li>
													<?php
													$a++;
													}
													?>
                                                    
                                                </ul>
                                                <!--end ul tabs -->  
                                                <!--start data tabs --> 
                                                <div class="tab-content">
                                                    <?php
													$sqlheadlist=$obj->FlyQuery("SELECT id,head_title,tab_name from account_module_list_of_head_accounts");
													if(!empty($sqlheadlist))
													$b=1;
													foreach($sqlheadlist as $head)
													{
													?>
                                                    <div class="tab-pane<?php if($b==1){ ?> active<?php } ?>" id="tab<?php echo $head->id; ?>">
                                                        <!--tab 1 content start from here-->
                                                        <h5>
                                                        <table class="table">
                                                            <thead class="btn-success" style="color:#333;">
                                                                <tr>
                                                                    <th  colspan="2" align="left"><?php echo $head->head_title; ?> Item Listed</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="loadledger<?php echo $head->id; ?>">
                                                            <?php
                                                            $sqld1=$obj->SelectAllByID_Multiple("account_module_ladger_list_properties",array("main_head_id"=>$head->id));
                                                            if(!empty($sqld1))
                                                            foreach($sqld1 as $row)
                                                            {
                                                            ?>
                                                                <tr>
                                                                    <td><?php echo $row->head_sub_list_name; ?></td>
                                                                    <td width="20%">
                                                                    <?php 
																	if($row->branch_id==$shop_id){
																	?>
                                                                    <a onClick="javascript:deleteledger(<?php echo $row->id; ?>,<?php echo $head->id; ?>)" class='btn btn-danger' href='#'>Delete</a> 
                                                                    <?php 
																	}
																	 ?>
                                                                    </td>
                                                                </tr>
                                                            <?php
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                            							</h5>
                                                        <!--tab 1 content start from here-->
                                                    </div>
                                                    <?php
													$b++;
													}
													?>
                                                    
                                                    
                                                </div>
                                                <!--End data tabs -->   
                                            </div>
                                        </div>
                                        <!-- General form elements -->


                                        <!--modal ledger_box start here-->
                                        <div id="ledger_box" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form class="form-horizontal" method="post" action="">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h5 id="myModalLabel"> <i class="icon-plus"></i> Add New Ledger</h5>
                                                </div>
                                                <div class="modal-body">
                                            <div class="tabbable">
                                                <!--start ul tabs -->
                                                
                                                <ul class="nav nav-tabs">
                                                    <?php
													$sqlhead=$obj->FlyQuery("SELECT id,head_title from account_module_list_of_head_accounts");
													if(!empty($sqlhead))
													$a=1;
													foreach($sqlhead as $row)
													{
													?>
														<li<?php if($a==1){ ?> class="active"<?php } ?>><a href="#acc<?php echo $row->id; ?>"  data-toggle="tab"><?php echo $row->head_title; ?></a></li>
													<?php
													$a++;
													}
													?>
                                                    
                                                </ul>
                                                <!--end ul tabs -->  
                                                <!--start data tabs --> 
                                                <div class="tab-content">
                                                    <?php
													$sqlheadlist=$obj->FlyQuery("SELECT id,head_title,tab_name from account_module_list_of_head_accounts");
													if(!empty($sqlheadlist))
													$b=1;
													foreach($sqlheadlist as $head)
													{
													?>
                                                    <div class="tab-pane<?php if($b==1){ ?> active<?php } ?>" id="acc<?php echo $head->id; ?>">
                                                        <!--tab 1 content start from here-->
                                                        <h4 style="margin-top:0px; margin-bottom:10px; padding-bottom:10px; border-bottom:2px #333333 solid;">List of <?php echo $head->head_title; ?></h4>
                                                        <h5 style="height:400px; width:auto; overflow:scroll;">
                                                        <ul id="treemenu<?php echo $head->id; ?>" class="treeview">
                                                        	<?php
                                                            $sql3=$obj->FlyQuery("SELECT id,head_title FROM ".$head->tab_name);
                                                            if(!empty($sql3))
															foreach($sql3 as $rows){ ?>
                                                            <li><?php echo $rows->head_title; ?>
                                                            <?php
                                                            $sql4=$obj->FlyQuery("SELECT id,head_title,status FROM account_module_head_list WHERE list_of_head_accounts_id='".$head->id."' AND list_of_sub_head_accounts_id='".$rows->id."'");
															if(!empty($sql4))
															{
																?>
                                                                <ul>
                                                                <?php
																foreach($sql4 as $hlled){
																?>
                                                                <li><a href="#"><?php echo $hlled->head_title; ?></a>
                                                                <?php 
																$sql5=$obj->FlyQuery("SELECT id,head_title,branch_id,status FROM account_module_head_sub_list WHERE list_of_head_accounts_id='".$head->id."' AND list_of_sub_head_accounts_id='".$rows->id."' AND list_of_sub_head_list_id='".$hlled->id."'");
																if(!empty($sql5)){
																?>
                                                                <ul>
                                                                <?php 
																foreach($sql5 as $subhl){
																?>
                                                                	<li id="sub_sub_head_<?php echo $subhl->id; ?>">
                                                                    <a onClick="javascript:AddLedgerBook(<?php echo $subhl->id; ?>,<?php echo $head->id; ?>)"  href="#"><?php echo $subhl->head_title; ?></a> 																						
                                                 </li>
                                                                <?php 
																}
																?>    
                                                                </ul>
                                                                <?php 
																}
																?>
                                                                </li>
                                                                <?php
																}
																?>
                                                                </ul>
                                                                <?php
															}
															?>
                                                            </li>
                                                            <?php } ?>   
                                                        </ul>
                            							</h5>
                                                        <!--tab 1 content start from here-->
                                                    </div>
                                                    <?php
													$b++;
													}
													?>
                                                <!--End data tabs -->   
                                            </div>
                                        </div>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                </div>   
                                                
                                                <div class="modal-footer" id="ledger_save_box" style="display:none;">
                                                	<button type="button" onClick="saveledger()"  class="btn btn-info"  name="save_ledger"  id="save_ledger">Save New Ledger </button>
                                                </div>
                                                                                             
                                                </form>
                                        </div>
                                        <!--modal ledger_box end here-->
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                </fieldset>                     



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
			ddtreemenu.createTree("treemenu1", true)
			ddtreemenu.createTree("treemenu2", true)
			ddtreemenu.createTree("treemenu3", true)
			ddtreemenu.createTree("treemenu4", true)
			ddtreemenu.createTree("treemenu5", true)
			ddtreemenu.createTree("treemenu6", true)
			ddtreemenu.createTree("treemenu7", false)
			</script>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
