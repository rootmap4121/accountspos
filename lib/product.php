<?php 
include('../class/auth.php');
extract($_POST);
if($st==1)
{
	if($obj->exists_multiple("account_module_product",array("id"=>$id))==0)
	{
		$productarray=array("id"=>$id,"name"=>"","price"=>"","description"=>"","quantity"=>"","status"=>2);	
	}
	else
	{
		$sqlproduct=$obj->FlyQuery("SELECT id,pname as name,price,description,quantity FROM account_module_product WHERE id='".$id."'");	
		if(!empty($sqlproduct))
		{
			$productarray=array("id"=>$id,"name"=>$sqlproduct[0]->name,"price"=>$sqlproduct[0]->price,"description"=>$sqlproduct[0]->description,"quantity"=>$sqlproduct[0]->quantity,"status"=>1);
			
		}
		else
		{
			$productarray=array("id"=>$id,"name"=>"","price"=>"","description"=>"","quantity"=>"","status"=>0);	
		}
	}
	
	echo json_encode($productarray);
	
}
elseif($st==2)
{
  	//sales invoice generate start here
	$date=$_POST['invoice_date'];
	$duedate=$_POST['paid_date'];
	
	$subheading=$_POST['subheading'];
	$currency=9;
	
	$footer="";
	$poso="";
	$notes=$_POST['memo'];
	$currentdate=date('Y-m-d');
	
	$link_id=time();
	$branch_id=$shop_id;
	
	$ladgername="Accounts Receivables"; //dr
	
	$ledgerid=$obj->SelectAllByVal("account_module_ladger_list_properties","head_sub_list_name",$ladgername,"id");
	$time=time();
	$invoicenew_id=$obj->insertAndReturnID("account_module_invoice",array("invoice_id"=>$time,"link_id"=>$link_id,"customer_id"=>$customer_id,"date"=>$date,"duedate"=>$duedate,"subheading"=>$subheading,"currency"=>$currency,"footer"=>$footer,"poso"=>$poso,"notes"=>$notes,"status"=>0,"branch_id"=>$branch_id,"input_by"=>$input_bys));
			
	$jdid=$invoicenew_id;
		
	foreach($_POST['product'] as $index=>$val) 
	{
		$itemid=$val['value'];
		$quantity=$_POST['quantity'][$index]['value'];
		$price=$_POST['unitprice'][$index]['value'];
		$tax=0;
		$rate=$obj->SelectAllByVal("account_module_newtax","id",$tax,"rate");
		$main_tax_id=$obj->SelectAllByVal("account_module_newtax","id",$tax,"main_id");
		$totalprice=$quantity*$price;
		$totalvat=$totalprice*$rate/100;
		$totalamount=$totalprice+$totalvat;
		
		$income_account_id=$obj->SelectAllByVal("account_module_product","id",$itemid,"in_ac_id");
		$invoice_id="S".$jdid;
		$obj->insert("account_module_ladger",array("ladger_id"=>$ledgerid,"link_id"=>$link_id,"invoice_id"=>$invoice_id,"ladger_date"=>$currentdate,"debit"=>$totalamount,"cradit"=>0,"branch_id"=>$branch_id));
		//dr value account reciveable
		$obj->insert("account_module_ladger",array("ladger_id"=>$income_account_id,"link_id"=>$link_id,"invoice_id"=>$invoice_id,"ladger_date"=>$currentdate,"debit"=>0,"cradit"=>$totalprice,"branch_id"=>$branch_id));
		//cr value Income Account
		$obj->insert("account_module_ladger",array("ladger_id"=>$main_tax_id,"link_id"=>$link_id,"invoice_id"=>$invoice_id,"ladger_date"=>$currentdate,"debit"=>0,"cradit"=>$totalvat,"branch_id"=>$branch_id));
		//cr value For Tax
		
		$obj->insert("account_module_invoice_detail",array("invoice_id"=>$jdid,"link_id"=>$link_id,"pid"=>$itemid,"quantity"=>$quantity,"price"=>$price,"tax_id"=>$tax,"tax_total"=>$totalvat,"subtotal"=>$totalprice,"totalamount"=>$totalamount,"status"=>0,"branch_id"=>$branch_id)); 
		
		if($obj->insert("account_module_stockout",array("sales_id"=>$jdid,"cid"=>$customer_id,"branch_id"=>$branch_id,"link_id"=>$link_id,"pid"=>$itemid,"quantity"=>$quantity,"sales_date"=>$date,"note"=>$notes,"input_by"=>$input_bys,"date"=>$currentdate,"status"=>1))==1)
		{
		  $obj->FlyQueryExcute("UPDATE account_module_product SET quantity=quantity-$quantity WHERE id='$itemid'");
		}  
	}

	echo 1;
	//sales invoice end here
}
elseif($st==3)
{
	//Sales Record Destroy
	$obj->delete("product",array("id"=>$id));
	echo 1;
	//Sales Record Destroy
}
elseif($st==4)
{
	//Sales Record
	$salesdata='';
	$sql=$obj->FlyQuery("select a.`id` AS `id`,
a.`barcode` AS `barcode`,
a.`name` AS `pname`,
a.`description` AS `description`,
ifnull(b.`head_sub_list_name`,0) AS `in_ac_id`,
ifnull(c.`head_sub_list_name`,0) AS `p_in_ac_id`,
a.`price_retail` AS `price`,
a.`price_cost` AS `p_price`,
a.`quantity` AS `quantity`,
a.`store_id` AS `branch_id`,
a.`input_by` AS `input_by`,
a.`date` AS `date`,
a.`status` AS `status` from `product` as a 
LEFT JOIN account_module_product_sales_ledger as d on d.pid=a.id 
LEFT JOIN account_module_product_purchase_ledger as e on e.pid=a.id 
LEFT JOIN account_module_ladger_list_properties as b on b.id=d.ledger 
LEFT JOIN account_module_ladger_list_properties as c on c.id=e.ledger");
	$i=1;
	if(!empty($sql))
	foreach($sql as $row):
	
	$salesdata .='<tr id="tr'.$row->id.'">
		<td>'.$i.'</td>
		<td><a href="view_product.php?pid='.$row->id.'">'.$row->pname.'</a></td>
		<td>'.$row->description.'</td>
		<td>'.$row->quantity.'</td>   
		<td>'.$row->price.'</td>
		<td>'.$row->p_price.'</td>
                <td>';
        
                if($row->in_ac_id=="0")
                {
                    $salesdata .='<a class="btn btn-info" style="width:65px;" href="setting_product_sales_ledger.php?pid='.$row->id.'">Add Ledger</a>';
                }
                else 
                {
                    $salesdata .=$row->in_ac_id;
                }
                
                $salesdata .='</td><td>';
                
                if($row->p_in_ac_id=="0")
                {
                    $salesdata .='<a class="btn btn-info" style="width:65px;" href="setting_product_purchase_ledger.php?pid='.$row->id.'">Add Ledger</a>';
                }
                else 
                {
                    $salesdata .=$row->p_in_ac_id;
                }
                
                $salesdata .='</td>    
		<td>'.$row->input_by.'</td>
		<td>'.$obj->dates($row->date).'</td>
		';
		
		$salesdata .='</td>
		<td>
		<a href="#" onClick="deleteR('.$row->id.')"><i class="icon-trash" ></i></a>';		
		$salesdata .='</td></tr>';
	$i++; 
	endforeach;
	
	$salesd=array("status"=>1,"salesdata"=>$salesdata);
	echo json_encode($salesd);
	//Sales Record
}
elseif($st==5)
{
	$viewinvoice=$obj->FlyQuery("SELECT a.`id`,
a.`invoice_id`,
a.`link_id`,
concat(b.`fname`,' ',b.`lname`) as `customer_id`,
a.`date`,
a.`duedate`,
a.`subheading`,
a.`notes`,
(select SUM(`amount`) as `amount` FROM `account_module_invoice_payment` as `d` WHERE d.invoice_id=a.invoice_id) as `amount`,
c.`name` as `branch_id` FROM account_module_invoice as a 
left join account_module_customer as b on b.`id`=a.`customer_id` 
left join store as c on c.`id`=a.`branch_id` WHERE a.id='$view'");
	
	$customer=$viewinvoice[0]->customer_id;
	$subheading=$viewinvoice[0]->subheading;
	$shop_id=$viewinvoice[0]->branch_id;
	$invoicedate=$obj->dates($viewinvoice[0]->date);
	$paiddate=$obj->dates($viewinvoice[0]->duedate);
	$notes=$viewinvoice[0]->notes;
	$invoice_id=$viewinvoice[0]->invoice_id;
	$paidamount=$viewinvoice[0]->amount;
	$link_id=$viewinvoice[0]->link_id;
	$subtotal=0;
	$subquantity=0;
	$subprice=0;
	$d=1;
	$newtr='';
	
	$sqlinvoice_detail=$obj->FlyQuery("SELECT a.`pid`,b.`pname` as `name`,b.`description` as `description`,a.`quantity`,a.`price`,a.`subtotal` FROM `account_module_invoice_detail` as a
left JOIN account_module_product as b on b.`id`=a.`pid` WHERE a.`invoice_id`='$view'");
	foreach($sqlinvoice_detail as $detail):
		$newtr .='<tr>';
			$newtr .='<td>'.$d.'</td>';
			$newtr .='<td>'.$detail->name.'</td>';
			$newtr .='<td>'.$detail->description.'</td>';
			$newtr .='<td style="text-align:right;">'.number_format($detail->quantity,2).'</td>'; $subquantity+=$detail->quantity;
			$newtr .='<td style="text-align:right;">'.number_format($detail->price,2).'</td>'; $subprice+=$detail->price;
			$newtr .='<td style="text-align:right;">'.number_format($detail->subtotal,2).'</td>';  $subtotal+=$detail->subtotal;
		$newtr .='</tr>';
		$d++;
	endforeach;
	
	$newtrfoot='';
	$newtrfoot .='<tr>';
		$newtrfoot .='<td style="text-align:left;" rowspan="3" colspan="4">
		<strong>Invoice ID :</strong> '.$invoice_id.'<br>
		<strong>Track ID :</strong> '.$link_id.'<br>
		<strong>Total Quantity :</strong> '.number_format($subquantity,2).'<br>
		<strong>Total Unit Price Total :</strong> '.number_format($subprice,2).'<br>
		</td>';
		$newtrfoot .='<td style="text-align:right;"><strong>Sub Total Amount = </strong></td>';
		$newtrfoot .='<td style="text-align:right;">'.number_format($subtotal,2).'</td>';
	$newtrfoot .='</tr>';
	
	$newtrfoot .='<tr>';
		$newtrfoot .='<td style="text-align:right;"><strong>Paid Amount =</strong> </td>';
		$newtrfoot .='<td style="text-align:right;">'.number_format($paidamount,2).'</td>';
	$newtrfoot .='</tr>';
	
	$dueamount=$subtotal-$paidamount;
	
	$newtrfoot .='<tr>';
		$newtrfoot .='<td style="text-align:right;"><strong>Due Amount =</strong> </td>';
		$newtrfoot .='<td style="text-align:right;">'.number_format($dueamount,2).'</td>';
	$newtrfoot .='</tr>';
	
	$ss=array("status"=>1,
	"customer"=>$customer,
	"subheading"=>$subheading,
	"shop_id"=>$shop_id,
	"invoicedate"=>$invoicedate,
	"paiddate"=>$paiddate,
	"notes"=>$notes,
	"invoice_id"=>$invoice_id,
	"datatr"=>$newtr,"datafot"=>$newtrfoot);
	
	echo json_encode($ss);
	
}
else
{
	echo 0;	
}
?>














