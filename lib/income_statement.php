<?php

include('../class/auth.php');
extract($_POST);
if ($st == 1) {

    $year = date('Y');
    if ($obj->exists_multiple("account_module_income_statement", array("year" => $year)) != 0) {
        $income = $obj->FlyQuery("SELECT amount as netprofit,directexpense,totalincome,grossprofit,date FROM account_module_income_statement WHERE year='" . $year . "' ORDER BY date DESC");
        if (!empty($income)) {
            $newarray = array("status" => 1, "net_profit" => $obj->amountconvert($income[0]->netprofit), "directexpense" => $obj->amountconvert($income[0]->directexpense), "totalincome" => $obj->amountconvert($income[0]->totalincome), "grossprofit" => $obj->amountconvert($income[0]->grossprofit), "date" => $obj->dates($income[0]->date));
            echo json_encode($newarray);
        } else {
            $newarray = array("status" => 0, "net_profit" => 0, "directexpense" => 0, "totalincome" => 0, "grossprofit" => 0, "date" => $obj->dates("0000-00-00"));
            echo json_encode($newarray);
        }
    } else {
        $newarray = array("status" => 2, "net_profit" => 0, "directexpense" => 0, "totalincome" => 0, "grossprofit" => 0, "date" => $obj->dates("0000-00-00"));

        echo json_encode($newarray);
    }
} elseif ($st == 2) {

    $year = date('Y');
    if ($obj->exists_multiple("account_module_balancesheet", array("year" => $year)) != 0) {
        $income = $obj->FlyQuery("SELECT `totalasset`,`totalliability`,`totalequity` FROM `account_module_balancesheet` WHERE `year`='" . $year . "' ORDER BY date DESC");
        if (!empty($income)) {
            $newarray = array("status" => 1, "totalasset" => $obj->amountconvert($income[0]->totalasset), "totalliability" => $obj->amountconvert($income[0]->totalliability), "totalequity" => $obj->amountconvert($income[0]->totalequity), "date" => $obj->dates($income[0]->date));
            echo json_encode($newarray);
        } else {
            $newarray = array("status" => 2, "totalasset" => "0.00", "totalliability" => "0.00", "totalequity" => "0.00", "date" => $obj->dates("0000-00-00"));
            echo json_encode($newarray);
        }
    } else {
        $newarray = array("status" => 3, "totalasset" => "0.00", "totalliability" => "0.00", "totalequity" => "0.00", "date" => $obj->dates("0000-00-00"));
        echo json_encode($newarray);
    }
} elseif ($st == 3) {

    $income = $obj->FlyQuery("SELECT 
    IFNULL(SUM(a.debit),0) as debit,
    IFNULL(SUM(a.cradit),0) as cradit,
    IFNULL((SUM(a.debit)-SUM(a.cradit)),0) as total 
    FROM 
    account_module_ladger as a
    WHERE 
    ladger_id='25' GROUP BY ladger_id");
    if (!empty($income)) {
        $newarray = array("status" => 1, "receiveables" => $obj->amountconvert($income[0]->total), "date" => $obj->dates($income[0]->date));
        echo json_encode($newarray);
    } else {
        $newarray = array("status" => 2, "receiveables" => "0.00", "date" => $obj->dates("0000-00-00"));
        echo json_encode($newarray);
    }
} elseif ($st == 4) {

    $income = $obj->FlyQuery("SELECT 
    IFNULL(SUM(a.debit),0) as debit,
    IFNULL(SUM(a.cradit),0) as cradit,
    IFNULL((SUM(a.debit)-SUM(a.cradit)),0) as total 
    FROM 
    account_module_ladger as a
    WHERE 
    ladger_id='34' GROUP BY ladger_id");
    if (!empty($income)) {
        $newarray = array("status" => 1, "payable" => $obj->amountconvert($income[0]->total), "date" => $obj->dates($income[0]->date));
        echo json_encode($newarray);
    } else {
        $newarray = array("status" => 2, "payable" => "0.00", "date" => $obj->dates("0000-00-00"));
        echo json_encode($newarray);
    }
}
?>




