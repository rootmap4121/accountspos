<?php 
include('../class/auth.php');
extract($_POST);
if($st==1)
{
	$gettable=$obj->SelectAllByVal("account_module_list_of_head_accounts","id",$main_head,"tab_name");
	$sqlmainsubhead=$obj->FlyQuery("SELECT id,head_title FROM `".$gettable."`");
	if(!empty($sqlmainsubhead))
	{
		?>
        <option selected="selected" value=""></option>
        <?php
		foreach($sqlmainsubhead as $mainsubhead){
		?>
        <option value="<?php echo $mainsubhead->id; ?>"><?php echo $mainsubhead->head_title; ?></option>
        <?php
		}
	}
	else
	{
		echo 0;
	}
}
elseif($st==2)
{
	$sqlmainsubhead=$obj->FlyQuery("SELECT id,head_title FROM account_module_head_list WHERE list_of_head_accounts_id='".$main_head."' AND list_of_sub_head_accounts_id='".$sub_head."'");
	if(!empty($sqlmainsubhead))
	{
		
        $html='<option selected="selected" value=""></option>';
        foreach($sqlmainsubhead as $mainsubhead){
        	$html .='<option value="'.$mainsubhead->id.'">'.$mainsubhead->head_title.'</option>';
        }
		$array=array("content"=>$html);
		echo json_encode($array);
		
	}
	else
	{
		echo 0;
	}
}
elseif($st==3)
{
	$chkmain=$obj->exists_multiple("account_module_head_sub_list",array("head_title"=>$ledger));
	if($chkmain==0)
	{
		if($obj->insert("account_module_head_sub_list",array("list_of_head_accounts_id"=>$main_head,"list_of_sub_head_accounts_id"=>$main_sub_head,"list_of_sub_head_list_id"=>$sub_sub_head,"head_title"=>$ledger,"status"=>2,"branch_id"=>$shop_id))==1)
		{
			echo 1;
		}
		else
		{
			echo 0;	
		}
	}
	else
	{
		echo 2;
	}
}
elseif($st==4)
{
	if($obj->delete("account_module_head_sub_list",array("id"=>$ledger_id))==1)
	{
		echo 1;
	}
	else
	{
		echo 0;	
	}
}
elseif($st==5)
{
	//reload data
	
	$gettable=$obj->SelectAllByVal("account_module_list_of_head_accounts","id",$head_id,"tab_name");
	
	$sql3=$obj->FlyQuery("SELECT id,head_title FROM ".$gettable);
	if(!empty($sql3))
	foreach($sql3 as $rows){ ?>
	<li><?php echo $rows->head_title; ?>
	<?php
	$sql4=$obj->FlyQuery("SELECT id,head_title,status FROM account_module_head_list WHERE list_of_head_accounts_id='".$head_id."' AND list_of_sub_head_accounts_id='".$rows->id."'");
	if(!empty($sql4))
	{
		?>
		<ul>
		<?php
		foreach($sql4 as $hlled){
		?>
		<li><a href="<?php echo $obj->filename(); ?>?ids=<?php echo $hlled->id; ?>&amp;subhead=<?php echo $head_id; ?>&amp;name=<?php echo $hlled->head_title; ?>&amp;status=<?php echo $hlled->status; ?>"><?php echo $hlled->head_title; ?></a>
		<?php 
		$sql5=$obj->FlyQuery("SELECT id,head_title,branch_id,status FROM account_module_head_sub_list WHERE list_of_head_accounts_id='".$head_id."' AND list_of_sub_head_accounts_id='".$rows->id."' AND list_of_sub_head_list_id='".$hlled->id."'");
		if(!empty($sql5)){
		?>
		<ul>
		<?php 
		foreach($sql5 as $subhl){
		?>
			<li id="sub_sub_head_<?php echo $subhl->id; ?>"><a  href="?id=<?php echo $subhl->id; ?>&amp;subhead=<?php echo $head_id; ?>&amp;name=<?php  echo $subhl->head_title;  ?>&amp;status=<?php echo $subhl->status;   ?>"><?php echo $subhl->head_title; ?></a> 		<?php 
if($subhl->branch_id!=0)
{ 
?>
<a  class="label_1"  onclick="javascript:deleteledger(<?php echo $subhl->id; ?>,<?php echo $head_id; ?>)" title="Delete"><i class="icon-trash"></i></a>
<?php } ?>
</li>
		<?php 
		}
		?>    
		</ul>
		<?php 
		}
		?>
		</li>
		<?php
		}
		?>
		</ul>
		<?php
	}
	?>
	</li>
	<?php }
	?>
    <script type="text/javascript">
			ddtreemenu.createTree("treemenu<?php echo $head_id; ?>", true)
	</script>
    <?php
	//reload data
}
elseif($st==6)
{
	if($obj->delete("account_module_ladger_list_properties",array("id"=>$ledger_id))==1)
	{
		echo 1;
	}
	else
	{
		echo 0;	
	}
}
elseif($st==7)
{
	//load ledger list
	$sqld1=$obj->FlyQuery("SELECT id,branch_id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='".$head_id."'");
	if(!empty($sqld1))
	foreach($sqld1 as $row)
	{
	?>
		<tr>
			<td><?php echo $row->head_sub_list_name; ?></td>
			<td width="20%">
            <?php 
			if($row->branch_id==$shop_id){
			?>
            <a onClick="javascript:deleteledger(<?php echo $row->id; ?>,<?php echo $head_id; ?>)" class='btn btn-danger' href='#'>Delete</a> 
			<?php 
			}
			?>
			</td>
		</tr>
	<?php
	}
	//load ledger list
}
elseif($st==8)
{
	//insert ledger list
	$chkmain=$obj->exists_multiple("account_module_ladger_list_properties",array("head_sub_list_id"=>$ledger_id,"main_head_id"=>$head_id));
	if($chkmain==0)
	{
		$getledgername=$obj->SelectAllByVal("account_module_head_sub_list","id",$ledger_id,"head_title");
		if($obj->insert("account_module_ladger_list_properties",array("main_head_id"=>$head_id,"head_sub_list_id"=>$ledger_id,"head_sub_list_name"=>$getledgername,"status"=>2,"branch_id"=>$shop_id))==1)
		{
			echo 1;
		}
		else
		{
			echo 0;	
		}
	}
	else
	{
		echo 2;
	}
	//insert ledger list
}
else
{
	echo "";	
}
?>