<?php
include('../class/auth.php');
extract($_POST);
if ($st == 1) {
    if ($obj->exists_multiple("product", array("id" => $product_id)) == 0) {
        $productarray = array("id" => $product_id, "name" => "", "status" => 3);
    } else {
        $ex = $obj->exists_multiple("account_module_product_sales_ledger", array("store_id" => $input_by, "pid" => $product_id,"input_by" => $input_bys));
        if ($ex == 0) {
            if ($obj->insert("account_module_product_sales_ledger", array("store_id" => $input_by, "pid" => $product_id, "ledger" => $ledger_id, "input_by" => $input_bys, "date" => date('Y-m-d'), "status" => 1)) == 1) {
                $productarray = array("id" => $product_id, "status" => 1);
            } else {
                $productarray = array("id" => $product_id, "status" => 0);
            }
        } else {
            $scqfid = $obj->FlyQuery("SELECT id FROM account_module_product_sales_ledger WHERE store_id='" . $input_by . "' AND pid='" . $product_id . "' AND input_by='" . $input_bys . "'");
            $idp = $scqfid[0]->id;
            $productarray = array("id" => $idp, "status" => 2);
        }
    }


    echo json_encode($productarray);
} elseif ($st == 2) {
    if ($obj->exists_multiple("product", array("id" => $product_id)) == 0) {
        $productarray = array("id" => $product_id, "name" => "", "status" => 3);
    } else {
        $ex = $obj->exists_multiple("account_module_product_sales_ledger", array("store_id" => $input_by, "pid" => $product_id, "input_by" => $input_bys));
        if ($ex == 1) {
            if ($obj->update("account_module_product_sales_ledger", array("id" => $ledger_product, "store_id" => $input_by, "pid" => $product_id, "ledger" => $ledger_id, "input_by" => $input_bys, "update_date" => date('Y-m-d'), "status" => 1)) == 1) {
                $productarray = array("id" => $product_id, "status" => 1);
            } else {
                $productarray = array("id" => $product_id, "status" => 0);
            }
        } else {
            $productarray = array("id" => $product_id, "status" => 2);
        }
    }


    echo json_encode($productarray);
} elseif ($st == 3) {
    //product Sales ledger Record start
    $salesdata = '';
    $sql = $obj->FlyQuery("SELECT a.id,a.pid,b.name,c.head_sub_list_name as ledger FROM `account_module_product_sales_ledger` as a 
left JOIN product as b on b.id=a.pid 
left JOIN account_module_ladger_list_properties as c on c.id=a.ledger 
WHERE a.`date`='" . date('Y-m-d') . "' OR a.`update_date`='" . date('Y-m-d') . "'");
    $i = 1;
    if (!empty($sql))
        foreach ($sql as $row):

            $salesdata .='<tr id="tr' . $row->id . '">
		<td>' . $i . '</td>
		<td><a href="view_product.php?pid=' . $row->pid . '">' . $row->name . '</a></td>
		<td>' . $row->ledger . '</td>';

            $salesdata .='<td>
		<a href="#" onClick="deleteR(' . $row->id . ')"><i class="icon-trash" ></i></a>';
            $salesdata .='</td></tr>';
            $i++;
        endforeach;

    $salesd = array("status" => 1, "listdata" => $salesdata);
    echo json_encode($salesd);
    //product Sales ledger Record end
} elseif ($st == 4) {
    //Sales ledger Record
    if($obj->delete("account_module_product_sales_ledger", array("id" => $id))==1)
    {
       echo 1;
    }
    else
    {
        echo 0;
    }
    //Sales ledger Record
} elseif ($st == 5) {
    $viewinvoice = $obj->FlyQuery("SELECT a.`id`,
a.`invoice_id`,
a.`link_id`,
concat(b.`fname`,' ',b.`lname`) as `customer_id`,
a.`date`,
a.`duedate`,
a.`subheading`,
a.`notes`,
(select SUM(`amount`) as `amount` FROM `account_module_invoice_payment` as `d` WHERE d.invoice_id=a.invoice_id) as `amount`,
c.`name` as `branch_id` FROM account_module_invoice as a 
left join account_module_customer as b on b.`id`=a.`customer_id` 
left join store as c on c.`id`=a.`branch_id` WHERE a.id='$view'");

    $customer = $viewinvoice[0]->customer_id;
    $subheading = $viewinvoice[0]->subheading;
    $shop_id = $viewinvoice[0]->branch_id;
    $invoicedate = $obj->dates($viewinvoice[0]->date);
    $paiddate = $obj->dates($viewinvoice[0]->duedate);
    $notes = $viewinvoice[0]->notes;
    $invoice_id = $viewinvoice[0]->invoice_id;
    $paidamount = $viewinvoice[0]->amount;
    $link_id = $viewinvoice[0]->link_id;
    $subtotal = 0;
    $subquantity = 0;
    $subprice = 0;
    $d = 1;
    $newtr = '';

    $sqlinvoice_detail = $obj->FlyQuery("SELECT a.`pid`,b.`pname` as `name`,b.`description` as `description`,a.`quantity`,a.`price`,a.`subtotal` FROM `account_module_invoice_detail` as a
left JOIN account_module_product as b on b.`id`=a.`pid` WHERE a.`invoice_id`='$view'");
    foreach ($sqlinvoice_detail as $detail):
        $newtr .='<tr>';
        $newtr .='<td>' . $d . '</td>';
        $newtr .='<td>' . $detail->name . '</td>';
        $newtr .='<td>' . $detail->description . '</td>';
        $newtr .='<td style="text-align:right;">' . number_format($detail->quantity, 2) . '</td>';
        $subquantity+=$detail->quantity;
        $newtr .='<td style="text-align:right;">' . number_format($detail->price, 2) . '</td>';
        $subprice+=$detail->price;
        $newtr .='<td style="text-align:right;">' . number_format($detail->subtotal, 2) . '</td>';
        $subtotal+=$detail->subtotal;
        $newtr .='</tr>';
        $d++;
    endforeach;

    $newtrfoot = '';
    $newtrfoot .='<tr>';
    $newtrfoot .='<td style="text-align:left;" rowspan="3" colspan="4">
		<strong>Invoice ID :</strong> ' . $invoice_id . '<br>
		<strong>Track ID :</strong> ' . $link_id . '<br>
		<strong>Total Quantity :</strong> ' . number_format($subquantity, 2) . '<br>
		<strong>Total Unit Price Total :</strong> ' . number_format($subprice, 2) . '<br>
		</td>';
    $newtrfoot .='<td style="text-align:right;"><strong>Sub Total Amount = </strong></td>';
    $newtrfoot .='<td style="text-align:right;">' . number_format($subtotal, 2) . '</td>';
    $newtrfoot .='</tr>';

    $newtrfoot .='<tr>';
    $newtrfoot .='<td style="text-align:right;"><strong>Paid Amount =</strong> </td>';
    $newtrfoot .='<td style="text-align:right;">' . number_format($paidamount, 2) . '</td>';
    $newtrfoot .='</tr>';

    $dueamount = $subtotal - $paidamount;

    $newtrfoot .='<tr>';
    $newtrfoot .='<td style="text-align:right;"><strong>Due Amount =</strong> </td>';
    $newtrfoot .='<td style="text-align:right;">' . number_format($dueamount, 2) . '</td>';
    $newtrfoot .='</tr>';

    $ss = array("status" => 1,
        "customer" => $customer,
        "subheading" => $subheading,
        "shop_id" => $shop_id,
        "invoicedate" => $invoicedate,
        "paiddate" => $paiddate,
        "notes" => $notes,
        "invoice_id" => $invoice_id,
        "datatr" => $newtr, "datafot" => $newtrfoot);

    echo json_encode($ss);
}
else {
    echo 0;
}
?>














