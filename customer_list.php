<?php
include('class/auth.php');
include('./querysheld/customer.php');
$obj_customer = new Customer();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_customer->admin($from, $to);
    } else {
        $sql = $obj_customer->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_customer->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_customer->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_customer->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_customer->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_customer->manager($from, $to, $input_by);
    } else {
        $sql = $obj_customer->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_customer->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_customer->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function deleteCus(id)
            {
                var c = confirm("are you sure to Delete this Customer ?.");
                if (c)
                {
                    $.jGrowl('Your delete is processing....', {sticky: false, theme: 'growl-info', header: 'Notification!'});
                    $.post("lib/supplier.php", {'st': 2, 'customer_id': id}, function (data) {
                        if (data == 1)
                        {
                            $('#tr' + id).hide('slow');
                            $.jGrowl('Your data deleted successfully.', {sticky: false, theme: 'growl-success', header: 'Notification!'});
                        }
                        else
                        {
                            $.jGrowl('Your data deletion failed.', {sticky: false, theme: 'growl-error', header: 'Notification!'});
                        }
                    });
                }

            }

            function pageR(id)
            {
                if (id != '')
                {
                    var c = confirm('are you sure to view this customer report ?');
                    if (c == true)
                    {
                        window.location.replace('./customer_report.php?customer=' + id);
                    }
                }
            }
        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Customer List </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">
                                    <div class="span12">
                                            <h3 align="center"><strong>Customer List</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>


                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                    <div class="table-overflow">
                                        <table class="table table-striped" id="data-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Paid Amount</th>
                                                    <th>Due Amount</th>
                                                    <th>Total Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
//                                                $sql1 = $obj->FlyQuery("SELECT
//                                            i.id,
//                                            i.company_name,
//                                            concat(i.fname,' ',i.lname) as name,
//                                            i.phone,
//                                            IFNULL(p.amount, 0) as paid_amount,
//                                            IFNULL(p.totalamount, 0) as total_amount,
//                                            IFNULL((p.totalamount-p.amount),0) as due_amount
//                                            FROM account_module_customer AS i
//                                            LEFT JOIN (SELECT
//                                                       customer_id,
//                                                       invoice_id,
//                                                       (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
//                                                       (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
//                                                       FROM account_module_invoice
//                                                       GROUP BY customer_id) AS p
//                                            ON p.customer_id = i.id ORDER BY i.id ASC LIMIT 1000");
                                                $aa = 1;
                                                if (!empty($sql))
                                                    foreach ($sql as $row) {
                                                        ?>
                                                        <tr id="tr<?php echo $row->id; ?>">
                                                            <td valign="middle" align="center" style="width:30px;">
                                                                <?php echo $aa; ?>    
                                                            </td>
                                                            <td align="left" valign="middle"><?php echo $row->name; ?></td>
                                                            <td align="left" valign="middle"><?php echo $row->phone; ?></td>

                                                            <td align="center" valign="middle"><?php echo $obj->amountconvert($row->paid_amount); ?></td>
                                                            <td align="center" valign="middle"><?php echo $obj->amountconvert($row->due_amount); ?></td>
                                                            <td align="center" valign="middle"><?php echo $obj->amountconvert($row->total_amount); ?></td>

                                                            <td>
                                                                <?php /* ?><a onClick="javascript:return confirm('Are you absolutely sure to View Transaction This?')" title="View Transaction" href="<?php echo $obj->filename(); ?>?view=<?php echo $row->id; ?>"><i class="icon-list"></i></a><?php */ ?>
                                                                <a onClick="javascript:pageR(<?php echo $row->id; ?>)" title="View Customer" href="#"><i class="icon-list"></i></a>
                                                                <a onClick="javascript:deleteCus(<?php echo $row->id; ?>)" title="Delete" href="#"><i class="icon-remove-sign"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $aa++;
                                                    }
                                                ?> 
                                            </tbody>
                                        </table>
                                    </div>        <!--tab 1 content start from here-->  

                                </div>
                                <!-- General form elements -->






                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->


                            </fieldset>                     



                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>  
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
