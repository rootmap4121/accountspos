<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
        <script type="text/javascript" src="js/simpletreemenu.js"></script>
        <link href="css/simpletree.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        .em{}
        .em:hover{ background:#093; }
        </style>
        <style type="text/css">
			.label_edit_1{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_1:hover{ color:rgba(153,0,153,1) !important; }
			.label_1{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_1:hover{ color:rgba(255,102,0,1) !important; }
			
			.label_edit_2{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_2:hover{ color:rgba(153,0,153,1) !important; }
			.label_2{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_2:hover{ color:rgba(255,102,0,1) !important; }
			
			.label_edit_3{ color:rgba(0,0,255,1) !important; float:right; }
			.label_edit_3:hover{ color:rgba(153,0,153,1) !important; }
			.label_3{ color:rgba(153,0,0,1) !important; float:right; margin-left:5px; }
			.label_3:hover{ color:rgba(255,102,0,1) !important; }
		</style>
        <script>
		function getmainsubhead(main_head)
		{
        	if(main_head!="")
			{
				$('#mainHead').attr('class',main_head);
				$.post("lib/coa.php",{'st':1,'main_head':main_head},function(data){
					$('#main_sub_head').html(data);
				});
			}
			else
			{
				alert("Please select a main account head");
			}
        }
		
		function getsubsubhead(sub_head)
		{
        	if(sub_head!="")
			{
				var main_head=$('#mainHead').attr('class');
				$.post("lib/coa.php",{'st':2,'sub_head':sub_head,'main_head':main_head},function(data){
					var datacl=jQuery.parseJSON(data);
					$('#sub_sub_head').html(datacl.content);
				});
			}
			else
			{
				alert("Please select a main account head");
			}
        }
		
		function showButton(vvv)
		{
			if(vvv!="")
			{
				$('#ledger_save_box').show('slow');
			}
		}
		
		function saveledger()
		{
			var ledger=$('#account').val();
			var main_head=$('#main_head').val();
			var main_sub_head=$('#main_sub_head').val();
			var sub_sub_head=$('#sub_sub_head').val();
			//alert(ledger+" "+main_head+" "+main_sub_head+" "+sub_sub_head);
			$.post("lib/coa.php",{'st':3,'ledger':ledger,'main_head':main_head,'main_sub_head':main_sub_head,'sub_sub_head':sub_sub_head},function(data){
					//alert(data);
					if(data==1)
					{
						reloadcoa(main_head)
						$.jGrowl('Saved, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
					}
					else if(data==2)
					{
						$.jGrowl('Failed, Already Exists.', { sticky: false, theme: 'growl-warning', header: 'warning!' });
					}
					else
					{
						$.jGrowl('Failed, Please Reload page & try again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
					}
					
					
			});
			
		}
		
		
		function deleteledger(ledger_id,head_id)
		{
			var c = confirm("Are You Sure To Delete this Ledger & its all payment reord ?.");
			if(c)
			{
				
				if(ledger_id!="")
				{
					$.post("lib/coa.php",{'st':4,'ledger_id':ledger_id},function(data)
					{
						if(data==1)
						{
							reloadcoa(head_id);
							$('#sub_sub_head_'+ledger_id).hide('slow');
							$.jGrowl('Deleted, Successfully.', { sticky: false, theme: 'growl-success', header: 'success!' });
							
						}
						else
						{
							$.jGrowl('Failed, Please Reload page & try again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
						}
					});	
				}
				else
				{
					$.jGrowl('Failed, Please Reload page & try again.', { sticky: false, theme: 'growl-error', header: 'Error!' });
				}
			}
			
		}
		
		function reloadcoa(head_id)
		{
			$('#treemenu'+head_id).html('<img alt="" src="images/elements/loaders/9.gif">');
			$.post("lib/coa.php",{'st':5,'head_id':head_id},function(data){
				$('#treemenu'+head_id).html(data);
			});
		}
		
		function AddLedgerBook(ledger_id,head_id)
		{
			var c = confirm("are you sure to add this ledger in ledger book ?.");
			if(c)
			{
				$.jGrowl('Your data is processing....',{ sticky:false,theme:'growl-info',header:'Notification!' });
				
				$.post("lib/coa.php",{'st':8,'ledger_id':ledger_id,'head_id':head_id},function(data){
					if(data==2)
					{
						$.jGrowl('Failed, Ledger Already Exists in Ledger Book.',{ sticky:false,theme:'growl-warning',header:'Warning!' });
					}
					else if(data==1)
					{
						$.jGrowl('Your ledger info, Successfully Saved.',{ sticky:false,theme:'growl-success',header:'Notification!' });	
					}
					else
					{
						$.jGrowl('Failed, You Reject to add ledger.',{ sticky:false,theme:'growl-error',header:'Warning!' });
					}
					
				});
			}
			else
			{
				$.jGrowl('Failed, You Reject to add ledger.',{ sticky:false,theme:'growl-error',header:'Warning!' });	
			}
		}
		
		
		</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Chart of accounts </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <div class="block span6">    
                                                              



                                </div>
                                <fieldset>

                                    <div class="row-fluid block"><a  data-toggle="modal" href="#ledger_box"  class="btn btn-info pull-right"><i class="icon-plus"></i> Create Ledger</a>
                                        <div class="well row-fluid span10">
                                            <div class="tabbable">
                                                <!--start ul tabs -->
                                                
                                                <ul class="nav nav-tabs">
                                                    <?php
													$sqlhead=$obj->FlyQuery("SELECT id,head_title from account_module_list_of_head_accounts");
													if(!empty($sqlhead))
													$a=1;
													foreach($sqlhead as $row)
													{
													?>
														<li<?php if($a==1){ ?> class="active"<?php } ?>><a href="#tab<?php echo $row->id; ?>"  data-toggle="tab"><?php echo $row->head_title; ?></a></li>
													<?php
													$a++;
													}
													?>
                                                    
                                                </ul>
                                                <!--end ul tabs -->  
                                                <!--start data tabs --> 
                                                <div class="tab-content">
                                                    <?php
													$sqlheadlist=$obj->FlyQuery("SELECT id,head_title,tab_name from account_module_list_of_head_accounts");
													if(!empty($sqlheadlist))
													$b=1;
													foreach($sqlheadlist as $head)
													{
													?>
                                                    <div class="tab-pane<?php if($b==1){ ?> active<?php } ?>" id="tab<?php echo $head->id; ?>">
                                                        <!--tab 1 content start from here-->
                                                        <h4 style="margin-top:0px; margin-bottom:10px; padding-bottom:10px; border-bottom:2px #333333 solid;">List of <?php echo $head->head_title; ?></h4>
                                                        <h5>
                                                        <ul id="treemenu<?php echo $head->id; ?>" class="treeview">
                                                        	<?php
                                                            $sql3=$obj->FlyQuery("SELECT id,head_title FROM ".$head->tab_name);
                                                            if(!empty($sql3))
															foreach($sql3 as $rows){ ?>
                                                            <li><?php echo $rows->head_title; ?>
                                                            <?php
                                                            $sql4=$obj->FlyQuery("SELECT id,head_title,status FROM account_module_head_list WHERE list_of_head_accounts_id='".$head->id."' AND list_of_sub_head_accounts_id='".$rows->id."'");
															if(!empty($sql4))
															{
																?>
                                                                <ul>
                                                                <?php
																foreach($sql4 as $hlled){
																?>
                                                                <li><a href="#"><?php echo $hlled->head_title; ?></a>
                                                                <?php 
																$sql5=$obj->FlyQuery("SELECT id,head_title,branch_id,status FROM account_module_head_sub_list WHERE list_of_head_accounts_id='".$head->id."' AND list_of_sub_head_accounts_id='".$rows->id."' AND list_of_sub_head_list_id='".$hlled->id."'");
																if(!empty($sql5)){
																?>
                                                                <ul>
                                                                <?php 
																foreach($sql5 as $subhl){
																?>
                                                                	<li id="sub_sub_head_<?php echo $subhl->id; ?>">
                                                                    <a onClick="javascript:AddLedgerBook(<?php echo $subhl->id; ?>,<?php echo $head->id; ?>)"  href="#"><?php echo $subhl->head_title; ?></a> 																							 													<?php 
													if($subhl->branch_id!=0)
													{ 
													?>
                                                <a  class="label_1"  onclick="javascript:deleteledger(<?php echo $subhl->id; ?>,<?php echo $head->id; ?>)" title="Delete"><i class="icon-trash"></i></a>
                                                <?php } ?>
                                                 </li>
                                                                <?php 
																}
																?>    
                                                                </ul>
                                                                <?php 
																}
																?>
                                                                </li>
                                                                <?php
																}
																?>
                                                                </ul>
                                                                <?php
															}
															?>
                                                            </li>
                                                            <?php } ?>   
                                                        </ul>
                            							</h5>
                                                        <!--tab 1 content start from here-->
                                                    </div>
                                                    <?php
													$b++;
													}
													?>
                                                    
                                                    
                                                </div>
                                                <!--End data tabs -->   
                                            </div>
                                        </div>
                                        <!-- General form elements -->


                                        <!--modal ledger_box start here-->
                                        <div id="ledger_box" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form class="form-horizontal" method="post" action="">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h5 id="myModalLabel"> <i class="icon-plus"></i> Create New Ledger</h5>
                                                </div>
                                                <div class="modal-body">
                                        
                                                    <div class="row-fluid">
                                                                  
                                                            <div class="control-group">
                                                                <label class="control-label"> 
                                                                	<strong>Account Name</strong> 
                                                                </label>
                                                                <div class="controls">
                                                                    <input type="text" placeholder="Type Your Account Name" class="span6" name="account" id="account">
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label"> 
                                                                	<strong id="mainHead">Main Head</strong> 
                                                                </label>
                                                                <div class="controls">
                                                                	<div class="span6">
                                                                    <select onChange="getmainsubhead(this.value)" name="main_head" id="main_head" data-placeholder="Select Main Head" class="select">
                                                                    <option value=""></option> 
                                                                    <?php
                                                                    if(!empty($sqlhead))
                                                                    {
                                                                        $a=1;
                                                                        foreach($sqlhead as $row)
                                                                        {
                                                                        ?>
                                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->head_title; ?></option>
                                                                        <?php
                                                                            $a++;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="control-group">
                                                                <label class="control-label"> 
                                                                	<strong>Main Sub Head</strong> 
                                                                </label>
                                                                <div class="controls">
                                                                    <div class="span6">
                                                                    <select  onChange="getsubsubhead(this.value)"  name="main_sub_head" id="main_sub_head" data-placeholder="Select Main Sub Head" class="select">
                                                                    <option value=""></option> 
                                                                    
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="control-group">
                                                                <label class="control-label"> 
                                                                	<strong>Sub Sub  Head</strong> 
                                                                </label>
                                                                <div class="controls">
                                                                    <div class="span6">
                                                                    <select onChange="showButton(this.value)" name="sub_sub_head" id="sub_sub_head" data-placeholder="Select Sub Sub Head" class="select">
                                                                    <option selected value=""></option> 
                                                                    
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>   
                                                
                                                <div class="modal-footer" id="ledger_save_box" style="display:none;">
                                                	<button type="button" onClick="saveledger()"  class="btn btn-info"  name="save_ledger"  id="save_ledger">Save New Ledger </button>
                                                </div>
                                                                                             
                                                </form>
                                        </div>
                                        <!--modal ledger_box end here-->
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                </fieldset>                     



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
			ddtreemenu.createTree("treemenu1", true)
			ddtreemenu.createTree("treemenu2", true)
			ddtreemenu.createTree("treemenu3", true)
			ddtreemenu.createTree("treemenu4", true)
			ddtreemenu.createTree("treemenu5", true)
			ddtreemenu.createTree("treemenu6", true)
			ddtreemenu.createTree("treemenu7", false)
			</script>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
