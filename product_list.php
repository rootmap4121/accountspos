<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>

            function loadtable()
            {
                $("table").DataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
                    "oLanguage": {
                        "sLengthMenu": "<span>Show entries:</span><div class='selector' id='uniform-undefined'><span class='entries' style='-moz-user-select: none;'>10</span> _MENU_ </div>"
                    }
                });
            }

            function changeEntires(vall)
            {
                nucleus(".entries").html(vall);
            }

        </script>
        <script>
            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Sales Invoice Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("./lib/product.php", {'st': 3, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            //loadtable();
                            $.jGrowl('Invoice Record, Successfully Deleted.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }
        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Sales List Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   

                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        <th>Quantity</th>
                                                        <th>S.Price</th>
                                                        <th>P.Price</th>
                                                        <th>S.Ledger</th>
                                                        <th>P.Ledger</th>
                                                        <th>Store</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
<!--                                                    <tr>
                                                        <td colspan="11"><div id="prg"></div></td>
                                                    </tr>-->
                                                </thead>
                                                <tbody id="loadsales">
                                                    
                                                <script>
                                                    nucleus(document).ready(function ()
                                                    {
                                                        //var ajaxTime= new Date().getTime();
                                                        nucleus("#loadsales").html("Loading Please Wait...");
                                                       // var totalTime = new Date().getTime()-ajaxTime;
                                                       // nucleus("#prg").html(totalTime);
                                                        nucleus.post("./lib/product.php", {'st': 4}, function (data)
                                                        {
                                                            var datacl = jQuery.parseJSON(data);
                                                            var status = datacl.status;
                                                            if (status == 1)
                                                            {
                                                                var stringdata = datacl.salesdata;
                                                                nucleus("#loadsales").html(stringdata);
                                                                loadtable();
                                                                nucleus("select[name=DataTables_Table_0_length]").attr("data-placeholder", "Entries");
                                                                nucleus("select[name=DataTables_Table_0_length]").attr("class", "select");
                                                                nucleus("select[name=DataTables_Table_0_length]").attr("tabindex", "2");
                                                                nucleus("select[name=DataTables_Table_0_length]").css("opacity", "0");
                                                                nucleus("select[name=DataTables_Table_0_length]").attr("onChange", "changeEntires(this.value)");

                                                            } else
                                                            {
                                                                var stringdata = "<tr><td colspan='9'>No Data Found</td></tr>";
                                                                nucleus("#loadsales").html(stringdata);
                                                            }
                                                        });

                                                    });





                                                </script>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
        <script>
            nucleus(document).ready(function () {
                nucleus("#showfullmenu").show('slow');
                nucleus("#hidefullmenu").hide();
                nucleus("#left-sidebar").hide('fast');
                nucleus(".content").css('margin-left', '0px');

                nucleus("#showfullmenu").click(function () {
                    nucleus("#left-sidebar").show('slow');
                    nucleus(".content").attr('style', '');
                    nucleus(".content").css('margin-right', '0px');
                    nucleus("#showfullmenu").hide('slow');
                    nucleus("#hidefullmenu").show('slow');

                });

                nucleus("#hidefullmenu").click(function () {
                    nucleus("#left-sidebar").hide('slow');
                    nucleus(".content").attr('style', '');
                    nucleus(".content").css('margin-right', '0px');
                    nucleus(".content").css('margin-left', '0px');
                    nucleus("#hidefullmenu").hide('slow');
                    nucleus("#showfullmenu").show('slow');
                });

            });
        </script>
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
