<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script type="text/javascript">
            // Use jQuery via jQuery() instead of via $()			 			
            function SaveCustomer()
            {
                var voucher_id=nucleus('input[name=voucher_id]').val();
                var date=nucleus('input[name=date]').val();
                var shop_id=nucleus('select[name=shop_id]').val();
                var dr = nucleus('select[name=dr]').serializeArray();
                var cr = nucleus('select[name=cr]').serializeArray();
                var memo = nucleus('input[name=memo]').serializeArray();
                var amount = nucleus('input[name=expense_amount]').serializeArray();
                var emptyflag=false;
                jQuery.each(dr, function (i, val) {
                    if(val.value==""){ emptyflag=true; }
                });
                jQuery.each(cr, function (i, val) {
                    if(val.value==""){ emptyflag=true; }
                });
                jQuery.each(memo, function (i, val) {
                    if(val.value==""){ emptyflag=true; }
                });
                jQuery.each(amount, function (i, val) {
                    if(val.value==""){ emptyflag=true; }
                });
                
                //console.log(emptyflag);
                
                //alert('success');
                if (voucher_id != "" && date != "" && shop_id != "" && emptyflag == false)
                {
                    $.post("lib/expense.php", {'st': 1, 'payment_date': date, 'shop_ids': shop_id, 'voucher_id': voucher_id, 'dr': dr, 'cr': cr, 'memo': memo, 'amount': amount}, function (data)
                    {
                        if (data == 1)
                        {
                            clear();
                            $.jGrowl('Saved, Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else if (data == 2)
                        {
                            $.jGrowl('Failed, Already Exists.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                        } else
                        {
                            $.jGrowl('Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });
                } else
                {
                    $.jGrowl('Failed, Some Field is Empty.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                }
            }

            function clear()
            {
                nucleus('input[name=voucher_id]').val("");
                nucleus('input[name=memo]').val("");
                nucleus('input[name=expense_amount]').val("");
                nucleus('#totalamount').val("0");
            }

            function checksum()
            {
                var amount = 0.00;
                //var aa;

                $(".expense_amount").each(function () {
                    amount += parseFloat($(this).val().replace(/\s/g, '').replace(',', '.'));
                });
                
                
               document.getElementById('totalamount').value =amount;
                //aa = amount;
//                if (st == 1)
//                {
//                    if (aa == 0 || aa == "")
//                    {
//                        return 0;
//                    } else
//                    {
//                        return 1;
//                    }
//                }
            }

        </script>

    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> New Expense Voucher Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <blockquote style="margin-top:-20px;">
                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                    </blockquote>


                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="row-fluid  span12 well">   

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Expense Voucher ID *</label>
                                                        <input type="text"  name="voucher_id" id="voucher_id"  placeholder="Expense Voucher ID" />
                                                    </div>

                                                </div>
                                                
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Payment Date *</label>
                                                        <input type="text" name="date" class="datepicker" placeholder="Payment Date" />
                                                    </div>

                                                </div>
                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">

                                                    <div class="control-group">
                                                        <label class="span12">Choose Shop *</label>
                                                        <select name="shop_id" data-placeholder="Select Shop" class="select-search" id="shop_id" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php
                                                            $sql2 = $obj->SelectAll("store");
                                                            if (!empty($sql2))
                                                                foreach ($sql2 as $rows) {
                                                                    ?>      
                                                                    <option value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>



                                                </div>

                                                
                                                <!-- /selects, dropdowns -->
                                            </div>
                                            <!-- /general form elements -->     

                                            <div class="clearfix"></div>
                                            <!-- Default datatable -->
                                            <!-- /default datatable -->
                                        </fieldset>                     
                                    </form>
                                    <!--tab 1 content start from here-->  

                                </div>

                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   

                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <form class="form-horizontal" id="myForms" method="post" name="invoice" action="">    
                                                <fieldset>
                                                    <table class="table table-striped table-bordered"  id="productmore">
                                                        <thead>
                                                            <tr>
                                                                <th width="20%" style="text-align: center;">Choose Expense Ledger</th>
                                                                <th style="text-align:center;" width="20%">Choose Expense Paid Ledger</th>
                                                                <th style="text-align:center;">Expense Specific Memo</th>
                                                                <th style="text-align:center;">Expense Amount</th>
                                                                <th style="text-align:center;"><input type="hidden" id="currentId"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="POITable">
                                                            <tr class="class0" id="0">
                                                                <td>
                                                                    <div class="control-group" id="select-customer-id-0">
                                                                        <select name="dr" id="dr" style="width: 200px;"> 
                                                                            <optgroup label="Expense Ledger">
                                                                                <?php
                                                                                $sql1 = $obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id=4");
                                                                                if (!empty($sql1)) {
                                                                                    foreach ($sql1 as $row) {
                                                                                        ?>
                                                                                        <option value="<?php echo $row->id; ?>"><strong><?php echo $row->head_sub_list_name; ?></strong></option>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </optgroup>    
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td style="text-align:right;">
                                                                    <select name="cr" id="cr" style="width: 200px;">
                                                                        <optgroup label="Paid Ledger">
                                                                            <?php
                                                                            $ss = $obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
                                                                            if (!empty($ss)) {
                                                                                foreach ($ss as $wow) {
                                                                                    ?>
                                                                                    <option value="<?php echo $wow->id; ?>"><?php echo $wow->head_sub_list_name; ?></option>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </optgroup>
                                                                    </select>
                                                                </td>
                                                                <td style="text-align:right;">
                                                                    <input type="text"  style="width: 200px;" name="memo" id="memo"  placeholder="Expense Specific Memo" />
                                                                </td>
                                                                <td style="text-align:right;">
                                                                    <input type="text" onkeyup="checksum()" style="width: 100px;" name="expense_amount" class="expense_amount" id="expense_amount" placeholder="Payment Amount" />
                                                                </td>                                                                 <td><label><a href="#" class="hhdrow" onClick="hiderow('class0')"><i class="icon-remove"></i></a></label></td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>       
                                                            <tr>
                                                                <td colspan="3" style="text-align:right;">
                                                                    
                                                                </td>
                                                                <td style="text-align:right;">
                                                                    <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalamount" id="totalamount">
                                                                </td>                                                               <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <button type="button"  onclick="return addTableRow('#productmore');
                                                                            return false;" id="btn2" class="btn" ><i class="icon-plus"></i> ADD MORE</button>
                                                                </td>
                                                                <td></td>
                                                                <td style="text-align:right;">
                                                                    <a onClick="clear()" href="<?php echo $obj->filename(); ?>?clear" class="btn btn-danger create">
                                                            <i class="icon-remove"></i> Clear Expense </a>
                                                                </td>
                                                                <td style="text-align:right;">
                                                                    <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                            <i class="icon-plus-sign"></i> Save Record </button>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </fieldset>                     

                                            </form>

                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     

                                    </fieldset>                     

                                    </form>
                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>

                                <!-- General form elements -->
                                <!--                                <div class="row-fluid block">
                                                                     General form elements 
                                                                    <div class="row-fluid  span12 well">   
                                
                                                                         Selects, dropdowns 
                                                                        <div class="table-overflow">
                                                                            <table class="table table-striped" id="data-table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>ID</th>
                                                                                        <th>Ex.V. ID</th>
                                                                                        <th>Expense Ledger</th>
                                                                                        <th>Paid Ledger</th>
                                                                                        <th>Total Amount</th>
                                                                                        <th>Created</th>
                                                                                        <th>Date</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                <?php
                                /* $sql = $obj->FlyQuery("SELECT `id`,(select concat(fname,' ',lname) FROM account_module_vendor WHERE account_module_vendor.id=account_module_bill_payment.vendor_id) as customer_name,`vendor_id`,`date`,`pa`,`amount`,(SELECT store.name FROM store WHERE store.id=account_module_bill_payment.`input_by`) as receiver FROM `account_module_bill_payment` WHERE date='" . date('Y-m-d') . "'");
                                  $i = 1;
                                  if (!empty($sql))
                                  foreach ($sql as $row):
                                  ?>
                                  <tr>
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $row->id; ?></td>
                                  <td><?php echo $row->customer_name; ?></td>
                                  <td><?php echo $row->pa; ?></td>
                                  <td><?php echo $row->amount; ?></td>
                                  <td><?php echo $row->receiver; ?></td>
                                  <td><?php echo $row->date; ?></td>

                                  </tr>
                                  <?php
                                  $i++;
                                  endforeach; */
                                ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                         /selects, dropdowns 
                                
                                
                                
                                                                         Selects, dropdowns 
                                
                                                                         /selects, dropdowns 
                                
                                
                                
                                                                    </div>
                                                                     /general form elements      
                                
                                
                                                                    <div class="clearfix"></div>
                                
                                                                     Default datatable 
                                
                                                                     /default datatable 
                                
                                
                                                                    tab 1 content start from here  
                                
                                                                </div>-->













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>  

        <script>
            function hiderow(id)
            {
                var chkclass = id;
                //alert(chkclass);
                if (chkclass == 'class0')
                {
                    alert("You Cann't Remove Table 1st & 2nd Rows");
                } else
                {
                    var c = confirm("are you sure to Delete this Row From Table ?.");
                    if (c)
                    {
                        $('.' + id).closest('tr').remove();
                        var increval = cloneCount--;
                        var increvald = cloneCountd--;
                    }
                }
                
                checksum();
                
            }

        </script>

        <script type="text/javascript">

            var cloneCount = 1;
            var cloneCountd = 0;
            function addTableRow(table)
            {
                var increval = cloneCount++;
                var increvald = cloneCountd++;
                var $tr = $(table + ' tbody:first').children("tr:last").clone().attr("class", 'class' + increval);
                $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                $tr.find(".hhdrow").attr("onClick", "hiderow('class" + increval + "')");
                //$(table+' tbody:first').children("tr:last").after($tr);
                $(table + ' tbody:first').children("tr:last").after($tr);
                
                checksum();
                
            }

        </script>

        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
