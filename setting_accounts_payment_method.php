<?php
include('class/auth.php');
include('./querysheld/session_setting_payment_method.php');
$obj_payement_method=new PaymentMethod();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_payement_method->admin($from, $to);
    }else {
        $sql=$obj_payement_method->admin($setting_start, $setting_end);
    }
}elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_payement_method->shop_admin($from, $to, $input_by);
    }else {
        $sql=$obj_payement_method->shop_admin($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_payement_method->cashier($from, $to, $input_by);
    }else {
        $sql=$obj_payement_method->cashier($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_payement_method->manager($from, $to, $input_by);
    }else {
        $sql=$obj_payement_method->manager($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 5) {

    $array_ch=array();
    $sqlchain_store_ids=$obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql=$obj_payement_method->store_chain_admin($from, $to, $array_ch);
        }else {
            $sql=$obj_payement_method->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    }else {
        $sql=array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function SaveCustomer()
            {
                var payment_method = $('select[name=payment_method]').val();
                var ledger_id = $('select[name=ledger_id]').val();
                if (payment_method != "" && ledger_id != "")
                {

                    $.post("lib/payment_method.php", {'st': 1,
                        'payment_method': payment_method,
                        'ledger_id': ledger_id
                    }, function (data) {
                        if (data == 1)
                        {
                            //clear();
                            $.jGrowl('Saved, Account Session Started Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else if (data == 2)
                        {
                            $.jGrowl('Saved, Accounts Session Finish Record Saved Successfully.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                        } else
                        {
                            $.jGrowl('Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });
                } else
                {
                    $.jGrowl('Failed, Some Field is Empty.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                }
            }

            function clear()
            {
//                $('.datepicker').val("");
//                $('.span12 .memo').val("");
//                $('#shop_id').val("");


//                $('#opt_a_d').val("");
//                $('#opt_a_debit').val("0.00");
//                $('#opt_a_cradit').val("0.00");


//                $('#opt_b_d').val("");
//                $('#opt_b_debit').val("0.00");
//                $('#opt_b_cradit').val("0.00");


//                $('#totaldebit').val("0.00");
//                $('#totalcradit').val("0.00");
            }

        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Setting Accounting Payment Method Ledger </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <blockquote style="margin-top:-20px;">
                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                    </blockquote>


                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="row-fluid  span12 well">

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12"> Payment Method *</label>
                                                        <select name="payment_method" data-placeholder="Select Payment Method" class="select-search" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            $sqlpayment_method=$obj->SelectAll("payment_method");
                                                            foreach ($sqlpayment_method as $method):
                                                                ?>
                                                                <option value="<?php echo $method->id; ?>"><?php echo $method->meth_name; ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12"> Bank & Cash Ledger *</label>
                                                        <select name="ledger_id" data-placeholder="Select Ledger ID" class="select-search" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            $ss=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
                                                            foreach ($ss as $wow) {
                                                                ?>
                                                                <option value="<?php echo $wow->id; ?>"><?php echo $wow->head_sub_list_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>


                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->

                                                <!-- /selects, dropdowns -->



                                                <div class="clearfix"></div>

                                                <!-- Selects, dropdowns -->


                                                <div class="span4">
                                                    <label class="span12" style="height: 35px;">&nbsp;</label>
                                                    <div class="control-group">
                                                        <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                            <i class="icon-plus-sign"></i> Save Record </button>
                                                    </div>
                                                </div>



                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->

                                                <!-- /selects, dropdowns -->



                                            </div>
                                            <!-- /general form elements -->


                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->


                                            <!--tab 1 content start from here-->

                                            </div>
                                            <!-- General form elements -->












                                            <div class="row-fluid block">
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12 well">

                                                    <!-- Selects, dropdowns -->
                                                    <div class="table-overflow">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Payment Method</th>
                                                                    <th>Payment Ledger</th>
                                                                    <th>Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="sitedata">
                                                                <?php
//                                                                $sqlquery = $obj->FlyQuery("SELECT
//                                                                a.id,
//                                                                a.store_id,
//                                                                a.store_id as store,
//                                                                a.input_by,
//                                                                a.finput_by,
//                                                                a.title,
//                                                                a.session_start_year,
//                                                                a.session_end_year,
//                                                                a.session_start_date,
//                                                                a.session_end_date,
//                                                                a.date,
//                                                                a.fdate,
//                                                                a.status
//                                                                FROM
//                                                                account_module_session_setting as a");
                                                                $dd='';
                                                                $d=1;
                                                                if (!empty($sql))
                                                                    foreach ($sql as $ww):
                                                                        $dd .='<tr>
                                                                        <td>' . $d . '</td>
                                                                        <td>' . $ww->meth_name . '</td>
                                                                        <td>' . $ww->ledger . '</td>
                                                                        <td>' . $obj->dates($ww->date) . '</td>
                                                                        </tr>';
                                                                        $d++;
                                                                    endforeach;
                                                                echo $dd;
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /selects, dropdowns -->



                                                    <!-- Selects, dropdowns -->

                                                    <!-- /selects, dropdowns -->



                                                </div>
                                                <!-- /general form elements -->


                                                <div class="clearfix"></div>

                                                <!-- Default datatable -->

                                                <!-- /default datatable -->


                                                <!--tab 1 content start from here-->

                                            </div>





                                            <!-- General form elements -->








                                            </div>



                                            <!-- General form elements -->

                                            <!-- /general form elements -->






                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->






                                            <!-- Content End from here customized -->




                                            <div class="separator-doubled"></div>



                                            </div>
                                            <!-- /content container -->

                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            <!-- /main content -->
                                            <?php include('include/footer.php'); ?>
                                            <!-- Right sidebar -->
                                            <?php //include('include/sidebar_right.php');      ?>
                                            <!-- /right sidebar -->

                                            </div>
                                            <!-- /main wrapper -->

                                            </body>
                                            </html>
