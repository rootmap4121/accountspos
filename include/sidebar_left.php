<div class="sidebar" id="left-sidebar">
    <ul class="navigation standard"><!-- standard nav -->
        <li><a href="index.php" title="" class="hh"><img src="images/icons/mainnav/dashboard.png" alt="" />Dashboard</a></li>
        <li><a href="setting_coa.php" title=""><img src="images/icons/mainnav/dashboard.png" alt="" />Chart of Accounts</a></li>
        <li><a href="setting_ledger_list.php" title=""><img src="images/icons/mainnav/dashboard.png" alt="" />Ledger List</a></li>
        <li><a href="setting_tax.php" title=""><img src="images/icons/mainnav/dashboard.png" alt="" />Settting Tax Ledger</a></li>
        <li>
            <a href="#" title=""  class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" /> Product / Service Info</a>
            <ul>
                <li><a href="product_list.php" title="Product List">Product List</a></li>
                <li><a href="setting_product_sales_ledger.php" title="">Product Sales Ledger</a></li>
                <li><a href="setting_product_purchase_ledger.php" title="">Product Purchase Ledger</a></li>
            </ul>
        </li>

        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Customer Info</a>
            <ul>
                <li><a href="customer.php" title="">Add New Customer</a></li>
                <li><a href="customer_list.php" title="">Customer List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Supplier Info</a>
            <ul>
                <li><a href="supplier.php" title="">Add New Supplier</a></li>
                <li><a href="supplier_list.php" title="">Supplier List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Expense  Voucher Info</a>
            <ul>
                <li><a href="expense_voucher.php" title="">Expense Voucher</a></li>
                <li><a href="expense_voucher_list.php" title="">Expense Voucher List</a></li>
                <li style="display: none;"><a href="view_expense_voucher.php" title="">View Expense Voucher</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Receive Payment Info</a>
            <ul>
                <li><a href="receive_new_payment.php" title="">Receive New Payment</a></li>
                <li><a href="receive_payment_list.php" title="">Receive Payment List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Paid Payment Info</a>
            <ul>
                <li><a href="paid_new_payment.php" title="">Paid New Payment</a></li>
                <li><a href="paid_payment_list.php" title="">Paid Payment List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" /> Sales Info</a>
            <ul>
                <li><a href="new_sales.php" title=""> Create New Sales </a></li>
                <li><a href="sales_list.php" title=""> Sales List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" /> Purchase Info</a>
            <ul>
                <li><a href="new_purchase.php" title=""> Create New Purchase </a></li>
                <li><a href="purchase_list.php" title=""> Purchase List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" /> Journal Info</a>
            <ul>
                <li><a href="new_single_entry_journal.php" title="New Single Entry Journal">New Single Entry Journal</a></li>
                <li><a href="single_entry_journal_list.php" title="Single Entry Journal List">Single Entry Journal List</a></li>
                <li><a href="contra_entry_journal.php" title="Contra Entry Journal">Contra Entry Journal</a></li>
                <li><a href="contra_journal_list.php" title="Contra Journal List">Contra Entry Journal List</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" /> Accounts Session Info</a>
            <ul>
                <li><a href="setting_accounts.php" title="Setting Accounts">Setting Accounts</a></li>
                <li><a href="setting_accounts_payment_method.php" title="Setting Payment Method Ledger">Setting Payment Method Ledger</a></li>
            </ul>
        </li>
        <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/dashboard.png" alt="" />Report Info</a>
            <ul>
                <li><a href="general_ledger.php" title="General Ledger">General Ledger</a></li>
                <li><a href="trial_balace.php" title="Trail Balance">Trial Balance</a></li>
                <li><a href="income_statement.php" title="Income Statement">Income Statement</a></li>
                <li><a href="balance_sheet.php" title="Balancesheet">Balancesheet</a></li>
                <li><a href="customer_transaction.php" title="Customer Transaction">Customer Transaction</a></li>
                <li><a href="receive_payment_list.php" title="Receive Payment">Receive Payment</a></li>
                <li><a href="paid_payment_list.php" title="Paid Payment">Paid Payment</a></li>
                <li><a href="Journal_list.php" title="Journal Transaction">Journal Transaction</a></li>
                <li><a href="expense_voucher_list.php" title="Expense Voucher">Expense Voucher</a></li>
                <li><a href="customer_report.php" title="Customer Report">Customer Report</a></li>
                <li><a href="supplier_list.php" title="Supplier">Supplier</a></li>
                <li><a href="sales_list.php" title="Sales List">Sales List / Invoices</a></li>
            </ul>
        </li>
    </ul>
</div>








































