<h5 align="center"><strong>Expected</strong></h5>
<?php
if (isset($_GET['date_report'])) {
    ?>
<h4 align="center">

    <form action="" method="get">
        <strong id="expected_session_label">From : <?php echo $obj->dates($from); ?> - To : <?php echo $obj->dates($to); ?></strong> 

        <strong id="expected_session_field" style="display: none;">
            <style type="text/css">
                .ui-datepicker-append{ display: none; }
            </style>    

            <input type="text" name="from" class="datepicker" placeholder="From" /> - <input type="text" name="to" class="datepicker" placeholder="To" /> 
            <button type="submit" name="date_report" class="btn btn-info">Generate</button>

        </strong> 
        <i style="position: absolute; margin-top: 4px; margin-left: 10px; cursor: pointer;" id="change_report_date" class="icon-edit"></i>
    </form>
    <script>
        nucleus(document).ready(function () {
            nucleus('#change_report_date').click(function () {
                nucleus('#expected_session_label').toggle();
                nucleus('#expected_session_field').toggle();
            });
        });
    </script>

</h4>
<?php
} else {
   ?>
 <h4 align="center">

    <form action="" method="get">
        <strong id="expected_session_label">From : <?php echo $obj->dates($setting_start); ?> - To : <?php echo $obj->dates($setting_end); ?></strong> 

        <strong id="expected_session_field" style="display: none;">
            <style type="text/css">
                .ui-datepicker-append{ display: none; }
            </style>    

            <input type="text" name="from" class="datepicker" placeholder="From" /> - <input type="text" name="to" class="datepicker" placeholder="To" /> 
            <button type="submit" name="date_report" class="btn btn-info">Generate</button>

        </strong> 
        <i style="position: absolute; margin-top: 4px; margin-left: 10px; cursor: pointer;" id="change_report_date" class="icon-edit"></i>
    </form>
    <script>
        nucleus(document).ready(function () {
            nucleus('#change_report_date').click(function () {
                nucleus('#expected_session_label').toggle();
                nucleus('#expected_session_field').toggle();
            });
        });
    </script>

</h4>   
    <?php
}
?>
