<div class="sidebar" id="right-sidebar">
        <div class="appendable">
        
            <!-- Sidebar stats -->
            <ul class="topstats block">
                <li>
                    <div class="left">Current balance:<h1>$15,859</h1></div>
                    <div class="topchart" id="balance">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
                </li>
                <li>
                    <div class="left">Total clicks:<h1>345k+</h1></div>
                    <div class="topchart" id="clicks">59,56,28,39,46,29,48,10,45,70,23,32,35,14</div>
                </li>
                <li>
                    <div class="left">Support tickets:<h1>+1,490</h1></div>
                    <div class="topchart" id="support">20,15,41,18,32,21,28,26,34,37,10,20,24,28</div>
                </li>
            </ul>
            <!-- /sidebar stats -->

            <div class="separator-doubled"></div>
            
            <!-- Contact list -->
            <ul class="user-list block">
                <li>
                    <a href="#" title="">
                        <img src="images/live/face.png" alt="" />
                        <span class="contact-name">
                            <strong>Eugene Kopyov <span>(5)</span></strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_away"></span>
                    </a>
                </li>
                <li class="active">
                    <a href="#" title="">
                        <img src="images/live/face2.png" alt="" />
                        <span class="contact-name">
                            <strong>Lucy Wilkinson <span>(12)</span></strong>
                            <i>Team leader</i>
                        </span>
                        <span class="status_off"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="images/live/face3.png" alt="" />
                        <span class="contact-name">
                            <strong>John Dow</strong>
                            <i>PHP developer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="images/live/face4.png" alt="" />
                        <span class="contact-name">
                            <strong>The Incredible</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="images/live/face5.png" alt="" />
                        <span class="contact-name">
                            <strong>The wazzup guy</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="images/live/face6.png" alt="" />
                        <span class="contact-name">
                            <strong>Viktor Fedorovich</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
            </ul>
            <!-- /contact list -->

            <div class="separator-doubled"></div>
            
            <!-- Date picker -->
            <div class="block">
                <div class="inlinepicker"></div>
            </div>
            <!-- /date picker -->

            <div class="separator-doubled"></div>
                    
            <!-- Progress bars -->
            <div class="block">
                <div class="progress progress-info slim"><div class="bar" data-percentage="25"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-success slim"><div class="bar" data-percentage="50"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-warning slim"><div class="bar" data-percentage="75"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-danger slim"><div class="bar" data-percentage="100"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-info"><div class="bar filled-text" data-percentage="25"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-success"><div class="bar filled-text" data-percentage="50"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-warning"><div class="bar filled-text" data-percentage="75"></div></div>
            </div>
            
            <div class="block">
                <div class="progress progress-danger"><div class="bar filled-text" data-percentage="100"></div></div>
            </div>
            <!-- /progress bars -->
                                
        </div>
    </div>