<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function loadtable()
            {
                $("table").DataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
                    "oLanguage": {
                        "sLengthMenu": "<span>Show entries:</span><div class='selector' id='uniform-undefined'><span class='entries' style='-moz-user-select: none;'>10</span> _MENU_ </div>"
                    }
                });
            }

            function removetable()
            {
                $("table").DataTable().fnDestroy();
            }

            function changeEntires(vall)
            {
                nucleus(".entries").html(vall);
            }


            function SaveCustomer()
            {
                var product_id = $('#product_id').val();
                var ledger_id = $('#ledger_id').val();
                if (product_id != "" && ledger_id != "")
                {
                    var c = confirm("are you sure to save this product sales ledger Record ?.");
                    if (c)
                    {
                        $.post("lib/ledger_purchase_sales.php", {'st': 1, 'product_id': product_id, 'ledger_id': ledger_id}, function (data)
                        {
                            var datacl = jQuery.parseJSON(data);
                            var status = datacl.status;
                            if (status == 1)
                            {
                                removetable();
                                nucleus("#reload").click();
                                $.jGrowl('Saved, Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                            } else if (status == 2)
                            {
                                $.jGrowl('Failed, Already Exists.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                                var id = datacl.id;
                                setTimeout(function () {
                                    updateledger(id, product_id, ledger_id);
                                }, 1000);

                            } else if (status == 3)
                            {
                                $.jGrowl('Failed,Product Not Exists.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                            } else
                            {
                                $.jGrowl('Insert Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                            }
                        });
                    }
                } else
                {
                    $.jGrowl('Failed, Some Field is Empty.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                }
            }

            function updateledger(id, product_id, ledger_id)
            {
                var d = confirm("do you want to update this product sales ledger Record ?.");
                if (d)
                {
                    $.post("lib/ledger_purchase_sales.php", {'st': 2, 'ledger_product': id, 'product_id': product_id, 'ledger_id': ledger_id}, function (datau)
                    {
                        var dataclu = jQuery.parseJSON(datau);
                        var statusu = dataclu.status;
                        if (statusu == 1)
                        {

                            $.jGrowl('Product Sales Ledger Updated, Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                            removetable();
                            nucleus("#reload").click();
                            
                        } else
                        {
                            $.jGrowl('Update Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });
                }
            }

            nucleus(document).ready(function ()
            {
                var pid = <?php echo filter_var($_GET['pid'], FILTER_SANITIZE_STRING); ?>;
                $.post("lib/search_controller.php", {'st': 2, 'id': pid}, function (fetch) {
                    var datacl = jQuery.parseJSON(fetch);
                    var status = datacl.status;
                    if (status == 1)
                    {
                        var id = datacl.id;
                        var name = datacl.name;
                        var ledger = datacl.ledger;
                        nucleus("#product_name").val(name);
                        nucleus("#product_id").val(id);
                        nucleus("#reload").click();
                        
                    } else
                    {
                        console.log(status);
                    }
                });


                nucleus("#reload").click(function () {
                    $.post("lib/ledger_purchase_sales.php", {'st': 3}, function (data) {
                        var datacl = jQuery.parseJSON(data);
                        var status = datacl.status;
                        if (status == 1)
                        {
                            var listdata = datacl.listdata;
                            nucleus('#sales_ledger').html(listdata);
                            loadtable();
                            nucleus("select[name=DataTables_Table_0_length]").attr("data-placeholder", "Entries");
                            nucleus("select[name=DataTables_Table_0_length]").attr("class", "select");
                            nucleus("select[name=DataTables_Table_0_length]").attr("tabindex", "2");
                            nucleus("select[name=DataTables_Table_0_length]").css("opacity", "0");
                            nucleus("select[name=DataTables_Table_0_length]").attr("onChange", "changeEntires(this.value)");
                        } else
                        {
                            $.jGrowl('Failed to load ledger list, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });

                });

            });
            
            
            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Sales Ledger Product Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("./lib/ledger_purchase_sales.php", {'st': 4, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            //loadtable();
                            $.jGrowl('Record, Successfully Deleted.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }


        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Product Sales Ledger Info </h5>
                            <ul class="icons">
                                <li><a href="#" id="reload" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <blockquote style="margin-top:-20px;">
                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                    </blockquote>


                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="span5 well">   

                                                <!-- Selects, dropdowns -->

                                                <div class="control-group">
                                                    <label class="span4">Product Name *</label>
                                                    <label class="span8">
                                                        <input type="text" name="product_name" id="product_name" readonly="readonly" placeholder="Product Name" />
                                                        <input type="hidden" name="product_id" id="product_id" readonly="readonly" placeholder="Product Name" />
                                                    </label>
                                                </div>

                                                <div class="control-group">
                                                    <label class="span4">Select Ledger *</label>
                                                    <label class="span8">
                                                        <select name="ledger_id" data-placeholder="Select Ledger" class="select-search" id="ledger_id" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php
                                                            $ss = $obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
                                                            foreach ($ss as $wow) {
                                                                ?>
                                                                <option value="<?php echo $wow->id; ?>"><?php echo $wow->head_sub_list_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </label>    
                                                </div>

                                                <div class="control-group">
                                                    <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                        <i class="icon-plus-sign"></i> Save Record </button>
                                                </div>


                                            </div>


                                            <div class="span7 well"> 
                                                <div class="table-overflow">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Product Name</th>
                                                                <th>Ledger Name</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="sales_ledger">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>




                                            <!-- /general form elements -->     


                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->


                                        </fieldset>                     

                                    </form>

                                    <!--tab 1 content start from here-->  

                                </div>
                                <!-- General form elements -->












                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   

                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');       ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
