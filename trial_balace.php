<?php
include('class/auth.php');
include('./querysheld/general_ledger.php');
$obj_generalledger = new GeneralLedger();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_generalledger->admin($from, $to);
    } else {
        $sql = $obj_generalledger->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_generalledger->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_generalledger->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_generalledger->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_generalledger->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_generalledger->manager($from, $to, $input_by);
    } else {
        $sql = $obj_generalledger->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_generalledger->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_generalledger->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Trial Balance Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Trial Balance</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Ledger ID</th>
                                                        <th>Accounts</th>
                                                        <th style="text-align:right; padding-right:25px;">Debit</th>
                                                        <th style="text-align:right; padding-right:25px;">Cradit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $debit_total=0;
                                                    $credit_total=0;
                                                    $a = 1;
                                                    if (!empty($sql))
                                                        foreach ($sql as $row):
                                                            ?>
                                                            <tr>
                                                                <td valign="middle" align="left">
                                                                    <?php echo $a; ?>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $row->id; ?>"><?php echo $row->id; ?></a>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $row->id; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                </td>
                                                                <td valign="middle" align="right"><?php echo number_format($row->debit, 2); ?></td>
                                                                <td align="right" valign="middle"><?php echo number_format($row->cradit, 2); ?></td>

                                                            </tr>
                                                            <?php
                                                            $debit_total+=$row->debit;
                                                            $credit_total+=$row->cradit;
                                                            $a++;
                                                        endforeach;
                                                    ?> 
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td align="right"><strong>Total = </strong></td>
                                                        <td align="right"><strong><?php echo number_format($debit_total, 2); ?></strong></td>
                                                        <td align="right"><strong><?php echo number_format($credit_total, 2); ?></strong></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
