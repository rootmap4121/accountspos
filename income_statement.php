<?php
include('class/auth.php');
include('./querysheld/income_statement.php');
$obj_incomestatement = new IncomeStatement();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_incomestatement->admin($from, $to);
    } else {
        $sql = $obj_incomestatement->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_incomestatement->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_incomestatement->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_incomestatement->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_incomestatement->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_incomestatement->manager($from, $to, $input_by);
    } else {
        $sql = $obj_incomestatement->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_incomestatement->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_incomestatement->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Income Statement </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Income Statement</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr><th valign="middle" align="left"><h4>Revenue</h4></th>
                                                <th align="center" valign="middle"></th>
                                                </tr>
                                                <?php
                                                $total_revenue = 0;
                                                $sqlrevenue = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    a.head_sub_list_name FROM account_module_ladger_list_properties as a 
                                                    left JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.main_head_id ='3' 
                                                    AND a.head_sub_list_id!='131' 
                                                    AND a.head_sub_list_id!='132' 
                                                    AND a.head_sub_list_id!='133' 
                                                    AND a.head_sub_list_id!='134' 
                                                    AND a.head_sub_list_id!='135' 
                                                    AND a.head_sub_list_id!='136' 
                                                    AND a.head_sub_list_id!='138'");
                                                //if (!empty($sqlrevenue))
                                                foreach ($sql as $row) {

                                                    $inid = $row->id;
                                                    $debit = $row->debit;
                                                    $cradit = $row->cradit;
                                                    if ($cradit != 0 || $debit != 0) {
                                                        ?>
                                                        <tr>
                                                            <th valign="middle" align="left">
                                                                <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                            </th>
                                                            <td align="right" valign="middle">
                                                                <?php
                                                                $netm = $debit - $cradit;
                                                                $getch = substr($netm, 0, 1);
                                                                if ($getch != '-') {
                                                                    echo $obj->amountconvert($netm);
                                                                } else {
                                                                    echo $obj->amountconvert(substr($netm, 1, 200));
                                                                }
                                                                $total_revenue+=$debit - $cradit;
                                                                ?></strong></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                                <tr><th valign="middle" align="left"><h4>Gross Revenue</h4></th>
                                                <td align="right" valign="middle"><?php
                                                    $chktr = substr($total_revenue, 0, 1);
                                                    if ($chktr != '-') {
                                                        echo $obj->amountconvert($gettotalrevenue = $total_revenue);
                                                    } else {
                                                        echo $obj->amountconvert($gettotalrevenue = substr($total_revenue, 1, 200));
                                                    }
                                                    ?></td>
                                                </tr>

                                                <!--operating expenses-->
                                                <tr><td colspan="2"></td></tr>
                                                <tr>
                                                    <th valign="middle" align="left">(Less) </th>
                                                    <td align="center" valign="middle">&nbsp;</td>
                                                </tr>
                                                <!--frieght in-->
                                                <?php
                                                $ld_sales_discount = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name as name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total FROM `account_module_ladger_list_properties` as a
                                                    LEFT JOIN (" . $sql . ") AS b ON b.ladger_id=a.id 
                                                    WHERE 
                                                    a.main_head_id = '4' 
                                                    AND 
                                                    a.head_sub_list_id = '380'");
                                                $sales_discount = 0;
                                                if (!empty($ld_sales_discount))
                                                    foreach ($ld_sales_discount as $row_sales_discount) {

                                                        $inid_sales_discount = $row_sales_discount->id;
                                                        $debit_sales_discount = $row_sales_discount->debit;
                                                        $cradit_sales_discount = $row_sales_discount->cradit;
                                                        $sales_discount = $row_sales_discount->total;
                                                        if ($sales_discount != 0) {
                                                            ?>

                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid_sales_discount; ?>"><?php echo $row_sales_discount->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm_sales_discount = $debit_sales_discount - $cradit_sales_discount;
                                                                    $getch = substr($netm_sales_discount, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm_sales_discount);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm_sales_discount, 1, 200));
                                                                    }
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>

                                                <?php
                                                $ld_sales_return = $obj->FlyQuery("SELECT 
                                                a.id,
                                                a.head_sub_list_name,
                                                IFNULL(b.debit,0) as debit,
                                                IFNULL(b.cradit,0) as cradit,
                                                IFNULL((b.debit-b.cradit),0) as total FROM `account_module_ladger_list_properties` as a 
                                                left JOIN (" . $sql . ") as b ON b.ladger_id=a.id 
                                                WHERE 
                                                a.main_head_id='4' AND 
                                                a.head_sub_list_id='379'");

                                                foreach ($ld_sales_return as $row_sales_return) {

                                                    $inid_sales_return = $row_sales_return->id;
                                                    $debit_sales_return = $row_sales_return->debit;
                                                    $cradit_sales_return = $row_sales_return->cradit;
                                                    $sales_return = $row_sales_return->total;
                                                    if ($sales_return != 0) {
                                                        ?>

                                                        <tr>
                                                            <th valign="middle" align="left">
                                                                <a href="viewledger.php?ladger_id=<?php echo $inid_sales_return; ?>"><?php echo $row_sales_return->head_sub_list_name; ?></a>
                                                            </th>
                                                            <td align="right" valign="middle">
                                                                <?php
                                                                $netm_sales_return = $debit_sales_return - $cradit_sales_return;
                                                                $getch = substr($netm_sales_return, 0, 1);
                                                                if ($getch != '-') {
                                                                    echo $obj->amountconvert($netm_sales_return);
                                                                } else {
                                                                    echo $obj->amountconvert(substr($netm_sales_return, 1, 200));
                                                                }
                                                                ?></strong></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                                <tr><td colspan="2"></td></tr>
                                                <tr>
                                                    <th valign="middle" align="left" colspan="2">(Less) </th></tr>
                                                <?php
                                                $chkfourcountled = $obj->FlyQuery("SELECT 
                                                c.id,
                                                c.head_sub_list_name,
                                                IFNULL(b.debit,0) as debit,
                                                IFNULL(b.cradit,0) as cradit,
                                                IFNULL((b.debit-b.cradit),0) as total FROM `account_module_head_sub_list` as a 
                                                left JOIN account_module_ladger_list_properties as c on c.main_head_id='4' AND c.head_sub_list_id=a.id 
                                                left JOIN (" . $sql . ") as b ON b.ladger_id=c.id 
                                                WHERE 
                                                a.list_of_head_accounts_id='4' AND 
                                                a.list_of_sub_head_accounts_id='2' AND 
                                                a.list_of_sub_head_list_id='35'");
                                                $salesexpense = 0;
                                                if (!empty($chkfourcountled))
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;

                                                        if ($debit != 0 || $cradit != 0) {
                                                            ?>
                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm = $debit - $cradit;
                                                                    $getch = substr($netm, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm, 1, 200));
                                                                    }
                                                                    $salesexpense+=$debit - $cradit;
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>  
                                                <tr><th valign="middle" align="left"><h4>Net Revenue</h4></th>
                                                <td align="right" valign="middle"><u><strong><?php
                                                        $netrevenue = $gettotalrevenue - $salesexpense - $sales_discount - $sales_return;
                                                        echo $obj->amountconvert($netrevenue);
                                                        ?></strong></u></td>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left">&nbsp;</th>
                                                    <th align="center" valign="middle"></th>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left">Other Income</th>
                                                    <th align="right" valign="middle"></th>
                                                </tr>
                                                <?php
                                                $other_income = 0;
                                                $total_other_income = 0;
                                                $sql_o_i = $obj->FlyQuery("SELECT 
                                                b.id,
                                                b.head_sub_list_name,
                                                IFNULL(c.debit,0) as debit,
                                                IFNULL(c.cradit,0) as cradit,
                                                IFNULL((c.debit-c.cradit),0) as total
                                                FROM account_module_ladger_list_properties as b 
                                                LEFT JOIN (" . $sql . ") AS c ON c.ladger_id=b.id
                                                WHERE 
                                                b.head_sub_list_id 
                                                IN 
                                                (SELECT a.id FROM `account_module_head_sub_list` as a WHERE a.list_of_head_accounts_id='3' AND a.list_of_sub_head_accounts_id='1' AND a.list_of_sub_head_list_id='16' AND a.id!='135' AND a.id!='137')");

                                                if (!empty($sql_o_i))
                                                    foreach ($sql_o_i as $row_o_i) {
                                                        $get_o_i = $row_o_i->id;
                                                        $debit_other_income = $row_o_i->debit;
                                                        $cradit_other_income = $row_o_i->cradit;
                                                        $db_other_income = $row_o_i->total;
                                                        if ($db_other_income != 0) {
                                                            ?>

                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $get_o_i; ?>"><?php echo $row_o_i->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm_other_income = $db_other_income;
                                                                    $total_other_income+=$netm_other_income;
                                                                    $getch_other_income = substr($netm_other_income, 0, 1);
                                                                    if ($getch_other_income != '-') {
                                                                        echo $obj->amountconvert($netm_other_income);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm_other_income, 1, 200));
                                                                    }
                                                                    $other_income+=$netm_other_income;
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>


                                                <!--cogs-->
                                                <tr><td colspan="2"></td></tr>
                                                <tr>
                                                    <th valign="middle" align="left"><h4>Total Other Income</h4></th>
                                                <td align="right" valign="middle"><u><strong><?php
                                                        $chktoi = substr($total_other_income, 0, 1);
                                                        if ($chktoi != "-") {
                                                            echo $obj->amountconvert($total_other_income);
                                                        } else {
                                                            echo $obj->amountconvert(substr($total_other_income, 1, 200));
                                                        }
                                                        ?></strong></u></td>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left"><h4>Total Income</h4></th>
                                                <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><u><u><strong><?php
                                                                    $chkti = substr($total_other_income, 0, 1);
                                                                    if ($chkti != "-") {
                                                                        $tincome = $total_other_income + $netrevenue;
                                                                        echo $obj->amountconvert($tincome);
                                                                    } else {
                                                                        $tincome = substr($total_other_income, 1, 200) + $netrevenue;
                                                                        echo $obj->amountconvert($tincome);
                                                                    }
                                                                    ?></strong></u></u></span></td>
                                                </tr>
                                                <tr><th valign="middle" align="left" colspan="2" >&nbsp;</th></tr>
                                                <tr><th valign="middle" align="left" colspan="2" ><h4>Cost of Goods Sold (COGS)</h4></th></tr>

                                                <tr>
                                                    <th valign="middle" align="left" >Beginning Inventory Value</th>
                                                    <td align="right" valign="middle">
                                                        <?php
//                                                        $iyear = date('Y') - 1;
//
//                                                        $cy = date('Y');
//                                                        $from = date("Y-m-d", mktime(0, 0, 0, 12, 31, $cy - 1));
//                                                        $to = date("Y-m-d", mktime(0, 0, 0, 12, 32, $cy - 2));
//                                                        $endfrom = date("Y-m-d", mktime(0, 0, 0, 12, 31, $cy));
//                                                        $endto = date("Y-m-d", mktime(0, 0, 0, 12, 32, $cy - 1));
                                                        /*
                                                          $sql_query=mysql_query("SELECT * FROM inventory WHERE year='$iyear'");
                                                          $fetiyear=mysql_fetch_array($sql_query);
                                                          echo $obj->amountconvert($begininginventory=$fetiyear['begging_value'],2); */

                                                        if ($input_status == 1) {
                                                            
                                                            $st_last=$obj_incomestatement->admin_last_year($input_by);
                                                            $start_st=$st_last[0]->last_year_opening;
                                                            $end_st=$st_last[0]->last_year_end;
                                                            $insql = "SELECT 
                                                            l.ladger_id, 
                                                            l.ladger_date, 
                                                            l.branch_id, 
                                                            SUM(l.debit) as debit, 
                                                            SUM(l.cradit) as cradit, 
                                                            s.store_id as store 
                                                            FROM account_module_ladger as l 
                                                            LEFT JOIN store as s ON s.id=l.branch_id 
                                                            WHERE l.ladger_date>='" .  $start_st . "' AND l.ladger_date<='" . $end_st . "' GROUP BY l.ladger_id";
                                                        } elseif ($input_status == 5) {
                                                            $fields = '';
                                                            $count = 0;
                                                            foreach ($array_ch as $val) {
                                                                if ($count++ != 0)
                                                                    $fields .= ' OR ';
                                                                $fields .= "extra.store = '$val' ";
                                                            }

                                                                $insql = "SELECT extra.* FROM (SELECT 
                                                            l.ladger_id, 
                                                            l.ladger_date, 
                                                            l.branch_id, 
                                                            SUM(l.debit) as debit, 
                                                            SUM(l.cradit) as cradit, 
                                                            s.store_id as store 
                                                            FROM account_module_ladger as l 
                                                            LEFT JOIN store as s ON s.id=l.branch_id 
                                                            WHERE l.ladger_date>='" . $start_st . "' AND l.ladger_date<='" . $end_st . "' GROUP BY l.ladger_id) as extra 
                                                            WHERE " . $fields;
                                                        } else {
                                                                $insql = "SELECT extra.* FROM (SELECT 
                                                            l.ladger_id, 
                                                            l.ladger_date, 
                                                            l.branch_id, 
                                                            SUM(l.debit) as debit, 
                                                            SUM(l.cradit) as cradit, 
                                                            s.store_id as store 
                                                            FROM account_module_ladger as l 
                                                            LEFT JOIN store as s ON s.id=l.branch_id 
                                                            WHERE l.ladger_date>='" . $start_st . "' AND l.ladger_date<='" . $end_st . "' GROUP BY l.ladger_id) as extra 
                                                            WHERE extra.store='" . $input_by . "'";
                                                        }


                                                        $ld_begining_inventory = $obj->FlyQuery("SELECT 
                                                        a.id,
                                                        b.debit,
                                                        b.cradit,
                                                        ( b.debit-b.cradit) as total,
                                                        a.`head_sub_list_name`
                                                        FROM `account_module_ladger_list_properties` as a 
                                                        LEFT JOIN (" . $insql . ") as b ON b.ladger_id=a.id 
                                                        WHERE 
                                                        main_head_id='1' AND head_sub_list_id='46'");

                                                        foreach ($ld_begining_inventory as $row_begining_inventory) {

                                                            $inid_begining_inventory = $row_begining_inventory->id;
                                                            $debit_begining_inventory = $row_begining_inventory->debit;
                                                            $cradit_begining_inventory = $row_begining_inventory->cradit;
                                                            $begininginventory = $row_begining_inventory->total;
                                                        }

                                                        $begininginventory = $debit_begining_inventory - $cradit_begining_inventory;

                                                        echo $obj->amountconvert($begininginventory);

                                                        $ld_ending_inventory = $obj->FlyQuery("SELECT 
                                                        a.id,
                                                        b.debit,
                                                        b.cradit,
                                                        ( b.debit-b.cradit) as total,
                                                        a.`head_sub_list_name`
                                                        FROM `account_module_ladger_list_properties` as a 
                                                        LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id AND b.ladger_date>='2015-12-01' AND b.ladger_date<='2015-12-20' 
                                                        WHERE 
                                                        main_head_id='1' AND head_sub_list_id='46'");
                                                        if (!empty($ld_ending_inventory))
                                                            foreach ($ld_ending_inventory as $row_ending_inventory) {

                                                                $inid_ending_inventory = $row_ending_inventory->id;
                                                                $debit_ending_inventory = $row_ending_inventory->debit;
                                                                $cradit_ending_inventory = $row_ending_inventory->cradit;
                                                                $endinventory = $row_ending_inventory->total;
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left">Add : Direct Expense</th>
                                                    <th align="center" valign="middle">&nbsp;</th>
                                                </tr>     
                                                <?php
                                                $chkfourcountled = $obj->FlyQuery("SELECT 
                                                a.id, 
                                                a.head_sub_list_name, 
                                                IFNULL(b.debit,0) as debit, 
                                                IFNULL(b.cradit,0) as cradit 
                                                FROM 
                                                account_module_ladger_list_properties as a 
                                                LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id 
                                                WHERE 
                                                a.id 
                                                IN 
                                                (SELECT head_sub_list_id FROM `account_module_head_sub_list` WHERE list_of_head_accounts_id ='4' AND list_of_sub_head_accounts_id='1') AND a.head_sub_list_id!='240'");
                                                $cogs = 0;
                                                if (!empty($chkfourcountled))
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;

                                                        if ($debit != 0 || $cradit != 0) {
                                                            ?>

                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm = $debit - $cradit;
                                                                    $sco = $debit - $cradit;

                                                                    $getch = substr($netm, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm, 1, 200));
                                                                    }
                                                                    $cogs+=$debit - $cradit;
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>

<!--                             <tr><th valign="middle" align="left"> Direct Expense</th>
   <td align="right" valign="middle"><?php //echo $obj->amountconvert($cogs,2);         ?></td>
</tr>-->
                                                <!--frieght in-->
                                                <?php
                                                $ld = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit
                                                    FROM 
                                                    account_module_ladger_list_properties as a 
                                                    LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.main_head_id ='4' AND 
                                                    a.head_sub_list_id ='240'");
                                                $fr = 0;
                                                if (!empty($ld))
                                                    foreach ($ld as $row) {

                                                        $inid = $row->id;
                                                        $debit = $row->debit;
                                                        $cradit = $row->cradit;

                                                        if ($debit != 0 || $cradit != 0) {
                                                            ?>

                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm = $debit - $cradit;
                                                                    $getch = substr($netm, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm, 1, 200));
                                                                    }
                                                                    $fr+=$netm;
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                                <tr>
                                                    <th valign="middle" align="left">Inventory Purchase</th>
                                                    <td align="right" valign="middle"><?php
                                                        $inventorypurchase = $obj->FlyQuery("SELECT 
                                                            a.id,
                                                            a.head_sub_list_name,
                                                            IFNULL(b.debit,0) as debit
                                                            FROM 
                                                            account_module_ladger_list_properties as a 
                                                            LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                            WHERE 
                                                            a.main_head_id ='1' AND 
                                                            a.head_sub_list_id ='46'");
                                                        $debit_purchase_inventory = 0;
                                                        if (!empty($inventorypurchase))
                                                            foreach ($inventorypurchase as $row_purchase_inventory) {
                                                                $debit_purchase_inventory+=$row_purchase_inventory->debit;
                                                            }
                                                        $inventorypurchase_op = $debit_purchase_inventory;
                                                        echo $obj->amountconvert($inventorypurchase_op);
                                                        ?></td>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left">Total Direct Expense</th>
                                                    <td align="right" valign="middle"><strong><?php
                                                            $totaldirectexpense = $begininginventory + $fr + $cogs + $inventorypurchase_op;
                                                            echo $obj->amountconvert($totaldirectexpense);
                                                            ?></strong></td>
                                                </tr> 
                                                <tr>
                                                    <th valign="middle" align="left">(Less) </th>
                                                    <th align="right" valign="middle">&nbsp;</th>
                                                </tr>
                                                <!--purchase_discount-->
                                                <?php
                                                $ld_purchase_discount = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit
                                                    FROM 
                                                    account_module_ladger_list_properties as a 
                                                    LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.main_head_id ='2' AND 
                                                    a.head_sub_list_id ='382'");
                                                $purchase_discount = 0;
                                                if (!empty($ld_purchase_discount))
                                                    foreach ($ld_purchase_discount as $row_purchase_discount) {

                                                        $inid_purchase_discount = $row_purchase_discount->id;
                                                        $debit_purchase_discount+=$row_purchase_discount->debit;
                                                        $cradit_purchase_discount+=$row_purchase_discount->cradit;
                                                        $purchase_discount+=$row_purchase_discount->total;
                                                        if ($purchase_discount != 0) {
                                                            ?>
                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid_purchase_discount; ?>"><?php echo $row_purchase_discount->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm_purchase_discount = $purchase_discount;
                                                                    $getch = substr($netm_purchase_discount, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm_purchase_discount);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm_purchase_discount, 1, 200));
                                                                    }
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>

                                                <?php
                                                $ld_purchase_return = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total
                                                    FROM 
                                                    account_module_ladger_list_properties as a 
                                                    LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.main_head_id ='2' AND 
                                                    a.head_sub_list_id ='375'");
                                                if (!empty($ld_purchase_return))
                                                    foreach ($ld_purchase_return as $row_purchase_return) {

                                                        $inid_purchase_return = $row_purchase_return->id;
                                                        $debit_purchase_return = $row_purchase_return->debit;
                                                        $cradit_purchase_return = $row_purchase_return->cradit;
                                                        $purchase_return = $row_purchase_return->total;

                                                        if ($purchase_return != 0) {
                                                            ?>

                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid_purchase_return; ?>"><?php echo $row_purchase_return->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm_purchase_return = $purchase_return;
                                                                    $getch = substr($netm_purchase_return, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm_purchase_return);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm_purchase_return, 1, 200));
                                                                    }
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?>


                                                <tr>
                                                    <th valign="middle" align="left">Net Direct Expense</th>
                                                    <td align="right" valign="middle"><strong><?php
                                                            $netdiorectexpense = $totaldirectexpense + ($purchase_discount + $purchase_return);
                                                            echo $obj->amountconvert($netdiorectexpense);
                                                            ?></strong></td>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left">End Inventory Value</th>
                                                    <td align="right" valign="middle"><?php
                                                        echo $obj->amountconvert($endinventory);
                                                        ?></td>
                                                </tr> 
                                                <tr><th valign="middle" align="left"><h4>Total Cost of Goods Sold (COGS)</h4></th>
                                                <td align="right" valign="middle"><u><strong><?php
                                                        $cogss = $netdiorectexpense - $endinventory;
                                                        echo $obj->amountconvert($cogss);
                                                        ?></strong></u></td>
                                                </tr>

                                                <tr><th valign="middle" align="left"></th>
                                                    <th align="right" valign="middle"></th>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left"><h4>Gross Profit/(Loss)</h4></th>
                                                <td align="right" valign="middle"><u><strong><?php
                                                        $gross_profit = $tincome - $cogss;
                                                        $lkj = substr($gross_profit, 0, 1);
                                                        if ($lkj != "-") {
                                                            echo $obj->amountconvert($gross_profit);
                                                        } else {
                                                            echo $obj->amountconvert(substr($gross_profit, 1, 200));
                                                        }
                                                        ?></strong></u></td>
                                                </tr>
                                                <tr><td colspan="2"></td></tr>
                                                <!--operating expenses-->
                                                <tr><td colspan="2"></td></tr>
                                                <tr><th valign="middle" align="left" colspan="2"><h4>Operating Expenses</h4></th></tr>
                                                <?php
                                                $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total
                                                    from account_module_ladger_list_properties as a
                                                    LEFT JOIN (" . $sql . ") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id='4' AND list_of_sub_head_accounts_id='2' AND list_of_sub_head_list_id!='35') AND main_head_id='4'");
                                                $opex = 0;
                                                if (!empty($chkfourcountled))
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;
                                                        if ($debit != 0) {
                                                            ?>
                                                            <tr>
                                                                <th valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                </th>
                                                                <td align="right" valign="middle">
                                                                    <?php
                                                                    $netm = $cogsexpense->total;
                                                                    $getch = substr($netm, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm, 1, 200));
                                                                    }
                                                                    $opex+=$debit - $cradit;
                                                                    ?></strong></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                ?> 
                                                <tr><th valign="middle" align="left"><h4>Total Operating Expenses</h4></th>
                                                <td align="right" valign="middle"><u><strong><?php echo $obj->amountconvert($opex); ?></strong></u></td>
                                                </tr>
                                                <tr><th valign="middle" align="left">&nbsp;</th>
                                                    <th align="right" valign="middle"></th>
                                                </tr>
                                                <tr>
                                                    <th valign="middle" align="left"><h4>NET PROFIT/(LOSS)</h4></th>
                                                <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><u><u><strong><?php
                                                                    $netprofit = $gross_profit - $opex;
                                                                    $akj = substr($netprofit, 0, 1);

                                                                    if ($akj != "-") {
                                                                        echo $obj->amountconvert($netprofit);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netprofit, 1, 200));
                                                                    }

                                                                    $year = date('Y');
                                                                    $date = date('Y-m-d');
                                                                    $chkincomestatement = $obj->exists_multiple("account_module_income_statement", array("year" => $year, "date" => $date));
                                                                    if ($chkincomestatement != 0) {
                                                                        $obj->update("account_module_income_statement", array("year" => $year, "amount" => $netprofit, "totalincome" => $tincome, "directexpense" => $cogs, "grossprofit" => $gross_profit, "branch_id" => $input_by, "date" => $date));
                                                                    } else {
                                                                        $obj->insert("account_module_income_statement", array("year" => $year, "amount" => $netprofit, "totalincome" => $tincome, "directexpense" => $cogs, "grossprofit" => $gross_profit, "branch_id" => $input_by, "date" => $date));
                                                                    }
                                                                    ?>
                                                                </strong></u></u></span>
                                                </td>
                                                </tr>
                                                <tr><th valign="middle" align="left">&nbsp;</th>
                                                    <th align="right" valign="middle"></th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->
                                        <!-- /selects, dropdowns -->
                                    </div>
                                    <!-- /general form elements -->     
                                    <div class="clearfix"></div>
                                    <!-- Default datatable -->
                                    <!-- /default datatable -->
                                    <!--tab 1 content start from here-->  
                                </div>
                            </div>
                            <!-- General form elements -->
                            <!-- /general form elements -->
                            <div class="clearfix"></div>
                            <!-- Default datatable -->
                            <!-- /default datatable -->
                            <!-- Content End from here customized -->
                            <div class="separator-doubled"></div> 
                        </div>
                        <!-- /content container -->
                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');       ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
