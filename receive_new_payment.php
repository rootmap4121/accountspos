<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function SaveCustomer()
            {
                var date = $('.datepicker').val();
                var amount = $('#paid_amount').val();
                var shop_id = $('#shop_id').val();
                var customer_id = $('#customer_id').val();
                var ledger_id = $('#ledger_id').val();
                var memo = $('.span12 .memo').val();
                if (date != "" && shop_id != "" && customer_id != "" && ledger_id != "")
                {
                    //console.log(date+" "+amount+" "+shop_id+" "+customer_id+" "+ledger_id+" "+memo);
                    $.post("lib/payment_receive.php", {'st': 1, 'payment_date': date, 'amount': amount, 'shop_id': shop_id, 'customer_id': customer_id, 'ledger_id': ledger_id, 'memo': memo}, function (data)
                    {
                        if (data == 1)
                        {
                            clear();
                            $.jGrowl('Saved, Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        }
                        else if (data == 2)
                        {
                            $.jGrowl('Failed, Already Exists.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                        }
                        else
                        {
                            $.jGrowl('Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });
                }
                else
                {
                    $.jGrowl('Failed, Some Field is Empty.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                }
            }

            function clear()
            {
                $('.datepicker').val("");
                $('#paid_amount').val("");
                $('#shop_id').val("");
                $('#customer_id').val("");
                $('#ledger_id').val("");
                $('.span12 .memo').val("");
            }

            nucleus(document).ready(function ()
            {
                nucleus('#select-customer-id').find('.select2-input').keyup(function () {
                    var getvalue = nucleus(this).val();
                    var getlength = getvalue.length;
                    var place = nucleus('#select-customer-id').find('select').attr('id');
                    $('#' + place).html("");
                    if (getlength >= 4)
                    {
                        $.post("lib/search_controller.php", {'st': 1, 'table': "account_module_customer", 'search': getvalue, 'field_a': "id", 'field_b': "concat(fname,' ',lname)"}, function (fetch) {
                            var datacl = jQuery.parseJSON(fetch);
                            var opt = datacl.data;
                            $('#' + place).html(opt);
                        });
                    }
                });
            });


        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Receive New Payment Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <blockquote style="margin-top:-20px;">
                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                    </blockquote>


                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="row-fluid  span12 well">

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Payment Date *</label>
                                                        <input type="text" name="date" class="datepicker" placeholder="Payment Date" />
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="span12">Amount *</label>
                                                        <input type="text" name="amount" id="paid_amount" class="span5" placeholder="Payment Amount" />
                                                    </div>

                                                </div>
                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">


                                                    <div class="control-group">
                                                        <label class="span12">Choose Shop *</label>
                                                        <select name="shop_id" data-placeholder="Select Shop" class="select-search" id="shop_id" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql2=$obj->SelectAll("branch");
                                                            if (!empty($sql2))
                                                                foreach ($sql2 as $rows) {
                                                                    ?>
                                                                    <option value="<?php echo $rows->branch_id; ?>"><?php echo $rows->name; ?></option>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="control-group" id="select-customer-id">
                                                        <label class="span12">Choose Customer *</label>
                                                        <select name="customer_id" data-placeholder="Select Customer"  class="minimum-select"  id="customer_id" tabindex="2">
                                                            <option value=""></option>

                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="span4">



                                                    <div class="control-group">
                                                        <label class="span12">Receive Ledger *</label>
                                                        <select name="ledger_id" data-placeholder="Select Ledger" class="select-search" id="ledger_id" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            $ss=$obj->FlyQuery("SELECT id,head_sub_list_name FROM account_module_ladger_list_properties WHERE main_head_id='1'");
                                                            foreach ($ss as $wow) {
                                                                ?>
                                                                <option value="<?php echo $wow->id; ?>"><?php echo $wow->head_sub_list_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="span12">Note/Memo/Description</label>
                                                        <input type="text" name="memo" class="span12 memo" placeholder="Regular field" />
                                                    </div>

                                                    <div class="control-group">
                                                        <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                            <i class="icon-plus-sign"></i> Save Record </button>
                                                    </div>


                                                </div>
                                                <!-- /selects, dropdowns -->



                                            </div>
                                            <!-- /general form elements -->


                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->


                                        </fieldset>

                                    </form>

                                    <!--tab 1 content start from here-->

                                </div>
                                <!-- General form elements -->












                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">

                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Rec. ID</th>
                                                        <th>Customer</th>
                                                        <th>Receive Ledger</th>
                                                        <th>Total Amount</th>
                                                        <th>Receiver</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
<?php
$sql=$obj->FlyQuery("SELECT `id`,(select concat(fname,' ',lname) FROM account_module_customer WHERE account_module_customer.id=account_module_invoice_payment.cid) as customer_name,`cid`,`date`,`pa`,`amount`,(SELECT store.name FROM store WHERE store.id=account_module_invoice_payment.`input_by`) as receiver FROM `account_module_invoice_payment` WHERE date='" . date('Y-m-d') . "'");
$i=1;
if (!empty($sql))
    foreach ($sql as $row):
        ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $row->id; ?></td>
                                                                <td><?php echo $row->customer_name; ?></td>
                                                                <td><?php echo $row->pa; ?></td>
                                                                <td><?php echo $row->amount; ?></td>
                                                                <td><?php echo $row->receiver; ?></td>
                                                                <td><?php echo $row->date; ?></td>

                                                            </tr>
        <?php
        $i++;
    endforeach;
?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div>



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
<?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
