<?php
extract($_GET);
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Individual Ledger Report Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->
                        <div class="body">
                            <!-- Content container -->
                            <div class="container">
                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong><?php echo $obj->SelectAllByVal("account_module_ladger_list_properties","id",$ladger_id,"head_sub_list_name"); ?></strong></h3>
                                            <h5 align="center"><strong>Expected</strong></h5>
                                            <h4 align="center"><strong>From : 1/1/2015 - To : 1/1/2016</strong></h4>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Tracking ID</th>
                                                        <th>Ledger Generated From</th>
                                                        <th style="text-align:right; padding-right:25px;">Debit</th>
                                                        <th style="text-align:right; padding-right:25px;">Cradit</th>
                                                        <th style="text-align:right; padding-right:25px;">Net Movement</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    function ledgerGenerated($invoice) {
                                                        $obj=new db_class();
                                                        if (substr($invoice,0,1) == "K") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT `date` FROM `account_module_invoice` WHERE `id`='".substr($invoice,1,100)."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $linkname = "view_purchase.php?id=".substr($invoice,1,100);
                                                            $linktitle = "Purchase Invoice : (".$dates.")";
                                                        }elseif (substr($invoice,0,1) == "S") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT `date` FROM `account_module_invoice` WHERE `id`='".substr($invoice,1,100)."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $linkname = "view_sales.php?id=".substr($invoice,1,100);
                                                            $linktitle = "Sales Invoice : (".$dates.")";
                                                        }elseif (substr($invoice,0,1) == "J") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT `date` FROM `account_module_invoice` WHERE `id`='".substr($invoice,1,100)."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $linkname = "edit_single_entry_journal.php?journal=".substr($invoice,1,100);
                                                            $linktitle = "Journal : (".$dates.")";
                                                        }elseif (substr($invoice,0,1) == "E") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT id,vdate as `date` FROM `account_module_office_expense_voucher` WHERE `expense_id`='".$invoice."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $eid=$sqlgetdate[0]->id;
                                                            $linkname = "view_expense_voucher.php?expense_id=".$eid;
                                                            $linktitle = "Expense : (".$dates.")";
                                                        }elseif (substr($invoice,0,1) == "P") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT id,date as `date` FROM `account_module_invoice_payment` WHERE `invoice_id`='".$invoice."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $eid=$sqlgetdate[0]->id;
                                                            $linkname = "receive_payment_list.php?payment_receive=".$eid;
                                                            $linktitle = "Receive Payment : (".$dates.")";
                                                        }elseif (substr($invoice,0,1) == "Q") {
                                                            $sqlgetdate=$obj->FlyQuery("SELECT id,date as `date` FROM `account_module_bill_payment` WHERE `bill_id`='".$invoice."'");
                                                            $dates=$obj->dates($sqlgetdate[0]->date);
                                                            $eid=$sqlgetdate[0]->id;
                                                            $linkname = "paid_payment_list.php?payment_paid=".$eid;
                                                            $linktitle = "Paid Payment : (".$dates.")";
                                                        }
                                                        
                                                        $dd=array("link"=>$linkname,"title"=>$linktitle);
                                                        return $dd;
//                                                        elseif ($njc == "J") {
//                                                            $linkname = "viewjournal.php";
//                                                            $linktitle = "Journal";
//                                                        } elseif ($njc == "N") {
//                                                            $linkname = "viewinvoice.php";
//                                                            $linktitle = "Invoice";
//                                                        } elseif ($njc == "B") {
//                                                            $linkname = "viewbill.php";
//                                                            $linktitle = "Bill";
//                                                        } elseif ($njc == "O") {
//                                                            $linkname = "view_officeexpense_voucher_ladger.php";
//                                                            $linktitle = "Office Expense ";
//                                                        } elseif ($njc == "Sal") {
//                                                            $linkname = "view_transaction_salary.php";
//                                                            $linktitle = "Salary Voucher ";
//                                                        } else {
//                                                            $linkname = "#";
//                                                            $linktitle = "";
//                                                        }
                                                    }

                                                    $debit_total = 0;
                                                    $credit_total = 0;
                                                    $netmovement_total = 0;
                                                    $sqlledger = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.ladger_id,
                                                    a.invoice_id,
                                                    IFNULL(a.debit,0) as debit,
                                                    IFNULL(a.cradit,0) as cradit,
                                                    IFNULL((a.debit-a.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger as a
                                                    WHERE 
                                                    ladger_id='" . $ladger_id . "'");
                                                    $a = 1;
                                                    if (!empty($sqlledger))
                                                        foreach ($sqlledger as $row):
                                                            ?>
                                                            <tr>
                                                                <td valign="middle" align="left">
        <?php echo $a; ?>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <strong>LTID</strong>-<strong><?php echo $row->id; ?></strong>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <a href="<?php echo ledgerGenerated($row->invoice_id)['link']; ?>"><?php echo ledgerGenerated($row->invoice_id)['title']; ?></a>
                                                                </td>
                                                                <td valign="middle" align="right"><?php echo $obj->amountconvert($row->debit); ?></td>
                                                                <td align="right" valign="middle"><?php echo $obj->amountconvert($row->cradit); ?></td>
                                                                <td align="right" valign="middle"><strong>
                                                            <?php
                                                            echo $obj->amountconvert($row->total);
                                                            $debit_total+=$row->debit;
                                                            $credit_total+=$row->cradit;
                                                            $netmovement_total+=$row->total;
                                                            ?></strong></td>
                                                            </tr>
        <?php
        $a++;
    endforeach;
?> 
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td align="right"><strong>Total = </strong></td>
                                                        <td align="right"><strong><?php echo number_format($debit_total, 2); ?></strong></td>
                                                        <td align="right"><strong><?php echo number_format($credit_total, 2); ?></strong></td>
                                                        <td align="right"><strong><?php echo number_format($netmovement_total, 2); ?></strong></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                        <!-- Selects, dropdowns -->
                                        <!-- /selects, dropdowns -->
                                    </div>
                                    <!-- /general form elements -->     
                                    <div class="clearfix"></div>
                                    <!-- Default datatable -->
                                    <!-- /default datatable -->
                                    <!--tab 1 content start from here-->  

                                </div>
                            </div>
                            <!-- General form elements -->

                            <!-- /general form elements -->
                            <div class="clearfix"></div>
                            <!-- Default datatable -->
                            <!-- /default datatable -->
                            <!-- Content End from here customized -->
                            <div class="separator-doubled"></div> 
                        </div>
                        <!-- /content container -->
                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
<?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');     ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
