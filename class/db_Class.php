<?php

class db_class {

    public function pdocon() {
        $db = new PDO("mysql:host=localhost;dbname=account_pos;", "root", "", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        //$db=new PDO("mysql:host=localhost;dbname=nucleusp_posaccounts;","nucleusp_posacc","F@h@d23713266");
        $db->exec('SET NAMES utf8');
        return $db;
    }

    function amountconvert($amounts) {
        $check = substr($amounts, 0, 1);
        if ($check == "-") {
            return "(" . number_format(substr($amounts, 1, 200), 2) . ")";
        } else {
            return number_format($amounts, 2);
        }
    }

    function dates($value) {
        //2015-02-28
        $date = substr($value, 8, 2);
        $month = substr($value, 5, 2);
        $year = substr($value, 0, 4);
        if($value!="")
        {
            return $date . "/" . $month . "/" . $year;
        }
        else 
        {
            return "00/00/0000";
        }
    }

    function password($password) {
        $st = "TotalSales";
        $newpassword = $st . "-" . $password;
        $convert = md5($newpassword);
        return $convert;
    }

    function baseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "account_nucleus/accounts/";
        //$web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    function Error($msg, $location) {
        $errmsg_arr[] = "<div class='notice outer'>
                            <div class='note note-danger'>
                            <button type='button' class='close'>×</button>
                            <strong>Notice!</strong> " . $msg . "</div>
                        </div>";
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location:" . $location);
            exit();
        }
    }

    function Success($msg, $location) {
        $errmsg_arr[] = "<div class='notice outer'>
                            <div class='note note-success'>
                            <button type='button' class='close'>×</button>
                            <strong>Notice!</strong> " . $msg . "</div>
                        </div>";
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location:" . $location);
            exit();
        }
    }

    function ShowMsg() {
        if (isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) > 0) {
            foreach ($_SESSION['ERRMSG_ARR'] as $msg) {
                unset($_SESSION['ERRMSG_ARR']);
                return $msg;
            }
        }

        if (isset($_SESSION['SMSG_ARR']) && is_array($_SESSION['SMSG_ARR']) && count($_SESSION['SMSG_ARR']) > 0) {
            foreach ($_SESSION['SMSG_ARR'] as $msgs) {
                unset($_SESSION['SMSG_ARR']);
                return $msgs;
            }
        }
    }

    function bodyhead() {

        /* <script type="text/javascript" src=' . $this->baseUrl("js/jquery-1.9.1.js") . '></script>
          <script type="text/javascript" src=' . $this->baseUrl("js/jquery-migrate-1.0.0.js") . '></script> */
        return '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
				<link rel="icon" href="/favicon.ico" type="image/x-icon">
				<title> Nucleus  </title>
				<link  type="text/css"  href=' . $this->baseUrl("css/main.css") . ' rel="stylesheet" />
				<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  				<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/jquery_ui_custom.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/charts/excanvas.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/charts/jquery.flot.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/charts/jquery.flot.resize.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/charts/jquery.sparkline.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.tagsinput.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.inputlimiter.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.maskedinput.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.autosize.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.ibutton.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.dualListBox.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.validate.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.uniform.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.select2.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/forms/jquery.cleditor.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/uploader/plupload.js") . '></script>

				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/uploader/jquery.plupload.queue.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/wizard/jquery.form.wizard.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/wizard/jquery.form.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.collapsible.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.timepicker.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.jgrowl.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.pie.chart.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.fullcalendar.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.elfinder.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/ui/jquery.fancybox.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/tables/jquery.dataTables.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/bootstrap/bootstrap.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/bootstrap/bootstrap-bootbox.min.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/bootstrap/bootstrap-progressbar.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/plugins/bootstrap/bootstrap-colorpicker.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/functions/custom.js") . '></script>
				<script type="text/javascript" src=' . $this->baseUrl("js/charts/chart.js") . '></script>
				<script src=' . $this->baseUrl("js/jquery.min.js") . '></script>
				<script>
				var nucleus=$.noConflict();
				</script>';
    }

    /**
     * Insert query for Object
     * @param type $object
     * @param type $object_array
     * @return string/Exception
     */
    function insert($object, $object_array) {
        $db = $this->pdocon();
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($count++ != 0)
                $fields .= ', ';

            $fields .= "`$col` = '$val'";
        }

        try {
            $query = "INSERT INTO `$object` SET $fields";
            $sql = $db->prepare($query);
            if ($sql->execute() == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    function insertAndReturnID($object, $object_array) {
        $db = $this->pdocon();
        $fields = '';
        $bindfields = '';
        $ss = 1;
        foreach ($object_array as $col => $val) {
            if ($count++ != 0)
                $fields .= ', ';

            $fields .= "`$col` = '$val'";
        }

        try {
            $query = "INSERT INTO `$object` SET $fields";
            $sql = $db->prepare($query);
            if ($sql->execute() == 1) {
                return $db->lastInsertId();
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    function lastid($object, $column) {
        $db = $this->pdocon();
        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' ORDER BY ' . $column . ' DESC LIMIT 1
		  ');

        //foreach ($object_array as $col => $val){ $sql->bindValue(':'.$col,$val, PDO::PARAM_STR); }		

        if ($sql->execute()) {
            return $sql->fetchAll(PDO::FETCH_OBJ);
        } else {
            return 0;
        }
    }

    /**
     * if the object is exists
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function SelectAllByID_Multiple($object, $object_array) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' WHERE ' . $fields . '
		  ');
        $arrayparam = "";
        foreach ($object_array as $col => $val) {
            $arrayparam[] = $val;
        }

        if ($sql->execute($arrayparam)) {
            $datacount = $sql->rowCount();
            if ($datacount == 0) {
                return 0;
            } else {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            }
        } else {
            return 0;
        }
    }

    function exists_multiple($object, $object_array) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }


        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' WHERE ' . $fields . '
		  ');
        $arrayparam = "";
        foreach ($object_array as $col => $val) {
            $arrayparam[] = $val;
        }

        if ($sql->execute($arrayparam)) {
            return $sql->rowCount();
        } else {
            return 0;
        }
    }

    function FlyQuery($slysql) {
        $db = $this->pdocon();
        $sql = $db->prepare($slysql);

        //var_dump($sql);

        try {
            $sql->execute();
            $datacount = $sql->rowCount();
            if ($datacount == 0) {
                return 0;
            } else {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    function FlyQueryExcute($slysql) {
        $db = $this->pdocon();
        $sql = $db->prepare($slysql);
        if ($sql->execute()) {
            return 1;
        } else {
            return 0;
        }
    }

    function FlyQueryWithCond($object, $select, $object_array, $limit) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        if ($limit == 0) {
            $sql = $db->prepare('
			  select ' . $select . ' from (SELECT * FROM ' . $object . '   WHERE ' . $fields . ' ORDER by id DESC) ' . $object . ' ORDER BY id ASC
			  ');
        } else {
            $sql = $db->prepare('
			  select ' . $select . ' from (SELECT * FROM ' . $object . '   WHERE ' . $fields . ' ORDER by id DESC LIMIT ' . $limit . ') ' . $object . ' ORDER BY id ASC
			  ');
        }
        $arrayparam = "";
        foreach ($object_array as $col => $val) {
            $arrayparam[] = $val;
        }
        if ($sql->execute($arrayparam)) {
            $datacount = $sql->rowCount();
            if ($datacount == 0) {
                return 0;
            } else {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            }
        } else {
            return 0;
        }
    }

    function FlyQueryWithCondExists($object, $select, $object_array, $limit) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        if ($limit == 0) {
            $sql = $db->prepare('
			  select ' . $select . ' from (SELECT * FROM ' . $object . '   WHERE ' . $fields . ' ORDER by id DESC) ' . $object . ' ORDER BY id ASC
			  ');
        } else {
            $sql = $db->prepare('
			  select ' . $select . ' from (SELECT * FROM ' . $object . '   WHERE ' . $fields . ' ORDER by id DESC LIMIT ' . $limit . ') ' . $object . ' ORDER BY id ASC
			  ');
        }
        $arrayparam = "";
        foreach ($object_array as $col => $val) {
            $arrayparam[] = $val;
        }
        if ($sql->execute($arrayparam)) {
            return $datacount = $sql->rowCount();
        } else {
            return 0;
        }
    }

    function SelectAllByVal($object, $field, $fval, $fetch) {
        $count = 0;
        $fields = '';
        $db = $this->pdocon();
        $fields .= $field . "=:" . $field;

        $sql = $db->prepare('
		  SELECT ' . $fetch . ' FROM ' . $object . ' WHERE ' . $fields . '
		  ');

        $sql->bindParam(':' . $field, $fval);
        if ($sql->execute()) {
            $datacount = $sql->fetchColumn();
            return $datacount;
        } else {
            return 0;
        }
    }

    function SelectAllByVal2($object, $field, $fval, $field2, $fval2, $fetch) {
        $count = 0;
        $fields = '';
        $db = $this->pdocon();
        $fields .= $field . "=:" . $field;
        $fields .= " AND ";
        $fields .= $field2 . "=:" . $field2;
        $sql = $db->prepare('
		  SELECT ' . $fetch . ' FROM ' . $object . ' WHERE ' . $fields . '
		  ');

        $sql->bindParam(':' . $field, $fval);
        $sql->bindParam(':' . $field2, $fval2);

        if ($sql->execute()) {
            $datacount = $sql->fetchColumn();
            return $datacount;
        } else {
            return 0;
        }
    }

    function TotalRows($object) {
        $count = 0;
        $fields = '';
        $db = $this->pdocon();
        $sql = $db->prepare('
		  SELECT * FROM ' . $object . '
		  ');
        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            return $datacount;
        } else {
            return 0;
        }
    }

    /**
     * Select all the objects
     * @param type $object
     * @return array
     */
    function SelectAll($object) {
        $count = 0;
        $fields = '';
        $db = $this->pdocon();
        $sql = $db->prepare('
		  SELECT * FROM ' . $object . '
		  ');
        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            if ($datacount != 0) {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function SelectAllOrder($object, $order) {
        $db = $this->pdocon();
        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' ORDER BY id ' . $order . '
		  ');


        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            if ($datacount != 0) {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function SelectAll_ddate($object, $field, $startdate, $enddate) {
        $fields = '';
        $db = $this->pdocon();
        $fields .= $field . ">=:" . $field . "1";
        $fields .= " AND ";
        $fields .= $field . "<=:" . $field . "2";

        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' WHERE ' . $fields . '
		  ');

        $sql->bindParam(':' . $field . "1", $startdate);
        $sql->bindParam(':' . $field . "2", $enddate);


        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            if ($datacount != 0) {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function SelectAllByIDOrderLimit($object, $object_array, $order, $limit) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=:" . $col;
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=:" . $col;
            }
            $ss++;
        }

        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' WHERE ' . $fields . ' ORDER BY id ' . $order . ' LIMIT ' . $limit . '
		  ');
        foreach ($object_array as $col => $val) {
            $sql->bindParam(':' . $col, $val);
        }
        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            if ($datacount == 0) {
                return 0;
            } else {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            }
        } else {
            return 0;
        }
    }

    function SelectAllLimit($object, $limit) {
        $db = $this->pdocon();
        $sql = $db->prepare('
		  SELECT * FROM ' . $object . ' LIMIT ' . $limit . '
		  ');


        if ($sql->execute()) {
            $datacount = $sql->rowCount();
            if ($datacount != 0) {
                return $sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Select object by ID
     * @param type $object
     * @param type $object_array
     * @return int
     */

    /**
     * Delete the object from database
     * @param type $object
     * @param type $object_array
     * @return string|\Exception
     */
    function delete($object, $object_array) {

        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ' AND ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        $sql = $db->prepare('
		  DELETE FROM ' . $object . ' WHERE ' . $fields . '
		  ');

        $arrayparam = "";
        foreach ($object_array as $col => $val) {
            $arrayparam[] = $val;
        }

        if ($sql->execute($arrayparam)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Delete the object
     * @param type $object
     * @param type $object_array
     */
    function update($object, $object_array) {
        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ', ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        $sql = $db->prepare('
		  UPDATE ' . $object . ' 
		  	SET ' . $fields . ' 
				WHERE ' . $key . ' = ' . $value . '
		  ');

        $bindarray = '';
        foreach ($object_array as $col => $val) {
            $bindarray[] = $val;
        }

        if ($sql->execute($bindarray)) {
            return 1;
        } else {
            return 0;
        }
    }

    function updateUsingMultiple($object, $object_array, $object_array2) {
        $count = 0;
        $fields = '';
        $fields2 = '';
        $ss = 1;
        $sss = 1;
        $db = $this->pdocon();
        $count_col = count($object_array);
        foreach ($object_array as $col => $val) {
            if ($ss != $count_col) {
                $fields .= $col . "=?";
                $fields .= ', ';
            } else {
                $fields .= $col . "=?";
            }
            $ss++;
        }

        $count_col2 = count($object_array2);
        foreach ($object_array2 as $col2 => $val2) {
            if ($sss != $count_col2) {
                $fields2 .= $col2 . "=?";
                $fields2 .= ' AND ';
            } else {
                $fields2 .= $col2 . "=?";
            }
            $sss++;
        }


        $sql = $db->prepare('
		  UPDATE ' . $object . ' SET ' . $fields . ' WHERE ' . $fields2 . '
		  ');

        $allnewarray = array();
        foreach ($object_array as $col => $val) {
            //$sql->bindParam(':'.$col, $val); 
            $allnewarray[] = $val;
        }

        foreach ($object_array2 as $col2 => $val2) {
            //$sql->bindParam(':'.$col2, $val2);  
            $allnewarray[] = $val2;
        }

        if ($sql->execute($allnewarray)) {
            return 1;
        } else {
            return 0;
        }
    }

    function filename() {
        return basename($_SERVER['PHP_SELF']);
    }

    function amount_incre($object, $field, $amount, $cond1, $val1) {

        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();


        $sql = $db->prepare('
		  UPDATE ' . $object . ' 
		  	SET ' . $field . '=' . $field . '+' . $amount . ' 
				WHERE ' . $cond1 . '=' . $val1 . '
		  ');

        if ($sql->execute()) {
            return 1;
        } else {
            return 0;
        }
    }

    function amount_decre($object, $field, $amount, $cond1, $val1) {

        $count = 0;
        $fields = '';
        $bindfields = '';
        $ss = 1;
        $db = $this->pdocon();


        $sql = $db->prepare('
		  UPDATE ' . $object . ' 
		  	SET ' . $field . '=' . $field . '-' . $amount . ' 
				WHERE ' . $cond1 . '=' . $val1 . '
		  ');

        if ($sql->execute()) {
            return 1;
        } else {
            return 0;
        }
    }

    function limit_words($string, $word_limit) {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit)) . "...";
    }

    function dmy($month) {
        $chkj = strlen($month);
        if ($chkj == 1) {
            return $chkjval = "0" . $month;
        } else {
            return $chkjval = $month;
        }
    }

}

?>
