<?php
include('class/auth.php');
include('./querysheld/contra_entry_journal.php');
$obj_contraentryjournal = new ContraEntryJournal();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_contraentryjournal->admin($from, $to);
    } else {
        $sql = $obj_contraentryjournal->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_contraentryjournal->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_contraentryjournal->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_contraentryjournal->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_contraentryjournal->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_contraentryjournal->manager($from, $to, $input_by);
    } else {
        $sql = $obj_contraentryjournal->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_contraentryjournal->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_contraentryjournal->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>

        <script>

            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Journal Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("lib/single_entry_journal.php", {'st': 3, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            $.jGrowl('Data, Successfully Delete.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        }
                        else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }

            function view(id)
            {
                var c = confirm("are you sure to view this Journal Record ?.");
                if (c)
                {
                    $('#tr' + id).css("background", "#999");
                    window.location.replace("./edit_single_entry_journal.php?journal=" + id);
                }
                else
                {
                    $('#tr' + id).css("background", "none");
                }
            }
        </script>
        
    </head>

    <body>
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Contra Entry Journal List Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">


                                
                                


                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Contra Entry Journal List</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Journal ID</th>
                                                        <th>Description</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                        <th style="text-align:center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                                                                                        <!--<tr><td colspan="5"><?php echo $chk; ?> Journal Transaction Record Found </td></tr>-->
                                                    <?php
//                                                    $sql = $obj->FlyQuery("select 
//                                                    a.id,
//                                                    a.jd_id,
//                                                    a.jd,
//                                                    a.jddate,
//                                                    sum(b.debit) as total,
//                                                    s.store_id as store
//                                                    from 
//                                                    account_module_journal_description as a 
//                                                    left join account_module_ladger as b on b.link_id=a.link_id 
//                                                    LEFT JOIN store as s ON s.id=a.branch_id
//                                                    WHERE a.`status`='2' 
//                                                    GROUP BY a.link_id");
                                                    $total_amount=0;
                                                    $total_quantity=0;
                                                    $i = 1;
                                                    if (!empty($sql))
                                                        foreach ($sql as $row) {
                                                            ?>
                                                            <tr class="em" id="tr<?php echo $row->id; ?>">
                                                                <td> <?php echo $i; ?> </td>
                                                                <td align="left"><?php echo $row->jd_id; ?></td>
                                                                <td align="left"><?php echo $row->jd; ?></td>

                                                                <td> 
                                                                    <?php
                                                                     echo $obj->amountconvert($row->total);
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $obj->dates($row->jddate); ?> </td>
                                                                <td align="center" valign="middle">
                                                                    <a href="#" onClick="view(<?php echo $row->id; ?>)"><i class="icon-list"></i></a>
                                                                    <a href="#" onClick="deleteR(<?php echo $row->id; ?>)"><i class="icon-trash"></i></a> 
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $total_amount+=$row->total;
                                                            $total_quantity+=1;
                                                            $i++;
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3" style="font-weight: bolder; text-align: right;">Total Amount = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_amount); ?></td>
                                                        <td style="font-weight: bolder; text-align: right;">Quantity = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_quantity); ?></td>
                                                    </tr>
                                                    
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->


                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>
       
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
