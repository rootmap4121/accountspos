<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script type="text/javascript">
            // Use jQuery via jQuery() instead of via $()			 			
            nucleus(document).ready(function () {
                var expense = '<?php echo $_GET['expense_id']; ?>';
                nucleus.post('./lib/expense.php', {'st': 3, 'expense': expense}, function (data) {
                    var datacl = jQuery.parseJSON(data);
                    var status = datacl.status;
                    if (status == 1)
                    {
                        var alldata=datacl.alldata;
                        var voucher_id=datacl.voucher_id;
                        var vdate=datacl.vdate;
                        var branch_id=datacl.branch_id;
                        var damount=datacl.damount;
                        nucleus('#voucher_id').html(voucher_id);
                        nucleus('#vdate').html(vdate);
                        nucleus('#branch_id').html(branch_id);
                        nucleus('#POITable').html(alldata);
                        nucleus('#totalamount').val(damount);
                        //alert(alldata);
                    }
                });
            });

        </script>

    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Expense Voucher Detail : <?php echo $_GET['expense_id']; ?> </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="row-fluid  span12 well">   

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                    <h5>
                                                        <div class="span5">Voucher ID : </div>
                                                        <div class="span6"> <small id="voucher_id"> None </small> </div>
                                                    </h5>
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                    <h5>
                                                        <div class="span5">Payment Date : </div>
                                                        <div class="span6"> <small id="vdate"> None </small> </div>
                                                    </h5>
                                                    </div>
                                                </div>
                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                    <h5>
                                                        <div class="span5">Store : </div>
                                                        <div class="span6"> <small id="branch_id"> None </small> </div>
                                                    </h5>
                                                    </div>
                                                </div>


                                                <!-- /selects, dropdowns -->
                                            </div>
                                            <!-- /general form elements -->     

                                            <div class="clearfix"></div>
                                            <!-- Default datatable -->
                                            <!-- /default datatable -->
                                        </fieldset>                     
                                    </form>
                                    <!--tab 1 content start from here-->  

                                </div>

                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   

                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <form class="form-horizontal" id="myForms" method="post" name="invoice" action="">    
                                                <fieldset>
                                                    <table class="table table-striped table-bordered"  id="productmore">
                                                        <thead>
                                                            <tr>
                                                                <th>Choose Expense Ledger</th>
                                                                <th>Choose Expense Paid Ledger</th>
                                                                <th>Expense Specific Memo</th>
                                                                <th>Expense Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="POITable">
                                                            
                                                        </tbody>
                                                        <tbody>       
                                                            <tr>
                                                                
                                                                <td  colspan="4" style="text-align:right;">
                                                                    <input type="text" class="required equalTo small" style="font-weight:bolder; width:250px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalamount" id="totalamount">
                                                                </td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </fieldset>                     

                                            </form>

                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     

                                    </fieldset>                     

                                    </form>
                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>

                                <!-- General form elements -->
                                <!--                                <div class="row-fluid block">
                                                                     General form elements 
                                                                    <div class="row-fluid  span12 well">   
                                
                                                                         Selects, dropdowns 
                                                                        <div class="table-overflow">
                                                                            <table class="table table-striped" id="data-table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>ID</th>
                                                                                        <th>Ex.V. ID</th>
                                                                                        <th>Expense Ledger</th>
                                                                                        <th>Paid Ledger</th>
                                                                                        <th>Total Amount</th>
                                                                                        <th>Created</th>
                                                                                        <th>Date</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                <?php
                                /* $sql = $obj->FlyQuery("SELECT `id`,(select concat(fname,' ',lname) FROM account_module_vendor WHERE account_module_vendor.id=account_module_bill_payment.vendor_id) as customer_name,`vendor_id`,`date`,`pa`,`amount`,(SELECT store.name FROM store WHERE store.id=account_module_bill_payment.`input_by`) as receiver FROM `account_module_bill_payment` WHERE date='" . date('Y-m-d') . "'");
                                  $i = 1;
                                  if (!empty($sql))
                                  foreach ($sql as $row):
                                  ?>
                                  <tr>
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $row->id; ?></td>
                                  <td><?php echo $row->customer_name; ?></td>
                                  <td><?php echo $row->pa; ?></td>
                                  <td><?php echo $row->amount; ?></td>
                                  <td><?php echo $row->receiver; ?></td>
                                  <td><?php echo $row->date; ?></td>

                                  </tr>
                                  <?php
                                  $i++;
                                  endforeach; */
                                ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                         /selects, dropdowns 
                                
                                
                                
                                                                         Selects, dropdowns 
                                
                                                                         /selects, dropdowns 
                                
                                
                                
                                                                    </div>
                                                                     /general form elements      
                                
                                
                                                                    <div class="clearfix"></div>
                                
                                                                     Default datatable 
                                
                                                                     /default datatable 
                                
                                
                                                                    tab 1 content start from here  
                                
                                                                </div>-->













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>  

        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
