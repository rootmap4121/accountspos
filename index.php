<?php
include('class/auth.php');
if(!isset($_SESSION['SESS_AMSIT_COG_STATUS']))
{
include('class/login.php');
$log=new login();
echo $log->accounts_session_add($input_bys, "Accounts Session Restored.");
exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <style type="text/css">
            .dash_panel {
                height: 155px;
                width: 174px;
                margin-right: 6px;
                margin-bottom: 5px;
                text-align: center;
                box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.74) inset;
                border-radius: 8px;
                display:inline-block;
            }

            .dash_img {
                margin-top:10px;
                height: 100px;
                width: 100px;
            }

            .dash_label {
                margin: 0px;
                color: #1D2024;
                font-family:Arial, sans-serif;
                font-size: 16px;
                font-weight: bolder;
                text-decoration: none;
                text-transform: capitalize;
                height: 30px;
                line-height: 30px;
                text-align: center;
                width: auto;
                outline: medium none #222;
            }

            .bg-blue{
                /*background-image: -moz-linear-gradient(center top , #4cb1e0 0%, #1675a1 100%) !important;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3) !important;*/
				background: url("../images/elements/ui/progress_overlay.png") repeat scroll 0 0%, rgba(0, 0, 0, 0) linear-gradient(to bottom, #68b4dd 0%, #2f86b2 100%) repeat scroll 0 0;
				box-shadow: 0 1px 1px rgba(255, 255, 255, 0.3) inset;
				color: #fff;
				text-shadow: 0 1px 1px #CCC;
				vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">
                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->
                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i>Accounts System Dashboard</h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->
                        <div class="body">
                            <!-- Middle navigation standard -->

                            <?php //include('include/quicklink.php'); ?>

                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">
<!--                                <div class="panel-header">
                                    <h3 style="border-bottom:1px #ccc dotted; border-top:1px #ccc dotted;"><i class="font-link"></i> Quick Links</h3>
                                </div>-->
                                <div class="panel-body no-padding" style="text-align:center;">
                                    <a href="setting_coa.php" class="dash_panel">
                                        <img src="images/icons/new_icons/home_inventory_icon_256.png" class="dash_img">
                                        <div class="dash_label">Chart of Account</div>
                                    </a>

                                    <a href="setting_ledger_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/conference.png" class="dash_img">
                                        <div class="dash_label"> List of Ledger </div>
                                    </a>

                                    <a href="sales_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/orderlist.png" class="dash_img">
                                        <div class="dash_label"> Sales Invoice </div>
                                    </a>

                                    <a href="purchase_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/Money_Calculator.png" class="dash_img">
                                        <div class="dash_label">Purchase Voucher</div>
                                    </a>

                                    <a href="expense_voucher_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/reports_out.png" class="dash_img">
                                        <div class="dash_label">Expense Voucher</div>
                                    </a>

                                    <a href="receive_payment_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/security_creditcard.png" class="dash_img">
                                        <div class="dash_label">Receive Payment</div>
                                    </a>

                                    <a href="#" class="dash_panel">
                                        <img src="images/icons/new_icons/leave.png" class="dash_img">
                                        <div class="dash_label">Salary Voucher</div>
                                    </a>

                                    <a href="expense_voucher_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/f_report_out.png" class="dash_img">
                                        <div class="dash_label">Office Expense</div>
                                    </a>

                                    <a href="customer_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/Add-Male-User.png" class="dash_img">
                                        <div class="dash_label">Customer List</div>
                                    </a>

                                    <a href="journal_list.php" class="dash_panel">
                                        <img src="images/icons/new_icons/pentest-report-icon.png" class="dash_img">
                                        <div class="dash_label">Journal Transaction</div>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="span12" style="margin-top:30px;">
                                    <h3 style="border-bottom:1px #ccc dotted; border-top:1px #ccc dotted;"><i class=" font-paper-clip"></i> Dashboard Reports</h3>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <!-- Table hover -->
                                        <div class="well">
                                            <div class="navbar">
                                                <div class="navbar-inner bg-blue" >
                                                    <h5 style="color: #ffffff !important;	text-shadow:none !important;"><i class="  font-book"></i> Bank & Asset Detail</h5>
                                                </div>
                                            </div>
                                            <div class="table-overflow">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center; color:#000; font-size:14px; font-weight:bolder;"><strong>Accounts</strong></th>
                                                            <th style="text-align:center; color:#000; font-size:14px; font-weight:bolder;"><strong>Balance</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $balance = 0;
                                                        $bi=1;
                                                        $sqlbankandasset="SELECT a.id,a.head_sub_list_name as name,b.debit,c.cradit,(b.debit - c.cradit) as total FROM account_module_ladger_list_properties as a 
                                                        left join 
                                                        (SELECT ladger_id, sum(debit) AS debit
                                                          FROM account_module_ladger
                                                          GROUP BY ladger_id)
                                                        as b ON b.ladger_id=a.id 
                                                        left join 
                                                        (SELECT ladger_id, sum(cradit) AS cradit
                                                          FROM account_module_ladger
                                                          GROUP BY ladger_id)
                                                        as c ON c.ladger_id=a.id
                                                        WHERE main_head_id='1'";
                                                        $ld = $obj->FlyQuery($sqlbankandasset);
                                                        foreach ($ld as $row):
                                                            $inid = $row->id;
                                                            $debit = $row->debit;
                                                            $cradit = $row->cradit;
                                                            $total = $row->total;
                                                            if ($total != 0) {
                                                                ?>
                                                                <tr>
                                                                    <td style="color:#000; font-size:14px; font-weight:bolder;">
                                                                        <strong>
                                                                            <a style="color:#000; " href="viewledger.php?ladger_id=<?php echo $inid; ?>">
                                                                                <?php echo $row->name; ?>
                                                                            </a>
                                                                        </strong>
                                                                    </td>
                                                                    <td style="text-align:right; color:#000; font-size:14px; font-weight:bolder;">
                                                                        <strong>
                                                                            <?php
                                                                            $netm = $total;
                                                                            $getch = substr($netm, 0, 1);
                                                                            if ($getch != '-') {
                                                                                echo number_format($netm);
                                                                            } else {
                                                                                echo "(" . number_format(substr($netm, 1, 200)) . ")";
                                                                            }
                                                                            $balance+=($debit-$cradit);
                                                                            ?>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                if($bi==5)
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                            $bi++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                    <thead><tr>
                                                            <th><a href="bank_asset_ledger.php">View All Bank & Asset Detail</a></th>
                                                            <th style="text-align:right; color:#000;">
                                                                <span style="border-bottom:1px #666666 dotted;">
                                                                    <strong>
                                                                        Total Balance = <?php echo $balance; ?>
                                                                    </strong>
                                                                </span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /table hover -->
                                    </div>
                                    <div class="span6">
                                        <!-- Table hover -->
                                        <div class="well">
                                            <div class="navbar">
                                                <div class="navbar-inner bg-blue" >
                                                    <h5 style="color: #ffffff !important; text-shadow:none !important;"><i class=" font-user"></i> Customer Accounts Status </h5>
                                                </div>
                                            </div>
                                            <div class="table-overflow">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center;">Name</th>
                                                            <th style="text-align:center;">Total Bill</th>
                                                            <th style="text-align:center;">Paid Amount</th>
                                                            <th style="text-align:center;">Due</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                        <?php
                                                        $ci=1;
                                                        $sqlcustomer="SELECT
                                                        i.id,
                                                        i.fname as name,
                                                        IFNULL(p.amount, 0) as paid_amount,
                                                        IFNULL(p.totalamount, 0) as total_amount,
                                                        IFNULL((p.totalamount-p.amount),0) as due_amount
                                                        FROM account_module_customer AS i

                                                        LEFT JOIN (SELECT
                                                                   customer_id,
                                                                   invoice_id,
                                                                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                                                                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                                                                   FROM account_module_invoice
                                                                   GROUP BY customer_id) AS p
                                                        ON p.customer_id = i.id ORDER BY i.id DESC";
                                                        $sqlcustomerquery=$obj->FlyQuery($sqlcustomer);
                                                        if(!empty($sqlcustomerquery))
                                                            foreach($sqlcustomerquery as $row):
                                                            if($row->total_amount!=0)
                                                            {
                                                        ?>
                                                        <tr>
                                                            <td style="text-align:center;"><a style="color:#000; " href="customer_list.php"><?php echo $row->name; ?></a></td>
                                                            <td style="text-align:center;"><?php echo $row->total_amount; ?></td>
                                                            <td style="text-align:center;"><?php echo $row->paid_amount; ?></td>
                                                            <td style="text-align:center;"><?php echo $row->due_amount; ?></td>
                                                        </tr>
                                                        <?php 
                                                                if($ci==5)
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                            
                                                            $ci++;
                                                        endforeach; ?>
                                                    </tbody>
                                                    <thead><tr>
                                                            <th style="text-align:center;" colspan="4"><strong><a href="customer_list.php">View All Customer Report</a></strong></th>
                                                         </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /table hover -->
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <!-- Table hover -->
                                        <div class="block well">
                                            <div class="navbar">
                                                <div class="navbar-inner bg-blue" >
                                                    <h5 style="color: #ffffff !important;	text-shadow:none !important;"><i class="font-th-large"></i> Report Summary ( Income Statement )</h5>
                                                </div>
                                            </div>
                                            <script>
                                                nucleus(document).ready(function(){
                                                    nucleus.post('./lib/income_statement.php',{'st':1},function(income){
                                                        var datacl=nucleus.parseJSON(income);
                                                        var status=datacl.status;
                                                        if(status==1)
                                                        {
                                                            nucleus('#total_income').html(datacl.totalincome);
                                                            nucleus('#gross_profit').html(datacl.grossprofit);
                                                            nucleus('#direct_expense').html(datacl.directexpense);
                                                            nucleus('#net_profit').html(datacl.net_profit);
                                                            nucleus('#lastreportdate').html(datacl.date);
                                                        }
                                                        else
                                                        {
                                                            nucleus('#total_income').html(datacl.totalincome);
                                                            nucleus('#gross_profit').html(datacl.grossprofit);
                                                            nucleus('#direct_expense').html(datacl.directexpense);
                                                            nucleus('#net_profit').html(datacl.net_profit);
                                                            nucleus('#lastreportdate').html(datacl.date);
                                                        }
                                                    });
                                                });
                                            </script>
                                            <div class="table-overflow">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-money" style="margin-right:15px;"></i>Total Income</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="total_income"><strong>0.00 (Not Calculated Yet)<span style="margin-left:5px;"><i class=" font-arrow-up" style="margin-right:5px; color:#0C0;"></i><i class="font-arrow-down" style="margin-right:5px; color:#C00;"></i></span></strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-bar-chart" style="margin-right:15px;"></i> Gross Profit</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="gross_profit"><strong>0.00 (Not Calculated Yet)<span style="margin-left:5px;"><i class=" font-arrow-up" style="margin-right:5px; color:#0C0;"></i><i class="font-arrow-down" style="margin-right:5px; color:#C00;"></i></span></strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-hand-right" style="margin-right:15px;"></i>Direct Expense</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="direct_expense"><strong>0.00 (Not Calculated Yet)<span style="margin-left:5px;"><i class=" font-arrow-up" style="margin-right:5px; color:#0C0;"></i><i class="font-arrow-down" style="margin-right:5px; color:#C00;"></i></span></strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-adjust" style="margin-right:15px;"></i> Net Profit</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="net_profit"><strong>0.00 (Not Calculated Yet)<span style="margin-left:5px;"><i class=" font-arrow-up" style="margin-right:5px; color:#0C0;"></i><i class="font-arrow-down" style="margin-right:5px; color:#C00;"></i></span></strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-calendar" style="margin-right:15px;"></i> Last Report Date</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="lastreportdate"><strong>0.00 (Not Calculated Yet)<span style="margin-left:5px;"><i class=" font-arrow-up" style="margin-right:5px; color:#0C0;"></i><i class="font-arrow-down" style="margin-right:5px; color:#C00;"></i></span></strong></th>
                                                        </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>
                                        <!-- /table hover -->
                                    </div>
                                    <div class="span6">
                                        <!-- Table hover -->
                                        <div class="block well">
                                            <div class="navbar">
                                                <div class="navbar-inner bg-blue" >
                                                    <h5 style="color: #ffffff !important; text-shadow:none !important;"><i class="font-list-alt"></i> Balance Sheet </h5>
                                                </div>
                                            </div>
                                            <script>
                                                nucleus(document).ready(function(){
                                                    nucleus.post('./lib/income_statement.php',{'st':2},function(balancesheet){
                                                        var dataclb=nucleus.parseJSON(balancesheet);
                                                        var statusb=dataclb.status;
                                                        if(statusb==1)
                                                        {
                                                            nucleus('#total_asset').html(dataclb.totalasset);
                                                            nucleus('#total_liability').html(dataclb.totalliability);
                                                            nucleus('#total_equity').html(dataclb.totalequity);
                                                        }
                                                        else
                                                        {
                                                            nucleus('#total_asset').html(dataclb.totalasset);
                                                            nucleus('#total_liability').html(dataclb.totalliability);
                                                            nucleus('#total_equity').html(dataclb.totalequity);
                                                        }
                                                    });
                                                    
                                                    nucleus.post('./lib/income_statement.php',{'st':3},function(receiveables){
                                                        var dataclbs=nucleus.parseJSON(receiveables);
                                                        var statusbs=dataclbs.status;
                                                        if(statusbs==1)
                                                        {
                                                            nucleus('#receiveables').html(dataclbs.receiveables);
                                                        }
                                                        else
                                                        {
                                                            nucleus('#receiveables').html(dataclbs.receiveables);
                                                        }
                                                    });
                                                    
                                                    nucleus.post('./lib/income_statement.php',{'st':4},function(payable){
                                                        var dataclbsp=nucleus.parseJSON(payable);
                                                        var statusbsp=dataclbsp.status;
                                                        if(statusbsp==1)
                                                        {
                                                            nucleus('#payable').html(dataclbsp.payable);
                                                        }
                                                        else
                                                        {
                                                            nucleus('#payable').html(dataclbsp.payable);
                                                        }
                                                    });
                                                    
                                                });
                                            </script>
                                            <div class="table-overflow">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong> <i class="font-briefcase" style="margin-right:15px;"></i>Total Asset</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="total_asset"><strong>0.00 </strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-filter" style="margin-right:15px;"></i>Total Liabilities</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="total_liability"><strong>0.00 </strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-beaker" style="margin-right:15px;"></i>Total Equity</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="total_equity"><strong>0.00 </strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong> <i class="font-plus-sign" style="margin-right:15px;"></i>Receivables</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="receiveables"><strong>10,900.00</strong></th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align:left; color:#000; font-size:14px; font-weight:bolder;"><strong><i class="font-minus-sign" style="margin-right:15px;"></i>Payable</strong></th>
                                                            <th style="text-align:left; font-size:12px; font-weight:bolder;" id="payable"><strong>310,000.00</strong></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /table hover -->
                                    </div>
                                </div>
								<div class="clearfix" style="height:15px;"></div>
								<div class="separator-doubled"></div>
                            </div>
                            <!-- /content container -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php');     ?>
            <!-- /right sidebar -->
        </div>
        <!-- /main wrapper -->
    </body>
</html>

