<?php
include('class/auth.php');
include('./querysheld/session_setting.php');
$obj_session_setting=new SessionSetting();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_session_setting->admin($from, $to);
    }else {
        $sql=$obj_session_setting->admin($setting_start, $setting_end);
    }
}elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_session_setting->shop_admin($from, $to, $input_by);
    }else {
        $sql=$obj_session_setting->shop_admin($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_session_setting->cashier($from, $to, $input_by);
    }else {
        $sql=$obj_session_setting->cashier($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql=$obj_session_setting->manager($from, $to, $input_by);
    }else {
        $sql=$obj_session_setting->manager($setting_start, $setting_end, $input_by);
    }
}elseif ($input_status == 5) {

    $array_ch=array();
    $sqlchain_store_ids=$obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql=$obj_session_setting->store_chain_admin($from, $to, $array_ch);
        }else {
            $sql=$obj_session_setting->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    }else {
        $sql=array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
            function SaveCustomer()
            {
                var start_date = $('input[name=start_date]').val();
                var end_date = $('input[name=end_date]').val();
                var title = $('input[name=title]').val();
                var start_year = $('select[name=start_year]').val();
                var end_year = $('select[name=end_year]').val();
                var session_status = $('select[name=session_status]').val();
                //alert(start_date+" "+start_date+" "+title+" "+start_year+" "+end_year);
                if (start_date != "" && title != "" && start_year != "" && session_status != "")
                {

                    $.post("lib/setting_session.php", {'st': 1,
                        'start_date': start_date,
                        'end_date': end_date,
                        'title': title,
                        'start_year': start_year,
                        'end_year': end_year,
                        'session_status': session_status
                    }, function (data)
                    {
                        if (data == 1)
                        {
                            //clear();
                            autoload();
                            $.jGrowl('Saved, Account Session Started Successfully.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else if (data == 2)
                        {
                            $.jGrowl('Saved, Accounts Session Finish Record Saved Successfully.', {sticky: false, theme: 'growl-warning', header: 'Error!'});
                        } else
                        {
                            $.jGrowl('Failed, Try Again.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                        }
                    });
                } else
                {
                    $.jGrowl('Failed, Some Field is Empty.', {sticky: false, theme: 'growl-error', header: 'Error!'});
                }
            }

            function clear()
            {
//                $('.datepicker').val("");
//                $('.span12 .memo').val("");
//                $('#shop_id').val("");


//                $('#opt_a_d').val("");
//                $('#opt_a_debit').val("0.00");
//                $('#opt_a_cradit').val("0.00");


//                $('#opt_b_d').val("");
//                $('#opt_b_debit').val("0.00");
//                $('#opt_b_cradit').val("0.00");


//                $('#totaldebit').val("0.00");
//                $('#totalcradit').val("0.00");
            }

            function autoload()
            {
                $.post('./lib/setting_session.php', {'st': 2}, function (data) {
                    var datacl = jQuery.parseJSON(data);
                    var status = datacl.status;
                    var sitedata = datacl.sitedata;
                    if (status == 1)
                    {
                        $('#sitedata').html(sitedata);
                        //console.log(1);
                    }
                });
            }

            nucleus(document).ready(function () {
                autoload();
            });

        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Setting Accounting Session Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                <div class="row-fluid block">

                                    <blockquote style="margin-top:-20px;">
                                        <small><cite title="Source Title"  class="text-error">Please Fill up All Mandatory Field (*)</cite></small>
                                    </blockquote>


                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                        <fieldset>
                                            <!-- General form elements -->
                                            <div class="row-fluid  span12 well">

                                                <!-- Selects, dropdowns -->
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Session Start Date *</label>
                                                        <input type="text" name="start_date" class="datepicker" placeholder="Session Start Date" />
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Session End Date *</label>
                                                        <input type="text" name="end_date" class="datepicker" placeholder="Session End Date" />
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Session Title *</label>
                                                        <input type="text" name="title" class="span12" placeholder="Session Title." />
                                                    </div>
                                                </div>
                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->

                                                <!-- /selects, dropdowns -->



                                                <div class="clearfix"></div>

                                                <!-- Selects, dropdowns -->
                                                <div class="span4" style="margin-left: 0px;">
                                                    <div class="control-group">
                                                        <label class="span12">Session Start Year *</label>
                                                        <select name="start_year"  data-placeholder="Select Session Start Year" class="select-search" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            for ($i=date('Y') + 10; $i >= date('Y') - 10; $i--):
                                                                ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php
                                                            endfor;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Session End Year *</label>
                                                        <select name="end_year"  data-placeholder="Select Session End Year" class="select-search" tabindex="2">
                                                            <option value=""></option>
                                                            <?php
                                                            for ($i=date('Y') + 10; $i >= date('Y') - 10; $i--):
                                                                ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php
                                                            endfor;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="span12">Session Status *</label>
                                                        <select name="session_status"  data-placeholder="Select Session Status" class="select-search" tabindex="2">
                                                            <option value=""></option>

                                                            <option value="1">Start</option>
                                                            <option value="2">End</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="span4">
                                                    <label class="span12" style="height: 35px;">&nbsp;</label>
                                                    <div class="control-group">
                                                        <button onClick="SaveCustomer()" type="button" name="create" class="btn btn-success create">
                                                            <i class="icon-plus-sign"></i> Save Record </button>
                                                    </div>
                                                </div>



                                                <!-- /selects, dropdowns -->

                                                <!-- Selects, dropdowns -->

                                                <!-- /selects, dropdowns -->



                                            </div>
                                            <!-- /general form elements -->


                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->


                                            <!--tab 1 content start from here-->

                                            </div>
                                            <!-- General form elements -->












                                            <div class="row-fluid block">
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12 well">

                                                    <!-- Selects, dropdowns -->
                                                    <div class="table-overflow">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Start Date</th>
                                                                    <th>End Date</th>
                                                                    <th>Start Year</th>
                                                                    <th>End Year</th>
                                                                    <th>Title</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="sitedata">
                                                                <?php
//                                                                $sqlquery = $obj->FlyQuery("SELECT
//                                                                a.id,
//                                                                a.store_id,
//                                                                a.store_id as store,
//                                                                a.input_by,
//                                                                a.finput_by,
//                                                                a.title,
//                                                                a.session_start_year,
//                                                                a.session_end_year,
//                                                                a.session_start_date,
//                                                                a.session_end_date,
//                                                                a.date,
//                                                                a.fdate,
//                                                                a.status
//                                                                FROM
//                                                                account_module_session_setting as a");
                                                                $dd='';
                                                                $d=1;
                                                                if (!empty($sql))
                                                                    foreach ($sql as $ww):
                                                                        $dd .='<tr>
                                                                        <td>' . $d . '</td>
                                                                        <td>' . $ww->session_start_date . '</td>
                                                                        <td>' . $ww->session_end_date . '</td>
                                                                        <td>' . $ww->session_start_year . '</td>
                                                                        <td>' . $ww->session_end_year . '</td>
                                                                        <td>' . $ww->title . '</td>
                                                                        <td>';

                                                                        if ($ww->status == "1") {
                                                                            $dd .='Start';
                                                                        }else {
                                                                            $dd .='End';
                                                                        }

                                                                        $dd .='</td>
                                                                        </tr>';
                                                                        $d++;
                                                                    endforeach;
                                                                echo $dd;
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /selects, dropdowns -->



                                                    <!-- Selects, dropdowns -->

                                                    <!-- /selects, dropdowns -->



                                                </div>
                                                <!-- /general form elements -->


                                                <div class="clearfix"></div>

                                                <!-- Default datatable -->

                                                <!-- /default datatable -->


                                                <!--tab 1 content start from here-->

                                            </div>





                                            <!-- General form elements -->








                                            </div>



                                            <!-- General form elements -->

                                            <!-- /general form elements -->






                                            <div class="clearfix"></div>

                                            <!-- Default datatable -->

                                            <!-- /default datatable -->






                                            <!-- Content End from here customized -->




                                            <div class="separator-doubled"></div>



                                            </div>
                                            <!-- /content container -->

                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            <!-- /main content -->
                                            <?php include('include/footer.php'); ?>
                                            <!-- Right sidebar -->
                                            <?php //include('include/sidebar_right.php');    ?>
                                            <!-- /right sidebar -->

                                            </div>
                                            <!-- /main wrapper -->

                                            </body>
                                            </html>
