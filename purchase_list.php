<?php
include('class/auth.php');
include('./querysheld/purchase.php');
$obj_purchase = new Purchase();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_purchase->admin($from, $to);
    } else {
        $sql = $obj_purchase->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_purchase->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_purchase->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_purchase->cashier($from, $to, $input_by);
    } else {
        $sql = $obj_purchase->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_purchase->manager($from, $to, $input_by);
    } else {
        $sql = $obj_purchase->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_purchase->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_purchase->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>
//        nucleus(document).ready(function()
//		{
//			nucleus("#loadsales").html("Loading Please Wait...");
//			nucleus.post("./lib/purchase.php",{'st':4},function(data)
//			{
//				var datacl=jQuery.parseJSON(data);
//				var status=datacl.status;
//				if(status==1)
//				{
//					var stringdata=datacl.salesdata;
//					nucleus("#loadsales").html(stringdata);
//					loadtable();
//					nucleus("select[name=DataTables_Table_0_length]").attr("data-placeholder","Entries");
//					nucleus("select[name=DataTables_Table_0_length]").attr("class","select");
//					nucleus("select[name=DataTables_Table_0_length]").attr("tabindex","2");
//					nucleus("select[name=DataTables_Table_0_length]").css("opacity","0");
//					nucleus("select[name=DataTables_Table_0_length]").attr("onChange","changeEntires(this.value)");
//					
//				}
//				else
//				{
//					var stringdata="<tr><td colspan='9'>No Data Found</td></tr>";
//					nucleus("#loadsales").html(stringdata);
//				}
//			});
//			
//		});

            function loadtable()
            {
                $("table").DataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
                    "oLanguage": {
                        "sLengthMenu": "<span>Show entries:</span><div class='selector' id='uniform-undefined'><span class='entries' style='-moz-user-select: none;'>10</span> _MENU_ </div>"
                    }
                });
            }

            function changeEntires(vall)
            {
                nucleus(".entries").html(vall);
            }

        </script>
        <script>
            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Purchase Voucher Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("lib/purchase.php", {'st': 3, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            //loadtable();
                            $.jGrowl('Invoice Record, Successfully Deleted.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        }
                        else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }
        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Purchase List Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Purchase List</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Voucher Number</th>
                                                        <th>Voucher Date</th>

                                                        <th>Vendor</th>

                                                        <th>Total</th>
                                                        <th>Paid</th>
                                                        <th>Due</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="loadsales">
                                                    <?php
                                                    $total_amount=0;
                                                    $total_paid_amount=0;
                                                    $total_due_amount=0;
                                                    $salesdata = '';
//                                                    $sql = $obj->FlyQuery("select 
//                                                    a.id,
//                                                    a.bill_id,
//                                                    a.vendor_id,
//                                                    a.branch_id,
//                                                    a.link_id, 
//                                                    a.currency,
//                                                    a.date,
//                                                    concat(d.fname,' ',d.lname) as `company_name`,
//                                                    IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
//                                                    IFNULL(SUM(ambd.`price`),0) as `price`,
//                                                    IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
//                                                    IFNULL(SUM(ambp.`amount`),0) as `amount`
//                                                    from account_module_bill as a
//                                                    left JOIN account_module_customer as d ON d.id=a.vendor_id 
//                                                    left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
//                                                    LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
//                                                    GROUP BY ambd.bill_id,ambp.bill_id");
                                                    $i = 1;
                                                    if (!empty($sql))
                                                        foreach ($sql as $row):
                                                            $inid = $row->id;
                                                            $link_id = $row->link_id;
                                                            $invoice_id = $row->bill_id;
                                                            $branch_id = $row->branch_id;
                                                            $cid = $row->vendor_id;
                                                            $totaldue = $row->subtotal;
                                                            $total = $row->subtotal;
                                                            $p = $row->amount;
                                                            $tax_invoice = 0;


                                                            $salesdata .='<tr id="tr' . $row->id . '">
                                                            <td>' . $i . '</td>
                                                            <td><a href="view_purchase.php?id=' . $row->id . '&amp;cid=' . $cid . '&amp;branch_id=' . $branch_id . '&amp;link_id=' . $link_id . '&amp;currency=' . $row->currency . '">Voucher ' . $row->id . '</a></td>
                                                            <td>' . $obj->dates($row->date) . '</td>
                                                            <td>' . $row->company_name . '</td>
                                                            <td>';
                                                            $amt = $total + $tax_invoice;
                                                            $salesdata .=number_format($amt, 2);
                                                            $salesdata .=' ' . $obj->SelectAllByVal("account_module_currency", "id", $currency, "detail") . '</td>';
                                                            $salesdata .='<td>' . number_format($p, 2);
                                                            $salesdata .=' ' . $obj->SelectAllByVal("account_module_currency", "id", $currency, "detail") . '</td>
                                                            <td>';
                                                            $amd = ($totaldue + $tax_invoice) - $p;
                                                            $salesdata .=number_format($amd, 2);
                                                            $salesdata .=' ' . $obj->SelectAllByVal("account_module_currency", "id", $currency, "detail") . '</td>
                                                            <td>';

                                                            $due_total+=$amd;
                                                            $payment_total+=$amt;

                                                            $status = $row->status;
                                                            if ($status == 0) {
                                                                $salesdata .='<button type="button" class="btn btn-small" disabled="disabled"><font color="#FF0000">Unpaid</font></button>';
                                                            } elseif ($status == 1) {

                                                                $salesdata .='<button type="button" class="btn btn-warning btn-small" disabled="disabled">Partial</button>';
                                                            } elseif ($status == 2) {

                                                                $salesdata .='<button type="button" class="btn btn-success btn-small" disabled="disabled">Paid</button>';
                                                            }


                                                            $salesdata .='</td>
                                                            <td>
                                                            <a href="#" onClick="deleteR(' . $row->id . ')"><i class="icon-trash" ></i></a>';


                                                            $totalam = $totaldue + $tax_invoice;
                                                            if ($totalam == $p) {
                                                                $obj->update("account_module_bill", array("id" => $inid, "status" => 2));
                                                            } elseif ($p != 0) {
                                                                $obj->update("account_module_bill", array("id" => $inid, "status" => 1));
                                                            } elseif ($p == 0) {
                                                                $obj->update("account_module_bill", array("id" => $inid, "status" => 0));
                                                            }

                                                            $salesdata .='</td></tr>';
                                                            
                                                            $total_amount+=$amt;
                                                            $total_paid_amount+=$p;
                                                            $total_due_amount+=$amd;
                                                            
                                                            $i++;
                                                        endforeach;
                                                        echo $salesdata;
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="8" style="font-weight: bolder; text-align: right;">Total Amount = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_amount); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="font-weight: bolder; text-align: right;">Paid Amount = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_paid_amount); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="font-weight: bolder; text-align: right;">Due Amount = </td>
                                                        <td style="font-weight: bolder;"><?php echo $obj->amountconvert($total_due_amount); ?></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->

                                        <!-- /selects, dropdowns -->



                                    </div>
                                    <!-- /general form elements -->     


                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                    <!--tab 1 content start from here-->  

                                </div>













                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->






                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
<?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
