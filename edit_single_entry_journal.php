<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
        <script>
			(function($) {
				var journal=<?php echo $_GET['journal']; ?>;
                $.post("./lib/single_entry_journal.php",{'st':2,'journal':journal},function(data){
					//alert('Success');
					var datacl=jQuery.parseJSON(data);
					$('.datepicker').val(datacl.date);	
					$('.span12 .memo').val(datacl.memo);
					$('select#shop_id').find('option[value="'+datacl.shop_id+'"]').attr('selected','selected'); 
					$('#single_entry').html(datacl.datas);
					$('#totaldebit').val(datacl.dval);
					$('#totalcradit').val(datacl.cval);
					$('#status').html(datacl.status);
				});
			})(jQuery);
			
			
				
		</script>
		
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> <b id="status">Single</b> Entry Journal Detail Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                          
                                            
                                          
                                          <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span12">Payment Date *</label>
                                                    <input type="text" name="date" class="datepicker" placeholder="Payment Date" />
                                                </div>                                               
                                            </div>
                                            
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span12">Journal tracking ID/Name/No *</label>
                                                    <input type="text" name="date" class="span12 memo" placeholder="Journal track ID/Name/No." />
                                                </div>                                               
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="span4">
                                                
												
                                                <div class="control-group">
                                                    <label class="span12">Choosen Shop</label>
                                                    <select disabled name="shop_id"  id="shop_id">
                                                    <option value="">Select Shop</option> 
                                                            <?php
														   $sql2=$obj->SelectAll("branch");
														   if(!empty($sql2))
														   foreach($sql2 as $rows)
														   {
														   ?>      
															<option value="<?php echo $rows->branch_id; ?>"><?php echo $rows->name; ?></option>
															<?php
														   }
															?>
                                                            
                                                       </select>
                                                </div>
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->












										<div class="row-fluid block">
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="table-overflow">
                                                <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Account</th>
                                                                <th>Description/Memo</th>
                                                                <th style="text-align:right;">Debit</th>
                                                                <th style="text-align:right;">Credit</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="single_entry">
                                                        
                                                        </tbody>
                                                        <tbody>
                                                        	
                                                            <tr>
                                                            	<td>
                                                                
                                                                </td>
                                                            	<td align="right" valign="middle" style="font-weight:bolder;"><strong>Total</strong></td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totaldebit" id="totaldebit">
                                                                </td>
                                                                <td style="text-align:right;">
                                                                <input type="text" class="required equalTo small" style="font-weight:bolder; width:100px; background:none; border:0px; text-align:right;" readonly value="0.00" name="totalcradit" id="totalcradit">
                                                                </td>                                                            </tr>        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>





                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>
                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        <!-- General form elements -->




                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                                  



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>  
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		
    </body>
</html>
