<?php
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script>

            nucleus(document).ready(function ()
            {
                nucleus('#select-customer-id').find('.select2-input').keyup(function () {
                    var getvalue = nucleus(this).val();
                    var getlength = getvalue.length;
                    var place = nucleus('#select-customer-id').find('select').attr('id');
                    nucleus('#' + place).html("");
                    if (getlength >= 4)
                    {
                        $(".select2-no-results").html("wait your data is loading...");
                        $.post("lib/search_controller.php", {'st': 1, 'table': "account_module_customer", 'search': getvalue, 'field_a': "id", 'field_b': "concat(fname,' ',lname)"}, function (fetch) {
                            var datacl = jQuery.parseJSON(fetch);
                            var opt = datacl.data;
                            nucleus('#' + place).html(opt);
                        });
                        checksum(0);
                    }
                });




            });

            function changeselect(valid)
            {
                if (valid != '')
                {
                    window.location.replace('./customer_report.php?customer=' + valid);
                }
            }

        </script>


        <script>
            nucleus(document).ready(function ()
            {
                nucleus("#loadsales").html("Loading Please Wait...");
                nucleus.post("./lib/sales.php", {'st': 6, 'customer': '<?php echo $_GET['customer'] ?>'}, function (data)
                {
                    var datacl = jQuery.parseJSON(data);
                    var status = datacl.status;
                    if (status == 1)
                    {
                        var stringdata = datacl.salesdata;
                        var stringfotterdata = datacl.fotterdata;
                        nucleus("#loadsales").html(stringdata);
                        nucleus('#salesfotter').html(stringfotterdata);
                        loadtable();
                        nucleus("select[name=DataTables_Table_0_length]").attr("data-placeholder", "Entries");
                        nucleus("select[name=DataTables_Table_0_length]").attr("class", "select");
                        nucleus("select[name=DataTables_Table_0_length]").attr("tabindex", "2");
                        nucleus("select[name=DataTables_Table_0_length]").css("opacity", "0");
                        nucleus("select[name=DataTables_Table_0_length]").attr("onChange", "changeEntires(this.value)");

                    } else
                    {
                        var stringdata = "<tr><td colspan='9'>No Data Found</td></tr>";
                        nucleus("#loadsales").html(stringdata);
                    }
                });


                nucleus("#loadpurchase").html("Loading Please Wait...");
                nucleus.post("./lib/purchase.php", {'st': 6, 'customer': '<?php echo $_GET['customer']; ?>'}, function (data)
                {
                    var datacl = jQuery.parseJSON(data);
                    var status = datacl.status;
                    if (status == 1)
                    {
                        var stringdata = datacl.salesdata;
                        var stringfotterdata = datacl.fotterdata;
                        nucleus("#loadpurchase").html(stringdata);
                        nucleus('#purchasefotter').html(stringfotterdata);
                        loadtablep();
                        nucleus("select[name=DataTables_Table_0_length]").attr("data-placeholder", "Entries");
                        nucleus("select[name=DataTables_Table_0_length]").attr("class", "select");
                        nucleus("select[name=DataTables_Table_0_length]").attr("tabindex", "2");
                        nucleus("select[name=DataTables_Table_0_length]").css("opacity", "0");
                        nucleus("select[name=DataTables_Table_0_length]").attr("onChange", "changeEntires(this.value)");

                    } else
                    {
                        var stringdata = "<tr><td colspan='9'>No Data Found</td></tr>";
                        nucleus("#loadpurchase").html(stringdata);
                    }
                });


            });

            function loadtable()
            {
                $("#ff").DataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
                    "oLanguage": {
                        "sLengthMenu": "<span>Show entries:</span><div class='selector' id='uniform-undefined'><span class='entries' style='-moz-user-select: none;'>10</span> _MENU_ </div>"
                    }
                });
            }

            function loadtablep()
            {
                $("#pp").DataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
                    "oLanguage": {
                        "sLengthMenu": "<span>Show entries:</span><div class='selector' id='uniform-undefined'><span class='entries' style='-moz-user-select: none;'>10</span> _MENU_ </div>"
                    }
                });
            }

            function changeEntires(vall)
            {
                nucleus(".entries").html(vall);
            }

        </script>
        <script>
            function deleteR(id)
            {
                var c = confirm("are you sure to Delete this Sales Invoice Record ?.");
                if (c)
                {
                    $('#tr' + id).hide('slow');
                    $.post("lib/sales.php", {'st': 3, 'id': id}, function (data)
                    {
                        if (data == 1)
                        {
                            //loadtable();
                            $.jGrowl('Invoice Record, Successfully Deleted.', {sticky: false, theme: 'growl-success', header: 'success!'});
                        } else
                        {
                            $.jGrowl('Failed, Please Try Again.', {sticky: false, theme: 'growl-error', header: 'Failed!'});
                        }
                    });
                }
            }
        </script>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5>
                                <i class="font-user"></i> Customer Report Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->

                                <?php if (!isset($_GET['customer'])) { ?>
                                    <div class="row-fluid block">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                            <fieldset>
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12 well">     
                                                    <!-- Selects, dropdowns -->
                                                    <div class="span6" style="padding:0px; margin:0px;">
                                                        <div class="control-group" id="select-customer-id">
                                                            <label class="span12">Type Your Customer First 5 Character *</label>
                                                            <select onchange="changeselect(this.value)" name="customer_id" data-placeholder="Select Customer"  class="minimum-select"  id="customer_id" tabindex="2">
                                                                <option value=""></option> 

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- /selects, dropdowns -->
                                                    <!-- Selects, dropdowns -->
                                                    <!-- /selects, dropdowns -->
                                                </div>
                                                <!-- /general form elements -->     
                                                <div class="clearfix"></div>
                                                <!-- Default datatable -->
                                                <!-- /default datatable -->
                                            </fieldset>                     
                                        </form>
                                        <!--tab 1 content start from here-->  
                                    </div>
                                    <!-- General form elements -->
                                    <?php
                                } else {
                                    extract($_GET);
                                    $cq = $obj->FlyQuery("SELECT 
                                a.id,
                                concat(a.firstname,' ',a.lastname) as name, 
                                a.phone,
                                a.email,
                                c.name as country,
                                a.city,
                                a.postalcode,
                                a.address1
                                FROM coustomer as a
                                LEFT JOIN country as c ON c.id=a.country WHERE a.id='" . $customer . "'");
                                    ?>

                                    <div class="row-fluid block">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                            <fieldset>
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12">   
                                                    <!-- Table with gradient -->
                                                    <div class="block well">
                                                        <div class="navbar">
                                                            <div class="navbar-inner">
                                                                <h5>Customer Information : </h5>
                                                            </div>
                                                        </div>
                                                        <div class="table-overflow">
                                                            <table class="table table-bordered table-block table-gradient">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Full Name</th>
                                                                        <th>Email Address</th>
                                                                        <th>Phone Number</th>
                                                                        <th>Country</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><?php echo $cq[0]->name; ?></td>
                                                                        <td><?php echo $cq[0]->email; ?></td>
                                                                        <td><?php echo $cq[0]->phone; ?></td>
                                                                        <td><?php echo $cq[0]->country; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                                <thead>
                                                                    <tr>
                                                                        <th>City Name</th>
                                                                        <th>Zip Code</th>
                                                                        <th colspan="2">Address</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><?php echo $cq[0]->city; ?></td>
                                                                        <td><?php echo $cq[0]->postalcode; ?></td>
                                                                        <td colspan="2"><?php echo $cq[0]->address1; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /table with gradient -->
                                                    <!-- /selects, dropdowns -->
                                                </div>
                                                <!-- /general form elements -->     
                                                <!-- Default datatable -->
                                                <!-- /default datatable -->
                                            </fieldset>                     
                                        </form>
                                        <!--tab 1 content start from here-->  
                                    </div>



                                    <div class="row-fluid block">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                            <fieldset>
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12">   
                                                    <!-- Table with gradient -->
                                                    <div class="block well">
                                                        <div class="navbar">
                                                            <div class="navbar-inner">
                                                                <h5>Customer Invoice Information : </h5>
                                                            </div>
                                                        </div>
                                                        <div class="table-overflow">
                                                            <table id="ff" class="table table-bordered table-block table-gradient">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Invoice</th>
                                                                        <th>Date</th>
                                                                        <th>Total</th>
                                                                        <th>Paid</th>
                                                                        <th>Due</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody  id="loadsales">
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><?php echo $cq[0]->email; ?></td>
                                                                        <td><?php echo $cq[0]->phone; ?></td>
                                                                        <td><?php echo $cq[0]->country; ?></td>
                                                                        <td><?php echo $cq[0]->phone; ?></td>
                                                                        <td><?php echo $cq[0]->country; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot id="salesfotter">

                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /table with gradient -->
                                                    <!-- /selects, dropdowns -->
                                                </div>
                                                <!-- /general form elements -->     
                                                <!-- Default datatable -->
                                                <!-- /default datatable -->
                                            </fieldset>                     
                                        </form>
                                        <!--tab 1 content start from here-->  
                                    </div>


                                    <div class="row-fluid block">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                            <fieldset>
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12">   
                                                    <!-- Table with gradient -->
                                                    <div class="block well">
                                                        <div class="navbar">
                                                            <div class="navbar-inner">
                                                                <h5>Customer Purchase Information : </h5>
                                                            </div>
                                                        </div>
                                                        <div class="table-overflow">
                                                            <table id="pp" class="table table-bordered table-block table-gradient">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Invoice</th>
                                                                        <th>Date</th>
                                                                        <th>Total</th>
                                                                        <th>Paid</th>
                                                                        <th>Due</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody  id="loadpurchase">
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><?php echo $cq[0]->email; ?></td>
                                                                        <td><?php echo $cq[0]->phone; ?></td>
                                                                        <td><?php echo $cq[0]->country; ?></td>
                                                                        <td><?php echo $cq[0]->phone; ?></td>
                                                                        <td><?php echo $cq[0]->country; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot id="purchasefotter">

                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /table with gradient -->
                                                    <!-- /selects, dropdowns -->
                                                </div>
                                                <!-- /general form elements -->     
                                                <!-- Default datatable -->
                                                <!-- /default datatable -->
                                            </fieldset>                     
                                        </form>
                                        <!--tab 1 content start from here-->  
                                    </div>

                                    <div class="row-fluid block">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" name="invoice" action="">
                                            <fieldset>
                                                <!-- General form elements -->
                                                <div class="row-fluid  span12">   
                                                    <!-- Table with gradient -->
                                                    <div class="block well">
                                                        <div class="navbar">
                                                            <div class="navbar-inner">
                                                                <h5>Customer Payment &AMP; Receive  Information : </h5>
                                                            </div>
                                                        </div>
                                                        <div class="table-overflow">
                                                            <table class="table table-bordered table-block table-gradient">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Receive Particular Information</th>
                                                                        <th>Payment Particular Information</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="table-overflow">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Date</th>
                                                                                            <th>Ledger</th>
                                                                                            <th>Total Amount</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $total_receive = 0;
                                                                                        $sql = $obj->FlyQuery("SELECT 
                                                                                        a.`id`,
                                                                                        concat(c.fname,' ',c.lname) as customer_name,
                                                                                        a.`cid`,
                                                                                        a.`date`,
                                                                                        l.head_sub_list_name AS `pa`,
                                                                                        a.`amount`,
                                                                                        s.name as receiver 
                                                                                        FROM `account_module_invoice_payment` as a 
                                                                                        LEFT JOIN account_module_customer as c ON c.id=a.cid 
                                                                                        LEFT JOIN store AS s ON s.id=a.input_by 
                                                                                        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa 
                                                                                        WHERE a.cid='" . $customer . "'");
                                                                                        $i = 1;
                                                                                        if (!empty($sql))
                                                                                            foreach ($sql as $row):
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo $obj->dates($row->date); ?></td>
                                                                                                    <td><?php echo $row->pa; ?></td>
                                                                                                    <td><?php echo $obj->amountconvert($row->amount); ?></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $total_receive+=$row->amount;
                                                                                                $i++;
                                                                                            endforeach;
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="table-overflow">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Date</th>
                                                                                            <th>Ledger</th>
                                                                                            <th>Total Amount</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $total_paid = 0;
                                                                                        $sql = $obj->FlyQuery("SELECT 
                                                                                        a.id,
                                                                                        concat(b.fname,' ',b.lname) as customer_name,
                                                                                        a.vendor_id,
                                                                                        a.date,
                                                                                        a.pa,
                                                                                        p.head_sub_list_name as ledger,
                                                                                        a.amount,
                                                                                        s.name as receiver 
                                                                                        FROM account_module_bill_payment as a 
                                                                                        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
                                                                                        LEFT JOIN store as s ON s.id=a.input_by 
                                                                                        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa
                                                                                        WHERE a.vendor_id='" . $customer . "'");
                                                                                        $i = 1;
                                                                                        if (!empty($sql))
                                                                                            foreach ($sql as $row):
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo $obj->dates($row->date); ?></td>
                                                                                                    <td><?php echo $row->ledger; ?></td>
                                                                                                    <td><?php echo $obj->amountconvert($row->amount); ?></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $i++;
                                                                                                $total_paid+=$row->amount;
                                                                                            endforeach;
                                                                                        ?>

                                                                                    </tbody>

                                                                                </table>
                                                                                <tfoot>
                                                                                    <tr>
                                                                                        <td style="text-align: right; font-weight: bold;">
                                                                                            Total Receive =  <?php echo $obj->amountconvert($total_receive); ?>
                                                                                        </td>
                                                                                        <td style="text-align: right; font-weight: bold;">
                                                                                            Total Payment Paid = <?php echo $obj->amountconvert($total_paid); ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" style="text-align: right; font-weight: bold;">
                                                                                            <?php
                                                                                            $rest = $total_receive - $total_paid;
                                                                                            if (substr($rest, 0, 1) == "-") {
                                                                                                ?>
                                                                                                Account Payables Amount =
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                Account Receiveables Amount =
                                                                                                <?php
                                                                                            }
                                                                                            echo $obj->amountconvert($rest); ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tfoot>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /table with gradient -->
                                                    <!-- /selects, dropdowns -->
                                                </div>
                                                <!-- /general form elements -->     
                                                <!-- Default datatable -->
                                                <!-- /default datatable -->
                                            </fieldset>                     
                                        </form>
                                        <!--tab 1 content start from here-->  
                                    </div>


<?php } ?>





                            </div>



                            <!-- General form elements -->

                            <!-- /general form elements -->






                            <div class="clearfix"></div>

                            <!-- Default datatable -->

                            <!-- /default datatable -->


                            </fieldset>                     



                            <!-- Content End from here customized -->




                            <div class="separator-doubled"></div> 



                        </div>
                        <!-- /content container -->

                    </div>
                </div>
            </div>
        </div>

        <!-- /main content -->
<?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');   ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
