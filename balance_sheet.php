<?php
include('class/auth.php');
include('./querysheld/balancesheet.php');
$obj_balancesheet = new BalanceSheet();
if ($input_status == 1) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_balancesheet->admin($from, $to);
    }else{
        $sql = $obj_balancesheet->admin($setting_start, $setting_end);
    }
} elseif ($input_status == 2) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_balancesheet->shop_admin($from, $to, $input_by);
    } else {
        $sql = $obj_balancesheet->shop_admin($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 3) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_balancesheet->cashier($from, $to, $input_by);
    }else{
        $sql = $obj_balancesheet->cashier($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 4) {
    extract($_GET);
    if (isset($_GET['date_report'])) {
        $sql = $obj_balancesheet->manager($from, $to, $input_by);
    } else {
        $sql = $obj_balancesheet->manager($setting_start, $setting_end, $input_by);
    }
} elseif ($input_status == 5) {

    $array_ch = array();
    $sqlchain_store_ids = $obj->FlyQuery("SELECT store_id FROM store_chain_admin WHERE sid='191'");
    if (!empty($sqlchain_store_ids)) {
        foreach ($sqlchain_store_ids as $ch):
            array_push($array_ch, $ch->store_id);
        endforeach;
    }


    extract($_GET);
    if (!empty($array_ch)) {
        if (isset($_GET['date_report'])) {
            $sql = $obj_balancesheet->store_chain_admin($from, $to, $array_ch);
        } else {
            $sql = $obj_balancesheet->store_chain_admin($setting_start, $setting_end, $array_ch);
        }
    } else {
        $sql = array();
    }
}

$sql_icpa=$obj->FlyQuery("SELECT 
            a.id,
            a.head_sub_list_name,
            IFNULL(b.debit,0) as debit,
            IFNULL(b.cradit,0) as cradit,
            IFNULL((b.debit-b.cradit),0) as total
            FROM 
            `account_module_ladger_list_properties` as a 
            LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id
            WHERE 
            a.main_head_id='6'");
$ipca_current_liability=0;
$ipca_current_asset=0;
if(!empty($sql_icpa))
foreach($sql_icpa as $row_ipca)
{
                $ipca_current_liability+=$row_ipca->debit; 
                $ipca_current_asset+=$row_ipca->cradit; 
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <bod
    <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Balance Sheet </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>

                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <!-- General form elements -->
                                <div class="row-fluid block">
                                    <!-- General form elements -->
                                    <div class="row-fluid  span12 well">   
                                        <div class="span12">
                                            <h3 align="center"><strong>Balancesheet Statement</strong></h3>
                                            <?php
                                            include('./include/expected.php');
                                            ?>
                                        </div>
                                        <style type="text/css">
                                            .datatable-header{ border-top: 1px #CCC dotted; }
                                        </style>
                                        <!-- Selects, dropdowns -->
                                        <div class="table-overflow">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td width="463" align="left" valign="middle"><h4><strong>Asset</strong></h4></td>
                                                        <td width="595" align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Bank</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='1')");
                                                    $cogs = 0;
                                                    $total_revenue = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;
                                                        $netm = $cogsexpense->total;
                                                        if ($netm != 0) {
                                                            ?>
                                                            <tr>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $cogsid; ?>" style="text-decoration:none; font-weight:normal;"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                </td>
                                                                <td align="right" valign="middle" >
                                                                    <span style="text-decoration:none; font-weight:normal;">
                                                                        <?php
                                                                        $getch = substr($netm, 0, 1);
                                                                        if ($getch != '-') {
                                                                            echo $obj->amountconvert($netm);
                                                                        } else {
                                                                            echo $obj->amountconvert(substr($netm, 1, 200));
                                                                        }
                                                                        $total_revenue+=$netm;
                                                                        ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Bank</strong></td>
                                                        <td align="right" valign="middle"><strong><u><?php
                                                                    //echo $total_revenue;
                                                                    $chktr = substr($total_revenue, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        $totalforbank = $total_revenue;
                                                                        echo $obj->amountconvert($totalforbank);
                                                                    } else {
                                                                        $totalforbank = substr($total_revenue, 1, 200);
                                                                        echo $obj->amountconvert($totalforbank);
                                                                    }
                                                                    ?></u></strong></td>
                                                    </tr>
                                                    <!--bank query end -->
                                                    <!--current asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Current Asset</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Inter Project Current Asset</td>
                                                        <td align="right" valign="middle" ><?php echo $obj->amountconvert($ipca_current_asset); ?></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='2')");
                                                    $currentasset = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;

                                                        $netm1 = $cogsexpense->total;
                                                        if ($netm1 != 0) {
                                                            ?>

                                                            <tr>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $cogsid; ?>"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                </td>
                                                                <td align="right" valign="middle" >
                                                                    <?php
                                                                    $getch = substr($netm1, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm1);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm1, 1, 200));
                                                                    }
                                                                    $currentasset+=$netm1;
                                                                    ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Current Asset</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($currentasset, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        $currentassettotal = $currentasset + $ipca_current_asset;
                                                                        echo $obj->amountconvert($currentassettotal);
                                                                    } else {
                                                                        $currentassettotal = substr($currentasset, 1, 200) + $ipca_current_asset;
                                                                        echo $obj->amountconvert($currentassettotal);
                                                                    }
                                                                    ?></u>
                                                            </strong></td>
                                                    </tr>
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Fixed Asset</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a
                                                    LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM account_module_head_sub_list WHERE list_of_head_accounts_id ='1' AND list_of_sub_head_accounts_id='3')");
                                                    $fixedasset = 0;
                                                    foreach ($chkfourcountled as $cogsexpense) {
                                                        $cogsid = $cogsexpense->id;
                                                        $debit = $cogsexpense->debit;
                                                        $cradit = $cogsexpense->cradit;

                                                        $netm2 = $cogsexpense->total;
                                                        if ($netm2 != 0) {
                                                            ?>
                                                            <tr>
                                                                <td valign="middle" align="left"><a href="viewledger.php?ladger_id=<?php echo $cogsid; ?>"></a><a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a></td>
                                                                <td align="right" valign="middle" ><?php
                                                                    $getch = substr($netm2, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm2);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm2, 1, 200));
                                                                    }
                                                                    $fixedasset+=$netm2;
                                                                    ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                    <tr><td valign="middle" align="left"><strong>Total for Fixed Asset</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($fixedasset, 0, 1);
                                                                    if ($chktr != '-') {
                                                                        $fixedassettotal = $fixedasset;
                                                                        echo $obj->amountconvert($fixedassettotal);
                                                                    } else {
                                                                        $fixedassettotal = substr($fixedasset, 1, 200);
                                                                        echo $obj->amountconvert($fixedassettotal);
                                                                    }
                                                                    ?></u>
                                                            </strong></td>
                                                    </tr>
                                                    <!--liability section start -->
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Asset</strong></td>
                                                        <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><strong><u><?php 
                                                        $totalasset = $total_revenue + $currentasset + $ipca_current_asset + $fixedasset;
                                                        echo $obj->amountconvert($totalasset); 
                                                        ?></u></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><h4><strong>Liability</strong></h4></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Current Liability</strong></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Inter Current Project Account</td>
                                                        <td align="right" valign="middle" ><?php echo $obj->amountconvert($ipca_current_liability); ?></td>
                                                    </tr>
                                                    <?php
                                                    $ld = $obj->FlyQuery("SELECT 
                                                    a.id,
                                                    a.head_sub_list_name,
                                                    IFNULL(b.debit,0) as debit,
                                                    IFNULL(b.cradit,0) as cradit,
                                                    IFNULL((b.debit-b.cradit),0) as total
                                                    FROM 
                                                    `account_module_ladger_list_properties` as a 
                                                    LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id AND b.ladger_id!='41'
                                                    WHERE 
                                                    a.main_head_id='2' AND a.head_sub_list_id!='382'");
                                                    $liabilitycurrent = 0;
                                                    foreach ($ld as $row) {
                                                        $inid = $row->id;
                                                        $debit = $row->debit;
                                                        $cradit = $row->cradit;
                                                        $netm3 = $row->total;
                                                        if ($netm3 != 0) {
                                                            ?>

                                                            <tr>
                                                                <td valign="middle" align="left">
                                                                    <a href="viewledger.php?ladger_id=<?php echo $inid; ?>"><?php echo $row->head_sub_list_name; ?></a>
                                                                </td>
                                                                <td align="right" valign="middle" ><?php
                                                                    $getch = substr($netm3, 0, 1);
                                                                    if ($getch != '-') {
                                                                        echo $obj->amountconvert($netm3);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($netm3, 1, 200));
                                                                    }
                                                                    $liabilitycurrent+=$netm3;
                                                                    ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total for Current Liability</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $chktr = substr($liabilitycurrent, 0, 1);
                                                                    if ($chktr != "-") {
                                                                        $liabilitycurrenttotal = $liabilitycurrent;
                                                                        echo $obj->amountconvert($liabilitycurrenttotal);
                                                                    } else {
                                                                        $liabilitycurrenttotal = substr($liabilitycurrent, 1, 200);
                                                                        echo $obj->amountconvert($liabilitycurrenttotal);
                                                                    }
                                                                    ?>
                                                                </u></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Liabilities</strong></td>
                                                        <td align="right" valign="middle"><strong><u>
                                                                    <?php
                                                                    $sdh = substr($liabilitycurrent, 0, 1);

                                                                    $absliablity = substr($liabilitycurrent, 1, 200);

                                                                    if ($sdh == "-") {
                                                                        $ttliabilities = $absliablity + $ipca_current_liability;
                                                                    } else {
                                                                        $ttliabilities = $liabilitycurrent + $ipca_current_liability;
                                                                    }
                                                                    echo $obj->amountconvert($ttliabilities);
                                                                    //if($sdh)
                                                                    //$ipca_current_liability;
                                                                    //echo $liabilitycurrent;								
                                                                    ?>
                                                                </u></strong></td>
                                                    </tr>

                                                    <!--liability section start -->
                                                    <!--Fixed asset start -->
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"> 
                                                            <h4><strong>Equity</strong></h4></td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <?php
                                                    $chkfourcountled = $obj->FlyQuery("SELECT 
                                                    a.id, 
                                                    a.head_sub_list_name, 
                                                    IFNULL(b.debit,0) as debit, 
                                                    IFNULL(b.cradit,0) as cradit, 
                                                    IFNULL((b.debit-b.cradit),0) as total 
                                                    FROM 
                                                    account_module_ladger_list_properties as a 
                                                    LEFT JOIN (".$sql.") as b ON b.ladger_id=a.id 
                                                    WHERE 
                                                    a.head_sub_list_id 
                                                    IN 
                                                    (SELECT id FROM `account_module_head_sub_list` WHERE list_of_head_accounts_id='5') AND a.main_head_id='5'");
                                                    $totalequity = 0;
                                                    if (!empty($chkfourcountled))
                                                        foreach ($chkfourcountled as $cogsexpense) {
                                                            $cogsid = $cogsexpense->id;
                                                            $debit = $cogsexpense->debit;
                                                            $cradit = $cogsexpense->cradit;
                                                            $netm4 = $cogsexpense->total;
                                                            if ($netm4 != 0) {
                                                                ?>
                                                                <tr>
                                                                    <td valign="middle" align="left">
                                                                        <a href="viewledger.php?ladger_id=<?php echo $cogsid; ?>"><?php echo $cogsexpense->head_sub_list_name; ?></a>
                                                                    </td>
                                                                    <td align="right" valign="middle" ><strong>
                                                                            <?php
                                                                            $getch = substr($netm4, 0, 1);
                                                                            if ($getch != '-') {
                                                                                echo $obj->amountconvert($netm4);
                                                                            } else {
                                                                                echo $obj->amountconvert(substr($netm4, 1, 200));
                                                                            }
                                                                            $totalequity+=$netm4;
                                                                            ?></strong></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    ?>


                                                    <tr>
                                                        <td valign="middle" align="left">Retained Earnings</td>
                                                        <td align="right" valign="middle">
                                                            <?php
                                                            $preyear = date('Y');
                                                            $previousyear = $preyear - 1;
                                                            $chkpredata = $obj->exists_multiple("account_module_income_statement", array("year" => $previousyear));
                                                            if ($chkpredata == 0) {
                                                                echo $obj->amountconvert("0");
                                                            } else {
                                                                $querypre = $obj->SelectAllByVal("account_module_income_statement", "year", $previousyear, "amount");
                                                                //$fetpre=mysql_fetch_array($querypre);
                                                                echo $obj->amountconvert($querypre);
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">Current Year Earnings</td>
                                                        <td align="right" valign="middle">
                                                            <!--income statement result script-->
                                                            <?php
                                                            $inyear = date('Y');
                                                            //$netprofit_query=$obj->SelectAllByID_Multiple("account_module_income_statement",array("year"=>$inyear));
                                                            //$fetnet=mysql_fetch_array($netprofit_query);
                                                            $netprofit = $obj->SelectAllByVal("account_module_income_statement", "year", $inyear, "amount");
                                                            $chknp = substr($netprofit, 0, 1);
                                                            $chknpp = substr($netprofit, 1, 200);

                                                            if ($chknp != "-") {
                                                                echo $obj->amountconvert($netprofit);
                                                            } else {
                                                                echo $obj->amountconvert(substr($netprofit, 1, 200));
                                                            }
                                                            ?>
                                                            <!--income statement result calculate end-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left"><strong>Total Equity</strong></td>
                                                        <td align="right" valign="middle"><u><strong>
                                                                    <?php
                                                                    $feq = $totalequity + $netprofit;

                                                                    $chknp1 = substr($feq, 0, 1);

                                                                    if ($chknp1 != "-") {
                                                                        echo $obj->amountconvert($feq);
                                                                    } else {
                                                                        echo $obj->amountconvert(substr($feq, 1, 200));
                                                                    }
                                                                    ?>
                                                                </strong></u></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">&nbsp;</td>
                                                        <td align="right" valign="middle"></td>
                                                    </tr>
                                                    <tr><td valign="middle" align="left"><strong>Total Liabilities and Equity</strong></td>
                                                        <td align="right" valign="middle"><span style="padding-bottom:2px; border-bottom:#000 medium inset;"><strong><u><?php
                                                                        $totalassetsl = $ttliabilities + $feq;
                                                                        echo $obj->amountconvert($totalassetsl);

                                                                        $year = date('Y');
                                                                        $date = date('Y-m-d');
                                                                        $chkbalancesheet = $obj->exists_multiple("account_module_balancesheet", array("year" => $year));
                                                                        if ($chkbalancesheet != 0) {
                                                                            $obj->update("account_module_balancesheet", array("year" => $year, "totalasset" => $totalasset, "totalliability" => $liabilitycurrent, "totalequity" => $feq, "branch_id" => $input_by, "date" => $date));
                                                                        } else {
                                                                            $obj->insert("account_module_balancesheet", array("totalasset" => $totalasset, "totalliability" => $liabilitycurrent, "totalequity" => $feq, "year" => $year, "branch_id" => $input_by, "date" => $date));
                                                                        }
                                                                        ?></u></strong></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /selects, dropdowns -->



                                        <!-- Selects, dropdowns -->
                                        <!-- /selects, dropdowns -->
                                    </div>
                                    <!-- /general form elements -->     
                                    <div class="clearfix"></div>
                                    <!-- Default datatable -->
                                    <!-- /default datatable -->
                                    <!--tab 1 content start from here-->  
                                </div>
                            </div>
                            <!-- General form elements -->
                            <!-- /general form elements -->
                            <div class="clearfix"></div>
                            <!-- Default datatable -->
                            <!-- /default datatable -->
                            <!-- Content End from here customized -->
                            <div class="separator-doubled"></div> 
                        </div>
                        <!-- /content container -->
                    </div>
                </div>
            </div>
        </div> 
        <!-- /main content -->
        <?php include('include/footer.php'); ?>
        <!-- Right sidebar -->
        <?php //include('include/sidebar_right.php');    ?>
        <!-- /right sidebar -->

    </div>
    <!-- /main wrapper -->

</body>
</html>
