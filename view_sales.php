<?php 
include('class/auth.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    	<?php echo $obj->bodyhead(); ?>
			<script>
        nucleus(document).ready(function()
		{
			var viewid=<?php echo $_GET['id']; 
			/*$ss=array("status"=>1,
	"customer"=>$customer,
	"subheading"=>$subheading,
	"shop_id"=>$shop_id,
	"invoicedate"=>$invoicedate,
	"paiddate"=>$paiddate,
	"notes"=>$notes,
	"invoice_id"=>$invoice_id,
	"datatr"=>$newtr);*/
			?>;
			nucleus.post("./lib/sales.php",{'st':5,'view':viewid},function(data)
			{
				var datacl=jQuery.parseJSON(data);
				var status=datacl.status;
				if(status==1)
				{
					var stringdata=datacl.datatr;
					var customer=datacl.customer;
					var subheading=datacl.subheading;
					var shop_id=datacl.shop_id;
					var invoicedate=datacl.invoicedate;
					var paiddate=datacl.paiddate;
					var notes=datacl.notes;
					var invoice_id=datacl.invoice_id;
					var fotter=datacl.datafot;
					nucleus("#customer").html(customer);
					nucleus("#invoice_date").html(invoicedate);
					nucleus("#paiddate").html(paiddate);
					nucleus("#subheading").html(subheading);
					nucleus("#shop_id").html(shop_id);
					nucleus("#memo").html(notes);
					nucleus("#loadsales").html(stringdata);	
					nucleus("#loadsales_fot").html(fotter);				
				}
				else
				{
					var stringdata="<tr><td colspan='5'>No Data Found</td></tr>";
					nucleus("#loadsales").html(stringdata);
				}
			});
			
		});
		</script>
		
        
    </head>

    <body>
     
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
				<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Sales Invoice Info </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                           
                        </div><!-- /page header -->

                        <div class="body">
							 
                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->


                                    <div class="row-fluid block">
                                     <form class="form-horizontal" id="myForm" method="post" name="invoice" action="">    
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="span4">
                                            	<div class="control-group" id="select-customer-id">
                                                    <label class="span6">Customer :</label>
                                                    <label class="span6" id="customer">Customer :</label>
                                                </div>
                                            	
                                            	<div class="control-group">
                                                    <label class="span6">Invoice Date *</label>
                                                    <label class="span6" id="invoice_date">Invoice Date *</label>
                                                </div>                                               
                                            </div>
                                            
                                            <div class="span4">
                                            	<div class="control-group">
                                                    <label class="span6">Subheading *</label>
                                                    <label class="span6" id="subheading">Subheading *</label>
                                                </div>        
                                                
                                                <div class="control-group">
                                                    <label class="span6">Due Paid Date *</label>
                                                    <label class="span6" id="paiddate"></label>
                                                </div>  
                                                                                       
                                            </div>
                                            <!-- /selects, dropdowns -->

                                            <!-- Selects, dropdowns -->
                                            <div class="span4">
                                                
												
                                                <div class="control-group">
                                                    <label class="span6">Shop *</label>
                                                    <label class="span6" id="shop_id"></label>
                                                </div>
                                                
                                                
                                                <div class="control-group">
                                                    <label class="span6">Note / Memo *</label>
                                                    <label class="span6" id="memo">Note / Memo *</label>
                                                </div> 
                                                
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     

										
                                        </fieldset>                     

                                </form>
                                        
                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>
                                        <!-- General form elements -->












										<div class="row-fluid block">
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">   
                                           
                                           <!-- Selects, dropdowns -->
                                            <div class="table-overflow">
                                           <form class="form-horizontal" id="myForms" method="post" name="invoice" action="">    
                                    <fieldset>
                                                <table class="table table-striped table-bordered"  id="productmore">
                                                        <thead>
                                                            <tr>
                                                            	<th>#</th>
                                                                <th>Product / Service</th>
                                                                <th>Detail</th>
                                                                <th style="text-align:right;">Quantity</th>
                                                                <th style="text-align:right;">Unite Price</th>
                                                                <th style="text-align:right;">Total Price</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody id="loadsales">
                                                        	       
                                                        </tbody>
                                                        <tfoot id="loadsales_fot">
                                                        
                                                        </tfoot>
                                                    </table>
                                                    </fieldset>                     

                                </form>
                                                      
                                                </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     

</fieldset>                     

                                </form>
                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->

                                          
                                          <!--tab 1 content start from here-->  
                                               
                                        </div>




									
                                        <!-- General form elements -->




                                        
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                                  



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>  
            
            
                
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->
		<div id="habijabi"></div>
    </body>
</html>
