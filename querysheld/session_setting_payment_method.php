<?php

class PaymentMethod {

    public function admin($from, $to) {
        $obj=new db_class();
        $query="select
        a.id,
        a.meth_name,
        p.store_id,
        p.pid,
        p.ledger_id,
        l.head_sub_list_name as ledger,
        p.date
        from
        payment_method as a
        LEFT JOIN account_module_payment_method_ledger as p ON p.pid=a.id
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=p.ledger_id";
        $sql=$obj->FlyQuery($query);

        return $sql;
    }

    public function shop_admin($from, $to, $store) {
        $obj=new db_class();
        $query="SELECT alldata.* FROM (select
        a.id,
        a.meth_name,
        p.store_id,
        p.pid,
        p.ledger_id,
        l.head_sub_list_name as ledger,
        p.date
        from
        payment_method as a
        LEFT JOIN account_module_payment_method_ledger as p ON p.pid=a.id AND p.store_id=" . $store . "
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=p.ledger_id) as alldata";
        $sql=$obj->FlyQuery($query);

        return $sql;
    }

    public function cashier($from, $to, $store) {
        $obj=new db_class();
        $query="SELECT alldata.* FROM (select
        a.id,
        a.meth_name,
        p.store_id,
        p.pid,
        p.ledger_id,
        l.head_sub_list_name as ledger,
        p.date
        from
        payment_method as a
        LEFT JOIN account_module_payment_method_ledger as p ON p.pid=a.id AND p.store_id=" . $store . "
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=p.ledger_id) as alldata";
        $sql=$obj->FlyQuery($query);

        return $sql;
    }

    public function manager($from, $to, $store) {
        $obj=new db_class();
        $query="SELECT alldata.* FROM (select
        a.id,
        a.meth_name,
        p.store_id,
        p.pid,
        p.ledger_id,
        l.head_sub_list_name as ledger,
        p.date
        from
        payment_method as a
        LEFT JOIN account_module_payment_method_ledger as p ON p.pid=a.id AND p.store_id=" . $store . "
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=p.ledger_id) as alldata";
        $sql=$obj->FlyQuery($query);

        return $sql;
    }

    public function store_chain_admin($from, $to, $store) {
        $count=0;
        $fields='';
        $obj=new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "p.store = '$val' ";
        }

        $query="SELECT chainadmin.* FROM (select
        a.id,
        a.meth_name,
        p.store_id,
        p.pid,
        p.ledger_id,
        l.head_sub_list_name as ledger,
        p.date
        from
        payment_method as a
        LEFT JOIN account_module_payment_method_ledger as p ON p.pid=a.id AND (" . $fields . ")
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=p.ledger_id) as chainadmin";
        $sql=$obj->FlyQuery($query);

        return $sql;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

