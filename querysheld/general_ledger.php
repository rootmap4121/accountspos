<?php

class GeneralLedger {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        A.id,
        A.head_sub_list_name,
        B.ladger_date as date,
        B.ladger_id,
        s.store_id as store,
        IFNULL(SUM(B.debit),0) as debit,
        IFNULL(SUM(B.cradit),0) as cradit,
        IFNULL((SUM(B.debit)-SUM(B.cradit)),0) as net_movement   
        FROM account_module_ladger_list_properties as A 
        LEFT JOIN account_module_ladger B ON A.id = B.ladger_id 
        LEFT JOIN store as s ON s.id=B.branch_id
        GROUP BY A.id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' GROUP BY alldata.ladger_id";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.jd_id,
        a.jd,
        a.jddate,
        a.jddate as date,
        sum(b.debit) as total,
        s.store_id as store
        from 
        account_module_journal_description as a 
        left join account_module_ladger as b on b.link_id=a.link_id 
        LEFT JOIN store as s ON s.id=a.branch_id
        WHERE a.`status`='1' 
        GROUP BY a.link_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.jd_id,
        a.jd,
        a.jddate,
        a.jddate as date,
        sum(b.debit) as total,
        s.store_id as store
        from 
        account_module_journal_description as a 
        left join account_module_ladger as b on b.link_id=a.link_id 
        LEFT JOIN store as s ON s.id=a.branch_id
        WHERE a.`status`='1' 
        GROUP BY a.link_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.jd_id,
        a.jd,
        a.jddate,
        a.jddate as date,
        sum(b.debit) as total,
        s.store_id as store
        from 
        account_module_journal_description as a 
        left join account_module_ladger as b on b.link_id=a.link_id 
        LEFT JOIN store as s ON s.id=a.branch_id
        WHERE a.`status`='1' 
        GROUP BY a.link_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (select 
        a.id,
        a.jd_id,
        a.jd,
        a.jddate,
        a.jddate as date,
        sum(b.debit) as total,
        s.store_id as store
        from 
        account_module_journal_description as a 
        left join account_module_ladger as b on b.link_id=a.link_id 
        LEFT JOIN store as s ON s.id=a.branch_id
        WHERE a.`status`='1' 
        GROUP BY a.link_id) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

