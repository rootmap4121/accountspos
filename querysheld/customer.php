<?php

class Customer {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT
        i.id,
        i.company_name,
        concat(i.fname,' ',i.lname) as name,
        i.phone,
        IFNULL(p.amount, 0) as paid_amount,
        IFNULL(p.totalamount, 0) as total_amount,
        IFNULL((p.totalamount-p.amount),0) as due_amount,
        i.date,
        i.shop_id as store
        FROM account_module_customer AS i
        LEFT JOIN (SELECT
                   customer_id,
                   invoice_id,
                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                   FROM account_module_invoice
                   GROUP BY customer_id) AS p
        ON p.customer_id = i.id ORDER BY i.id DESC) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        return $sql;
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT
        i.id,
        i.company_name,
        concat(i.fname,' ',i.lname) as name,
        i.phone,
        IFNULL(p.amount, 0) as paid_amount,
        IFNULL(p.totalamount, 0) as total_amount,
        IFNULL((p.totalamount-p.amount),0) as due_amount,
        i.date,
        i.shop_id as store
        FROM account_module_customer AS i
        LEFT JOIN (SELECT
                   customer_id,
                   invoice_id,
                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                   FROM account_module_invoice
                   GROUP BY customer_id) AS p
        ON p.customer_id = i.id ORDER BY i.id DESC) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT
        i.id,
        i.company_name,
        concat(i.fname,' ',i.lname) as name,
        i.phone,
        IFNULL(p.amount, 0) as paid_amount,
        IFNULL(p.totalamount, 0) as total_amount,
        IFNULL((p.totalamount-p.amount),0) as due_amount,
        i.date,
        i.shop_id as store
        FROM account_module_customer AS i
        LEFT JOIN (SELECT
                   customer_id,
                   invoice_id,
                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                   FROM account_module_invoice
                   GROUP BY customer_id) AS p
        ON p.customer_id = i.id ORDER BY i.id DESC) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT
        i.id,
        i.company_name,
        concat(i.fname,' ',i.lname) as name,
        i.phone,
        IFNULL(p.amount, 0) as paid_amount,
        IFNULL(p.totalamount, 0) as total_amount,
        IFNULL((p.totalamount-p.amount),0) as due_amount,
        i.date,
        i.shop_id as store
        FROM account_module_customer AS i
        LEFT JOIN (SELECT
                   customer_id,
                   invoice_id,
                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                   FROM account_module_invoice
                   GROUP BY customer_id) AS p
        ON p.customer_id = i.id ORDER BY i.id DESC) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (SELECT
        i.id,
        i.company_name,
        concat(i.fname,' ',i.lname) as name,
        i.phone,
        IFNULL(p.amount, 0) as paid_amount,
        IFNULL(p.totalamount, 0) as total_amount,
        IFNULL((p.totalamount-p.amount),0) as due_amount,
        i.date,
        i.shop_id as store
        FROM account_module_customer AS i
        LEFT JOIN (SELECT
                   customer_id,
                   invoice_id,
                   (select sum(amount) from account_module_invoice_payment where account_module_invoice_payment.invoice_id=account_module_invoice.invoice_id) as amount,
                   (select sum(totalamount) from account_module_invoice_detail where account_module_invoice_detail.invoice_id=account_module_invoice.invoice_id) as totalamount  
                   FROM account_module_invoice
                   GROUP BY customer_id) AS p
        ON p.customer_id = i.id ORDER BY i.id DESC) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

