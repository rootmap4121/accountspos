<?php

class Sales {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select a.id,
        a.invoice_id,
        a.customer_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(amid.`quantity`),0) as `quantity`,
        IFNULL(SUM(amid.`price`),0) as `price`,
        IFNULL(SUM(amid.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(amip.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_invoice as a
        LEFT JOIN account_module_customer as d ON d.id=a.customer_id 
        LEFT JOIN account_module_invoice_detail as amid ON amid.invoice_id=a.id
        LEFT JOIN account_module_invoice_payment as amip ON amip.invoice_id=a.id
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY amid.invoice_id,amip.invoice_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select a.id,
        a.invoice_id,
        a.customer_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(amid.`quantity`),0) as `quantity`,
        IFNULL(SUM(amid.`price`),0) as `price`,
        IFNULL(SUM(amid.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(amip.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_invoice as a
        LEFT JOIN account_module_customer as d ON d.id=a.customer_id 
        LEFT JOIN account_module_invoice_detail as amid ON amid.invoice_id=a.id
        LEFT JOIN account_module_invoice_payment as amip ON amip.invoice_id=a.id
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY amid.invoice_id,amip.invoice_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select a.id,
        a.invoice_id,
        a.customer_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(amid.`quantity`),0) as `quantity`,
        IFNULL(SUM(amid.`price`),0) as `price`,
        IFNULL(SUM(amid.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(amip.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_invoice as a
        LEFT JOIN account_module_customer as d ON d.id=a.customer_id 
        LEFT JOIN account_module_invoice_detail as amid ON amid.invoice_id=a.id
        LEFT JOIN account_module_invoice_payment as amip ON amip.invoice_id=a.id
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY amid.invoice_id,amip.invoice_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select a.id,
        a.invoice_id,
        a.customer_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(amid.`quantity`),0) as `quantity`,
        IFNULL(SUM(amid.`price`),0) as `price`,
        IFNULL(SUM(amid.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(amip.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_invoice as a
        LEFT JOIN account_module_customer as d ON d.id=a.customer_id 
        LEFT JOIN account_module_invoice_detail as amid ON amid.invoice_id=a.id
        LEFT JOIN account_module_invoice_payment as amip ON amip.invoice_id=a.id
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY amid.invoice_id,amip.invoice_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (select a.id,
        a.invoice_id,
        a.customer_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(amid.`quantity`),0) as `quantity`,
        IFNULL(SUM(amid.`price`),0) as `price`,
        IFNULL(SUM(amid.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(amip.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_invoice as a
        LEFT JOIN account_module_customer as d ON d.id=a.customer_id 
        LEFT JOIN account_module_invoice_detail as amid ON amid.invoice_id=a.id
        LEFT JOIN account_module_invoice_payment as amip ON amip.invoice_id=a.id
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY amid.invoice_id,amip.invoice_id) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

