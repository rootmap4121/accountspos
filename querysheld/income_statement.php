<?php

class IncomeStatement {

    public function admin($from, $to) {
        $query="SELECT 
        l.ladger_id,
        l.ladger_date,
        l.branch_id,
        SUM(l.debit) as debit,
        SUM(l.cradit) as cradit,
        s.store_id as store
        FROM account_module_ladger as l
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."'
        GROUP BY l.ladger_id";
        return $query;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function cashier($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function manager($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "extra.store = '$val' ";
        }
        
        
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE ".$fields;
        
        return $query;        
    }
    
    
    public function admin_last_year($store)
    {
        $obj=new db_class();
        $query="SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a 
        WHERE a.store='".$store."' AND a.status='2' ORDER BY a.id DESC";
        $getse=$obj->FlyQuery($query);
        return $new_array=array("last_year_opening"=>$obj->dates(@$getse[0]->session_start_date),"last_year_end"=>$obj->dates(@$getse[0]->session_end_date));
    }
    
    public function chain_admin_last_year($store)
    {
        $obj=new db_class();
        
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "extra.store = '$val' ";
        }
        
        $query="SELECT extra.* FROM (SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a 
        WHERE a.status='2' ORDER BY a.id DESC) as extra WHERE ".$fields;
        $getse=$obj->FlyQuery($query);
        return $new_array=array("last_year_opening"=>$obj->dates(@$getse[0]->session_start_date),"last_year_end"=>$obj->dates(@$getse[0]->session_end_date));
    }
    
    public function other_admin_last_year($store)
    {
        $obj=new db_class();
        $query="SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a 
        WHERE a.store='".$store."' AND a.status='2' ORDER BY a.id DESC";
        $getse=$obj->FlyQuery($query);
        return $new_array=array("last_year_opening"=>$obj->dates(@$getse[0]->session_start_date),"last_year_end"=>$obj->dates(@$getse[0]->session_end_date));
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

