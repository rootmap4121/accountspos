<?php

class BalanceSheet {

    public function admin($from, $to) {
        $query="SELECT 
        l.ladger_id,
        l.ladger_date,
        l.branch_id,
        SUM(l.debit) as debit,
        SUM(l.cradit) as cradit,
        s.store_id as store
        FROM account_module_ladger as l
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."'
        GROUP BY l.ladger_id";
        return $query;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function cashier($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function manager($from, $to,$store) {
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE extra.store='".$store."'";
        
        return $query;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "extra.store = '$val' ";
        }
        
        
        $query="SELECT extra.* FROM (SELECT 
        l.ladger_id, 
        l.ladger_date, 
        l.branch_id, 
        SUM(l.debit) as debit, 
        SUM(l.cradit) as cradit, 
        s.store_id as store 
        FROM account_module_ladger as l 
        LEFT JOIN store as s ON s.id=l.branch_id 
        WHERE l.ladger_date>='".$from."' AND l.ladger_date<='".$to."' GROUP BY l.ladger_id) as extra 
        WHERE ".$fields;
        
        return $query;        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

