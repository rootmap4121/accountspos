<?php

class Payment {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        concat(b.fname,' ',b.lname) as customer_name,
        a.vendor_id,
        a.date,
        a.pa,
        p.head_sub_list_name as ledger,
        a.amount,
        s.name as receiver,
        s.store_id as store
        FROM account_module_bill_payment as a 
        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
        LEFT JOIN store as s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        return $sql;
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        concat(b.fname,' ',b.lname) as customer_name,
        a.vendor_id,
        a.date,
        a.pa,
        p.head_sub_list_name as ledger,
        a.amount,
        s.name as receiver,
        s.store_id as store
        FROM account_module_bill_payment as a 
        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
        LEFT JOIN store as s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        concat(b.fname,' ',b.lname) as customer_name,
        a.vendor_id,
        a.date,
        a.pa,
        p.head_sub_list_name as ledger,
        a.amount,
        s.name as receiver,
        s.store_id as store
        FROM account_module_bill_payment as a 
        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
        LEFT JOIN store as s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        concat(b.fname,' ',b.lname) as customer_name,
        a.vendor_id,
        a.date,
        a.pa,
        p.head_sub_list_name as ledger,
        a.amount,
        s.name as receiver,
        s.store_id as store
        FROM account_module_bill_payment as a 
        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
        LEFT JOIN store as s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (SELECT 
        a.id,
        concat(b.fname,' ',b.lname) as customer_name,
        a.vendor_id,
        a.date,
        a.pa,
        p.head_sub_list_name as ledger,
        a.amount,
        s.name as receiver,
        s.store_id as store
        FROM account_module_bill_payment as a 
        LEFT JOIN account_module_vendor as b ON b.id=a.vendor_id 
        LEFT JOIN store as s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as p ON p.id=a.pa) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

