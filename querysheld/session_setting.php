<?php

class SessionSetting {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a) as alldata WHERE alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a) as alldata WHERE alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a) as alldata WHERE alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT 
        a.id,
        a.store_id,
        a.store_id as store,
        a.input_by,
        a.finput_by,
        a.title,
        a.session_start_year,
        a.session_end_year,
        a.session_start_date,
        a.session_end_date,
        a.date,
        a.fdate,
        a.status
        FROM 
        account_module_session_setting as a) as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

