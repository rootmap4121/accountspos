<?php

class Purchase {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.bill_id,
        a.vendor_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
        IFNULL(SUM(ambd.`price`),0) as `price`,
        IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(ambp.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_bill as a
        left JOIN account_module_customer as d ON d.id=a.vendor_id 
        left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
        LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY ambd.bill_id,ambp.bill_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.bill_id,
        a.vendor_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
        IFNULL(SUM(ambd.`price`),0) as `price`,
        IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(ambp.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_bill as a
        left JOIN account_module_customer as d ON d.id=a.vendor_id 
        left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
        LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY ambd.bill_id,ambp.bill_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.bill_id,
        a.vendor_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
        IFNULL(SUM(ambd.`price`),0) as `price`,
        IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(ambp.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_bill as a
        left JOIN account_module_customer as d ON d.id=a.vendor_id 
        left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
        LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY ambd.bill_id,ambp.bill_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (select 
        a.id,
        a.bill_id,
        a.vendor_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
        IFNULL(SUM(ambd.`price`),0) as `price`,
        IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(ambp.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_bill as a
        left JOIN account_module_customer as d ON d.id=a.vendor_id 
        left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
        LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY ambd.bill_id,ambp.bill_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (select 
        a.id,
        a.bill_id,
        a.vendor_id,
        a.branch_id,
        a.link_id, 
        a.currency,
        a.date,
        concat(d.fname,' ',d.lname) as `company_name`,
        IFNULL(SUM(ambd.`quantity`),0) as `quantity`,
        IFNULL(SUM(ambd.`price`),0) as `price`,
        IFNULL(SUM(ambd.`subtotal`),0) as `subtotal`,
        IFNULL(SUM(ambp.`amount`),0) as `amount`,
        s.store_id as store
        from account_module_bill as a
        left JOIN account_module_customer as d ON d.id=a.vendor_id 
        left JOIN account_module_bill_detail as ambd ON ambd.bill_id=a.id 
        LEFT JOIN account_module_bill_payment AS ambp ON ambp.bill_id=a.id 
        LEFT JOIN store as s ON s.id=a.branch_id
        GROUP BY ambd.bill_id,ambp.bill_id) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

