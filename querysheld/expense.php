<?php

class Expense {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.expense_id,
        IFNULL(SUM(vd.amount),0) as amount,
        IFNULL(a.vdate,'0000-00-00') as date,
        IFNULL(a.currentdate,'0000-00-00') as currentdate,
        IFNULL(ss.name,'Empty') as branch_id,
        IFNULL(s.name,'Empty') as input_by
        FROM account_module_office_expense_voucher as a 
        RIGHT JOIN account_module_expense_voucher_detail as vd ON vd.voucher_id=a.expense_id
        LEFT JOIN store as s  ON s.id=a.input_by
        LEFT JOIN store as ss ON ss.store_id=a.branch_id
        GROUP BY a.expense_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.expense_id,
        IFNULL(SUM(vd.amount),0) as amount,
        IFNULL(a.vdate,'0000-00-00') as date,
        IFNULL(a.currentdate,'0000-00-00') as currentdate,
        IFNULL(ss.name,'Empty') as branch_id,
        IFNULL(a.branch_id,0) as store,
        IFNULL(s.name,'Empty') as input_by
        FROM account_module_office_expense_voucher as a 
        RIGHT JOIN account_module_expense_voucher_detail as vd ON vd.voucher_id=a.expense_id
        LEFT JOIN store as s  ON s.id=a.input_by
        LEFT JOIN store as ss ON ss.store_id=a.branch_id
        GROUP BY a.expense_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.expense_id,
        IFNULL(SUM(vd.amount),0) as amount,
        IFNULL(a.vdate,'0000-00-00') as date,
        IFNULL(a.currentdate,'0000-00-00') as currentdate,
        IFNULL(ss.name,'Empty') as branch_id,
        IFNULL(a.branch_id,0) as store,
        IFNULL(s.name,'Empty') as input_by
        FROM account_module_office_expense_voucher as a 
        RIGHT JOIN account_module_expense_voucher_detail as vd ON vd.voucher_id=a.expense_id
        LEFT JOIN store as s  ON s.id=a.input_by
        LEFT JOIN store as ss ON ss.store_id=a.branch_id
        GROUP BY a.expense_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.id,
        a.expense_id,
        IFNULL(SUM(vd.amount),0) as amount,
        IFNULL(a.vdate,'0000-00-00') as date,
        IFNULL(a.currentdate,'0000-00-00') as currentdate,
        IFNULL(ss.name,'Empty') as branch_id,
        IFNULL(a.branch_id,0) as store,
        IFNULL(s.name,'Empty') as input_by
        FROM account_module_office_expense_voucher as a 
        RIGHT JOIN account_module_expense_voucher_detail as vd ON vd.voucher_id=a.expense_id
        LEFT JOIN store as s  ON s.id=a.input_by
        LEFT JOIN store as ss ON ss.store_id=a.branch_id
        GROUP BY a.expense_id) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (SELECT 
        a.id,
        a.expense_id,
        IFNULL(SUM(vd.amount),0) as amount,
        IFNULL(a.vdate,'0000-00-00') as date,
        IFNULL(a.currentdate,'0000-00-00') as currentdate,
        IFNULL(ss.name,'Empty') as branch_id,
        IFNULL(a.branch_id,0) as store,
        IFNULL(s.name,'Empty') as input_by
        FROM account_module_office_expense_voucher as a 
        RIGHT JOIN account_module_expense_voucher_detail as vd ON vd.voucher_id=a.expense_id
        LEFT JOIN store as s  ON s.id=a.input_by
        LEFT JOIN store as ss ON ss.store_id=a.branch_id
        GROUP BY a.expense_id) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

