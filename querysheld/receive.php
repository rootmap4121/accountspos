<?php

class Receive {

    public function admin($from, $to) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.`id`,
        concat(c.fname,' ',c.lname) as customer_name,
        a.`cid`,
        a.`date`,
        l.head_sub_list_name AS `pa`,
        a.`amount`,
        s.name as receiver 
        FROM `account_module_invoice_payment` as a 
        LEFT JOIN account_module_customer as c ON c.id=a.cid 
        LEFT JOIN store AS s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function shop_admin($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.`id`,
        concat(c.fname,' ',c.lname) as customer_name,
        a.`cid`,
        a.`date`,
        l.head_sub_list_name AS `pa`,
        a.`amount`,
        s.name as receiver,
        s.store_id as store
        FROM `account_module_invoice_payment` as a 
        LEFT JOIN account_module_customer as c ON c.id=a.cid 
        LEFT JOIN store AS s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function cashier($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.`id`,
        concat(c.fname,' ',c.lname) as customer_name,
        a.`cid`,
        a.`date`,
        l.head_sub_list_name AS `pa`,
        a.`amount`,
        s.name as receiver,
        s.store_id as store
        FROM `account_module_invoice_payment` as a 
        LEFT JOIN account_module_customer as c ON c.id=a.cid 
        LEFT JOIN store AS s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function manager($from, $to,$store) {
        $obj = new db_class();
        $query="SELECT alldata.* FROM (SELECT 
        a.`id`,
        concat(c.fname,' ',c.lname) as customer_name,
        a.`cid`,
        a.`date`,
        l.head_sub_list_name AS `pa`,
        a.`amount`,
        s.name as receiver,
        s.store_id as store
        FROM `account_module_invoice_payment` as a 
        LEFT JOIN account_module_customer as c ON c.id=a.cid 
        LEFT JOIN store AS s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa) as alldata WHERE alldata.date>='".$from."' AND alldata.date<='".$to."' AND alldata.store='".$store."'";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }
    
    public function store_chain_admin($from, $to,$store) {
        $count = 0;
        $fields = '';
        $obj = new db_class();
        foreach ($store as $val) {
            if ($count++ != 0)
                $fields .= ' OR ';
            $fields .= "chainadmin.store = '$val' ";
        }
        
        $query="SELECT chainadmin.* FROM (SELECT alldata.* FROM (SELECT 
        a.`id`,
        concat(c.fname,' ',c.lname) as customer_name,
        a.`cid`,
        a.`date`,
        l.head_sub_list_name AS `pa`,
        a.`amount`,
        s.name as receiver,
        s.store_id as store
        FROM `account_module_invoice_payment` as a 
        LEFT JOIN account_module_customer as c ON c.id=a.cid 
        LEFT JOIN store AS s ON s.id=a.input_by 
        LEFT JOIN account_module_ladger_list_properties as l ON l.id=a.pa) as alldata 
        WHERE alldata.date>='".$from."' 
        AND 
        alldata.date<='".$to."') as chainadmin 
        WHERE $fields";
        $sql = $obj->FlyQuery($query);
        
        return $sql;
        
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

